var base_url='http://rigalio.com/x/no_access/'; 

function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                nav = document.querySelector("nav");
            if (distanceY > shrinkOn) {
                classie.add(nav,"smaller");
            } else {
                if (classie.has(nav,"smaller")) {
                    classie.remove(nav,"smaller");
                }
            }
        });
    }
    window.onload = init();
/* ends header onscroll script */
/*  header script to open searchbar */
  $(document).ready(function(){
      $(".search-icon").click(function(){
          $(".dropdown-search").slideToggle( "slow" );
      });
  });

/* ends header script to open searchbar */

/*popup script of header */
$(document).ready(function() {
            
  $(".popup-btn").click(function(e) {
      $("body").append(''); $(".popup").show("medium"); 
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".popup, .overlay").hide("medium"); 
      }); 
  }); 
});
/*popup script ends of header */
/*popup script of header sign in via */
$(document).ready(function() {       
  $(".signin-popup-btn").click(function(e) {
      var effect = 'slide';
      var options = { direction: $('.mySelect').val() };
      var duration = 500;
      $("body").append(''); $(".signin-popup").show("medium"); 
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".signin-popup, .overlay").hide("medium"); 
      }); 
  }); 
});
/*popup script of header sign in via ends */

/* script to show hide more sociual icons */
$(document).ready(function(){
      /*$(".show-more").click(function(){
          $(".hidden-more-icons").slideToggle( "slow" );
      });
      $(".fwd-icon-sectn").click(function(){
          $(".hidden-more-icons").slideToggle( "slow" );
      });
*/
  });

/* script to show hide more sociual icons ends  */
/* script to show hide fwd icons */
$(document).ready(function(){
      /*$("#fwd-id1").click(function(){
          $(".sec7").slideToggle( "slow" );
      });
*/
  });
/* script to show hide fwd icons ends */
/* script to show hide hover cat icon */
$(function() {
    $('.cat-zoom-icon').click(function() {
        $(this).hide();
        $(this).next('.cat-plus-icon').show();
       //$('.cat-plus-icon').show();
    });
    $('.cat-plus-icon').click(function() {
        $(this).hide();
        $(this).prev(".cat-zoom-icon").show();
        //$('.cat-zoom-icon').show();
    });
    $('.cat-minus-icon').click(function() {
        $(this).hide();
        $(this).next('.cat-plus-icon').show();
       //$('.cat-plus-icon').show();
    });
});
/* script to show hide hover cat icon ends */
/* script to show hide hover cat follow brand button */
$(function() {


    $('.following').hover(function() {

      $(this).parent().children( ".following" ).hide();
      $(this).parent().children( ".unfollow" ).show();
           /*   $('.unfollow').show();
              $('.following').hide();
              */
    });
    $(".unfollow").on('mouseleave',function(){
        $(this).parent().children( ".following" ).show();
      $(this).parent().children( ".unfollow" ).hide();
            
    });
    $('.unfollow').click(function() {
        //$(this).hide();
        //$(this).parent().prev(".follow").show();

    });
    
});

/* script to show hide hover cat follow button ends */
//script to hide show the follow status of h1cat follow category
$(function() {
    //$(".following-cat").hide();
    //$(".unfollow-cat").hide();
    /*$('.follow-cat').click(function() {
        $(this).hide();
        $(this).next(".following-cat").show();
    });
    $('.following-cat').click(function() {
        $(this).hide();
        $(this).prev(".follow-cat").show();
    });*/
    $(document).ready(function(){
$('.following-cat').hide();


    });
    $('.following-cat').hover(function() {
              $('.unfollow-cat').show();
              $('.following-cat').hide();
    });
    $(".unfollow-cat").on('mouseleave',function(){
              $('.unfollow-cat').hide();
              $('.following-cat').show();
    });
    
});
//script to hide show the follow status of h1cat ends


/* script to scroll to top icon */
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
/* script to scroll to top icon ends */
/* script to hide show subcat icons on cat page */
$(document).ready(function(){
      $(".sub-cat-btn").click(function(){
          $(".hidden-cat-optns").slideToggle( "slow" );
      });
  });
/* script to hide show subcat icons on cat page ends */
/* category page follow btn script */
$(document).ready(function(){
/*$(".category_pg .normal").hover(
  function () {
    $(this).addClass("cat_fashion");
  },
  function () {
    $(this).removeClass("cat_fashion");

  });*/
$('.category_pg .following').hover(
      function(){ $(this).addClass('unfollow') },
     function(){ $(this).removeClass('unfollow') }
);
});

$(document).ready(function () {
    $(".category_pg .following").mouseover(function () {
      $('.category_pg .following').text("UNFOLLOW");
    });
    $(".category_pg .following").mouseout(function () {
      $('.category_pg .following').text("FOLLOWING");
    });
  });

$(function() {
    $('.category_pg .normal').click(function() {
        $('.normal').hide();
        $('.following').show();
    });
    $('.category_pg .following').click(function() {
        $('.following').hide();
        $('.normal').show();
    });
    
});
/* category page follow btn script ends */
/* category page sub cat icons script */
$(document).ready(function(){
$(".hidden-cat-optns ul li span").hover(
  function () {
    $(this).addClass("cat_fashion");
  },
  function () {
    $(this).removeClass("cat_fashion");

  });
$(".hidden-cat-optns h4").hover(
  function () {
    $(this).addClass("cat_fashion");
  },
  function () {
    $(this).removeClass("cat_fashion");

  });
});
/* category page sub cat icons script ends */
/* change color in cat on hover */
$(document).ready(function(){
$(".date-status").hover(
  function () {
    $(this).addClass("cat_fashion");
  },
  function () {
    $(this).removeClass("cat_fashion");

  });
});
/* change color in cat on hover ends */
/* placeholder in category page */
$(document).ready(function(){ 
  $('.textarea1').find("input[type=textarea], textarea").each(function(ev)
  {
      if(!$(this).val()) { 
     $(this).attr("placeholder", "Type your answer here");
  }
  });
});
/* placeholder in category page ends */

/*  for the follow and crown logic*/

 $(document).ready(function(){
      $(".myan").click(function(event){
        /*alert(this.id);
        var id=this.id;
        //alert($(this).find('a:first'));
         //var button = $(event.target).closest('button').css( "background-color", "red" );;
         console.log("You clicked on:", button);
       // $("button").children().css( "background-color", "red" );
       */
      });

  });

//for crown section 

$(document).ready(function(){
        $(".crown-sectn").click(function(event){

              var id=this.id;
              var userid=$("#myid").html();
        if(userid==''){
        alert("Please login first");
        }
        else{
        //alert(id);
        if(id){
            var data={
                "productid":id,
                "userid":userid
              }
        $.ajax({
                 type: "POST",
                  url: ""+base_url+"Ajax/",
               //dataType: 'json',
                  data: data,
                  success: function(html){
                  alert(html);
                  $("#"+id).next().css( "background", "yellow" );
                  //alert(html);
             
           }
                });  //ajax ends here 
        } 
            } //else ends here

        });


});

// for grtting the tab of feature
function gettab(featureid,productid){

//alert(featureid);
//alert(productid);
      var data={
                "productid":productid,
                "featureid":featureid
                };
          $.ajax({
                 type: "POST",
                  url: ""+base_url+"Ajax/get_specification",
               //dataType: 'json',
                  data: data,
                  success: function(html){
                    var me=html
                    //alert(me);
                 // $("#"+id).next().css( "background", "yellow" );
                 //$(this).next().append(html);
                 //$("#acc_res").html(html);//working 
                  
                 $( ".panel-default" ).after(html);

                 console.log(html);
             
                              }
                });  //ajax ends here 

}
              //fb login code 

              $(document).ready(function(){

                $('.fb').click(function(){
                  //alert("hi");
                 var imagurl= $(this).parent().parent().parent().parent().parent().find( "img" ).attr('src');
                 //alert(imagurl);
   FB.ui(
      {
       method: 'feed',
       name: 'Rigalio',
       caption: "This is testing",
       description: (
        'Rigalio'
       ),
       link: 'http://rigalio.com/x/no_access/',
       picture:+imagurl
      },
      function(response) {
      if (response && response.post_id) {
        //$("#me";
      //window.location.href="roadsafetywars.php";
        //alert("done");
        
        //window.open();
  
      } else {
       // alert('Post was not published.');
        
       // alert('https://lyxel.net/serious_biker/img/results/<?php //echo $img_result;?>');
      }
      }
    );


                });




              });

        //for the searchig of the text 
      function search(){
                 // alert("hi");
                 var serach_text=$('#serach_text').val();
                 if(serach_text!=''){
                  window.location.href=""+base_url+"main/search/"+serach_text;
                 }
                 else{
                    alert("please write something");
                }
        

        }

          //for the following brand 
        $(document).ready(function(){
           $(".follow").click(function(){
                  var userid=$("#myid").html();
                  var mythis=this;
                  //alert(mythis);
                  if(userid!=''){
                          var brandid=$(this).parent().attr('id');
                          //alert(brandid);
                          var data={
                          "brandid":brandid,
                          "userid":userid
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/follow_brand",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val>0){
                                 
                                        $(mythis).parent().children( ".follow" ).hide();
                                       $(mythis).parent().children( ".following" ).show();
                                            alert("Followed");
                                 }
                                 else{
                                   
                                        $(mythis).parent().children( ".follow" ).hide();
                                        $(mythis).parent().children( ".following" ).show();
                                        alert('already following');
                                 }                       
                      }
                          });  //ajax ends here 
                }
                else{

                  alert("Please login first");
                }

            });
        });


          //for unfollowing the brand 

            $(document).ready(function(){
                $('.unfollow').click(function(){
                  var userid=$("#myid").html();
                  var mythis=this;
                  if(userid!=''){
                          var brandid=$(this).parent().attr('id');
                          //alert(brandid);
                          var data={
                          "brandid":brandid,
                          "userid":userid
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/unfollow_brand",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val==0){
                                      alert("unfollowed");
                                      //$('.follow').show();
                                      $(mythis).parent().children( ".follow" ).show();
                                       $(mythis).parent().children( ".following" ).hide();
                                      //$('.following').hide();

                                 }
                                 else{
                                      alert('already following');
                                       $(mythis).parent().children( ".follow" ).hide();
                                       $(mythis).parent().children( ".following" ).show();
                                 }                       
                      }
                          });  //ajax ends here 
                }
                else{

                  alert("Please login first");
                }


                });


            });
    
            // for the following category
                    $(document).ready(function(){
                         $('.follow-cat').click(function(){
                          //alert("category click");
                          var mythis=this;
                          var user_id=$("#myid").html();

                  if(user_id!=''){
                          var cat_id=$(this).parent().parent().attr('id');

                         // alert(cat_id);
                             var data={
                          "categoryid":cat_id,
                          "userid":user_id
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/follow_category",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val>0){
                                    
                                     // $('.following-cat').show();
                                      //$('.follow-cat').hide();
                                       $(mythis).parent().find('.following-cat').show();
                                          $(mythis).parent().find('.follow-cat').hide();
                                            alert("followed category");

                                 }
                                 else{
                                     
                                       
                                         $(mythis).parent().find('.following-cat').show();
                                          $(mythis).parent().find('.follow-cat').hide();
                                      //$('.follow-cat').hide();
                                       alert('already following');
                                 }                       
                      }
                          });  //ajax ends here 
                }
                else{

                  alert("Please login first");
                }



                         });
                    });

                //for the unfollowing category

                $(document).ready(function(){
                  $('.unfollow-cat').click(function(){

                          var user_id=$("#myid").html();
                          var mythis=this;

                  if(user_id!=''){
                          var cat_id=$(this).parent().attr('id');

                          //alert(cat_id);
                             var data={
                          "categoryid":cat_id,
                          "userid":user_id
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/unfollow_category",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val>0){
                                      alert(" already following category");
                                      $(mythis).parent().find('.following-cat').hide();
                                       $(mythis).parent().find('.follow-cat').show();

                                 }
                                 else{
                                     
                                       // $('.following-cat').hide();
                                      //$('.follow-cat').show();
                                      $(mythis).parent().find('.following-cat').hide();
                                       $(mythis).parent().find('.follow-cat').show();
                                        alert('un follow');
                                      //  $(".following-cat").hide();
                                        //$(".follow-cat").show();

                                 }                       
                      }
                          });  //ajax ends here 
                }
                else{

                  alert("Please login first");
                }

                  });
                });

                //category page follow category function 
                  $(document).ready(function(){
                    $('.mycatfun').click(function(){
                      var cat_id=this.id;
                      //alert(cat_id);
                      var user_id=$("#myid").html();
                     // alert(user_id);
                       if(user_id!=''){
                        //  alert(cat_id);
                             var data={
                          "categoryid":cat_id,
                          "userid":user_id
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/follow_category",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val>0){
                                    
                                     // $('.following-cat').show();
                                      //$('.follow-cat').hide();
                                       //$(mythis).parent().find('.following-cat').show();
                                          //$(mythis).parent().find('.follow-cat').hide();
                                            alert("followed category");

                                 }
                                 else{
                                     
                                       
                                         //$(mythis).parent().find('.following-cat').show();
                                          //$(mythis).parent().find('.follow-cat').hide();
                                      //$('.follow-cat').hide();
                                       alert('already following');
                                 }                       
                      }
                          });  //ajax ends here 

                   }
                   else{
                    alert("login first");
                   }
                 });

                  });

            // functionality of the brand page follow button
            //brand-page-follow
            $(document).ready(function(){
                $('.brand-page-follow').click(function(){
                    var brandid=this.id;
                    var userid=$("#myid").html();
                     if(userid!=''){
                          //var brandid=$(this).parent().attr('id');
                          //alert(brandid);
                          var data={
                          "brandid":brandid,
                          "userid":userid
                          };

                        $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/follow_brand",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                           var get_val=html;
                           //checking the response 0 done following 1 done already
                                 if(get_val>0){
                                 
                                        //$(mythis).parent().children( ".follow" ).hide();
                                       //$(mythis).parent().children( ".following" ).show();
                                            alert("Followed");
                                 }
                                 else{
                                   
                                        //$(mythis).parent().children( ".follow" ).hide();
                                        //$(mythis).parent().children( ".following" ).show();
                                        alert('already following');
                                 }                       
                      }
                          });  //ajax ends here 
                }
                else{

                  alert("Please login first");
                }
                });
            });


            //plus button for the home page 

            $(document).ready(function(){
                $('.fa-plus').click(function(){
                  //alert("hi");
                  var productid=this.id;
                  var userid=$("#myid").html();
                  //alert(productid);
                    if(userid!=''){
                             
                             var data={
                          "productid":productid,
                          "userid":userid
                          };

                            $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/save_for_later",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                                  var get_val=html;
                           //checking the response 0 done following 1 done already
                                      if(get_val>0){
                                 
                                        //$(mythis).parent().children( ".follow" ).hide();
                                       //$(mythis).parent().children( ".following" ).show();
                                              alert("saved for later");
                                              return false;
                                            }
                                      else{
                                   
                                        //$(mythis).parent().children( ".follow" ).hide();
                                        //$(mythis).parent().children( ".following" ).show();
                                            alert('already saved');
                                            return false;
                                          }                       
                                  }
                          });
                    }
                    else{
                      alert("please login first");
                    }

                });
            });


          // minus button for the home page

          $(document).ready(function(){
              $('.fa-minus').click(function(){
                alert("click minus");
                   //alert("hi");
                  var productid=this.id;
                  var userid=$("#myid").html();
                  //alert(productid);
                    if(userid!=''){
                             
                             var data={
                          "productid":productid,
                          "userid":userid
                          };

                            $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/minus_for_later",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                                  var get_val=html;
                           //checking the response 0 done following 1 done already
                                      if(get_val>0){
                                 
                                        //$(mythis).parent().children( ".follow" ).hide();
                                       //$(mythis).parent().children( ".following" ).show();
                                              alert("saved for later");
                                              return false;
                                            }
                                      else{
                                   
                                        //$(mythis).parent().children( ".follow" ).hide();
                                        //$(mythis).parent().children( ".following" ).show();
                                            alert('removed from save for later');
                                            return false;
                                          }                       
                                  }
                          });
                    }
                    else{
                      alert("please login first");
                    }

              });
          });  


                //for the share button menu


                $(document).ready(function(){
                      $('.fwd-icon-sectn').click(function(){
                          //alert("hello");
                          var dis= $(this).parent().parent().parent().parent().children('.fwd-social-icons').is(':visible');
                          //alert(dis);
                          if(dis==false){
                           // alert("ok");
$('.fwd-social-icons').hide();
                            $('.write-comment').hide();
                              $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideDown();
                            }
                            else{
                              ///alert("not ok");
                              $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideUp();
                            }
                      });
                });


              //for the share more button functionality


              $(document).ready(function(){
                $(this).parent().parent().children('.hidden-more-icons').hide();
                  $('.show-more').click(function(){

                    var dis=$(this).parent().parent().children('.hidden-more-icons').is(':visible');
                    if(dis==false)
                    {
                      $(this).parent().parent().children('.hidden-more-icons').slideDown();

                    }
                    else
                    {
                      $(this).parent().parent().children('.hidden-more-icons').slideUp();
                    }

                  });
              }); 


            //for the comment section posting event

            $(document).ready(function(){
                $('.post-comment-btn').click(function(){
                    var comment=$('#comment').val();
                    //alert(comment);
                    if(comment!=''){
                       var productid=$("#productid").html();
                  var userid=$("#myid").html();
                  //alert(productid);
                    if(userid!=''){
                             
                             var data={
                          "productid":productid,
                          "userid":userid,
                          "comment":comment
                          };

                            $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/user_comment",
                            //dataType: 'json',
                            data: data,
                            success: function(html){

                              $("#comment_section").html(html);
                              var insertid=html;
                              $('#comment').empty();
                              /*if(insertid>0)
                              {
                                alert(insertid)
                              }
                              else
                              {
                                alert("error please try again");
                              }
                              */

                                  }
                          });
                    }
                    else{
                      alert("please login first");
                    }
                    }
                    else{
                      alert("Please fill your comment");
                    }

                });
            });
          //comment section crown 

          $(document).ready(function(){
              $(".reply-no").click(function(){
                var commentid=this.id;
                var user_id=$("#myid").html();
                //alert(user_id);
                if(user_id!=''){

                      var data={
                          "commentid":commentid,
                          "userid":user_id
                          };

                      $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/like_comment",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                                //alert(html);
                              //$("#comment_section").append(html);
                              // var insertid=html;
                                  }
                      });
                }
                else{
                  alert('please login first to perform this action');
                }
              });
          });
          
		   //for the image resize in a box
            $(document).ready(function(){
                  $("#abc img").each(function(){
                    var maxwidth=300;
                    var ratio=0;
                    var img=$(this);
                    if(img.width()>maxwidth){
                      ratio=img.height()/img.width();
                      img.css('height',(maxwidth*ratio));
                      img.css('width',maxwidth);
                    }
                  });

            });
		  $(document).ready(function(){

                     
                               // $("#timeline ul li:nth-child(odd)").addClass('timeline-inverted');
                              
                      });
		//for uploading the user status and image
                  $(function() {
                      $("#userfile").change(function()
                      {
                        //alert("hi");
                          var files = !!this.files ? this.files : [];
                          if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
                   
                          if (/^image/.test( files[0].type)){ // only image file
                              var reader = new FileReader(); // instance of the FileReader
                              reader.readAsDataURL(files[0]); // read the local file
                   
                              reader.onloadend = function(){ // set image data as background of div
                                  $("#user_files").css("background-image", "url("+this.result+")");
                              }
                          }
                      });
                  });
			
			 //for sending following request to the user
                $(document).ready(function(){

                  $('.follow-profile-btn').click(function(){
                    var myid=$("#myid").html();
                    var friendid=$("#friend_id").html();
                    //alert(myid+friendid);
                   // alert("hi i am in friend following");
                   if(friendid!='undefined'){
                    var data={
                          "myid":myid,
                          "friendid":friendid
                          };
                      $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/user_follow",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                                alert(html);
                              }
                      });
                    }
                    else {
                      alert("You can not follow yourself.");
                    }
                  });
                });
				
				//for posting status and image upload 
                     $(function(){
                            var filename=$('input[name=userfile]');
                            var form_url=$("#upload_file").attr('action');
                        
                            $('#post_comment1').on('click',function(event){
                               //alert("please run");    
                                   var post=$("#post").val();
                                   var user_id=$("#myid").html();
                            //alert(post);
                              var fileupload=filename[0].files[0];
                              if(fileupload!='undefined'){
                                var formData=new FormData();
                                formData.append('userfile',fileupload);
                                    formData.append('post',post);
                                      formData.append('user_id',user_id);

                                
                                $.ajax({
                                  url:form_url,
                                  type:'post',
                                  data:formData,
                                  processData:false,
                                  contentType:false,
                                  success:function(html){
                                   $( "li" ).removeClass("timeline-inverted");
                                  $("#main_content").prepend(html);
                                  $("#timeline ul li:nth-child(odd)").addClass('timeline-inverted');
                                  $("#post").val(" ");    
                                  $("#user_files").fadeOut();                              }

                                });
                                 //console.log(fileupload);
                              }
                            });
                     }); 
					 
			 /* placeholder in category page ends */
                $(document).ready(function(){
                        $(".crown-sectn1").click(function(event){

                              var id=this.id;
                              var userid=$("#myid").html();
                        if(userid==''){
                        alert("Please login first");
                        }
                        else{
                        //alert(id);
                        if(id){
                            var data={
                                "post_id":id,
                                "userid":userid
                              }
                        $.ajax({
                                 type: "POST",
                                  url: ""+base_url+"Ajax/post_crown",
                               //dataType: 'json',
                                  data: data,
                                  success: function(html){
                                  //alert(html);
                                  //$("#"+id).next().css( "background", "yellow" );
                                  //alert(html);
                             
                                    }
                                });  //ajax ends here 
                        } 
                            } //else ends here

                        });
                });
			
			 //script for the timeline comment popup
              $(document).ready(function() {   
              var post_data="";    
                $(".tag,.mytag").on('click',function() {
                  //alert("hi");
                 //  $(".tag").parent().attr('id')
                    var id=$(this).parent().attr('id');
                      $("#thispost_id").html(id);
                    var source=$(this).parent().attr('name');
                    post_data=$(this).html();          

                    $("#user_post_show").html(post_data);

                    var data={
                      "post_id":id
                    }
                    $.ajax({
                                 type: "POST",
                                  url: ""+base_url+"finalrigalio/Main/get_comments",
                               //dataType: 'json',
                                  data: data,
                                  success: function(html){
                                    //alert(html);
                                $("#all_comments").html(html);

                                  //$("#"+id).next().css( "background", "yellow" );
                                  //alert(html);
                             
                                    }
                              });  //ajax ends here 

                    var effect = 'slide';
                    var options = { direction: $('.mySelect').val() };
                    var duration = 500;
                    $("body").append(''); $(".comment-pop").show("medium"); 
                    $(".overlay").show("medium");
                    $(".close").click(function(e) { 
                    $(".comment-pop,.overlay").hide("medium"); 
                    }); 
                    
                    $( "#header_nav" ).addClass( "high-z" );
                }); 
              });  
			 
			  //script for the timeline comment img popup functionality
              $(document).ready(function() {       
                $(".popimg").click(function(e) {

                  var id=$(this).parent().attr('id');
                  //alert(id);
                   $("#img_pop_post_id").html(id);
                  
                   var post_image =$(this).children().attr('src');
                   $('.pop_comment_img').attr('src',post_image);
                    post_data=$(this).next().html();;
                    $("#image_popup_status").html(post_data);
                          //$("#thispost_id").html(id);
                    var source=$(this).parent().attr('name');
                    var effect = 'slide';
                    var options = { direction: $('.mySelect').val() };
                    var duration = 500;
                    $("body").append(''); $(".img-post").show("medium"); 
                    $(".overlay").show("medium");
                    $(".close").click(function(e) { 
                    $(".img-post, .overlay").hide("medium"); 
                    }); 
                }); 
              });   
			  
			  //posting comment on the timeline page popup functionality

             $(document).ready(function(){
                $('.write-reply').click(function(){
                    var comment=$('#write_comment').val();
                   // alert(comment);
                    if(comment!=''){
                       var post_id=$("#thispost_id").html();
                  var userid=$("#myid").html();
                  //alert(post_id);
                    if(userid!=''){
                             
                             var data={
                          "post_id":post_id,
                          "userid":userid,
                          "comment":comment
                          };

                            $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/user_comment_popup",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                              //alert(html);
                              $("#all_comments").html(html);
                              //var insertid=html;
                             $("#write_comment").val("");
                              /*if(insertid>0)
                              {
                                alert(insertid)
                              }
                              else
                              {
                                alert("error please try again");
                              }
                              */
                                  }
                          });
						   }
                    else{
                      alert("please login first");
                    }
                    }
                    else{
                      alert("Please fill your comment");
                    }

                });
            });
			
			//posting comment on the timeline page on the image popup functionality

             $(document).ready(function(){
                $('.img-popup-comment').click(function(){
                    var comment=$('#popup_comment').val();
                    //alert(comment);
                    if(comment!=''){
                       var post_id=$("#img_pop_post_id").html();
                  var userid=$("#myid").html();
                  //alert(post_id);
                    if(userid!=''){
                             
                             var data={
                          "post_id":post_id,
                          "userid":userid,
                          "comment":comment
                          };

                            $.ajax({
                            type: "POST",
                            url: ""+base_url+"Ajax/user_comment_popup",
                            //dataType: 'json',
                            data: data,
                            success: function(html){
                              //alert(html);
                              $("#image_popup_comments").html(html);
                              //var insertid=html;
                             $("#popup_comment").val("");
                              /*if(insertid>0)
                              {
                                alert(insertid)
                              }
                              else
                              {
                                alert("error please try again");
                              }
                              */

                                  }
                          });
                    }
                    else{
                      alert("please login first");
                    }
                    }
                    else{
                      alert("Please fill your comment");
                    }

                });
            });
		  
          $(document).ready(function(){
      $('.comment-sectn').click(function(){
          //alert("hello");
          var dis= $(this).parent().parent().parent().parent().children('.write-comment').is(':visible');
          //alert(dis);
          if(dis==false){
           // alert("ok");
$('.fwd-social-icons').hide();
           $('.write-comment').hide();
              $(this).parent().parent().parent().parent().children('.write-comment').slideDown();
            }
            else{
              ///alert("not ok");
              $(this).parent().parent().parent().parent().children('.write-comment').slideUp();
            }
      });
});

/* script to hide show try now form */
 $(document).ready(function(){
   $(".test-drive-btn").click(function(){
       $(".try-now-con").slideToggle( "slow" );
$(".try-now-con").css("margin-top", "-100px");
       $('html,body').animate({
       scrollTop: $(".content_headline-categorypg").offset().top},
       'slow');
   });
});

 $(document).ready(function(){
   $(".try-now-tab").click(function(){
       $(".try-now-con").slideToggle( "slow" );
       $(".try-now-con").css("margin-top", "-100px");
   });
});

/* ends script to hide show try now form */

/*script for product page slider expand icon width switch */
$(document).ready(function(){
   $(".expand-slide-img").click(function(){
       $(".product-carousel-inner").toggleClass("fullwidth");
   });
});

//Place this plugin snippet into another file in your applicationb
(function ($) {
   $.toggleShowPassword = function (options) {
       var settings = $.extend({
           field: "#password",
           control: "#toggle_show_password",
       }, options);

       var control = $(settings.control);
       var field = $(settings.field)

       control.bind('click', function () {
           if (control.is(':checked')) {
               field.attr('type', 'text');
           } else {
               field.attr('type', 'password');
           }
       })
   };
}(jQuery));

$(document).ready(function(){
  $("#btn-sendform").click(function(){
      $(".sendquery-form").slideToggle( "slow" );
      $(".sendquery-form").css("margin-top", "-100px");
      $('html,body').animate({
      scrollTop: $(".btn-sectn").offset().top},
      'slow');
  });
});

//for the signup page hide and show

  $(document).ready(function(){
        //for the ques 1
        $("#start").click(function(){
          $("#upload").show();
		  $('html, body').animate({
					scrollTop: $("#upload").offset().top
				}, 1000);
          //alert("hi");     
        });
  });
  
 //for multiple select category
  $(document).ready(function(){
	         //for the ques 1
		//var category = "";
		var category = [];
		var keepyou = "";
		 keepyou = new Array(); 
		var getinfo = "";
		 getinfo = new Array(); 
		var fname = "";
		var lname = "";
	    var email = "";
		var username ="";
	    var password ="";
		var allcat = "";
		var subtitlefield = "";
		subtitlefield = new Array();
		var auth =$("#auth").val();
		var country ="";
	    var Occupation ="";
	    var city = "";
    
	 $("#abcd").click(function(){
	    username =$("#username123").val();
	    password =$("#password123").val();
	  
	  if(username== '' )
	  {
		  //alert(fname);
	   alert("Please Enter username ");
	  //document.getElementById('msg1').innerHTML="Please Enter First Name";
	 // document.myform.fname.focus();
	  return false;
	  }
	  
	  if(password== '' )
	  {
		  //alert(fname);
	   alert("Please Enter password");
	  //document.getElementById('msg1').innerHTML="Please Enter First Name";
	 // document.myform.fname.focus();
	  return false;
	  }	  
	  else{
                 $("#ques1").show();
				$('html, body').animate({
					scrollTop: $("#ques1").offset().top
				}, 1000);
                   // window.location.hash = '#ques2';
                     $("#ques1").focus();
            }
          //alert("hi");     
        });
	  
     $("#next1").click(function(){

			allcat =$("#allcat").val(); 
            var inputElems = document.getElementsByName("cat[]"),
            count = 0;         
            for (var i=0; i<inputElems.length; i++) 
             {       
                  if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) 
                  {
                      count++;
                    category.push(inputElems[i].value);
                  }
            }
            //alert(count + category);
            if(count < 3 )
            {
            //document.getElementById('msg1').innerHTML="Please select atleast 3 Luxry Intrest";
                  alert("Please select atleast 3 Luxry Intrest");
                  //document.myform.category.focus();
                  return false;
            }
            else{
                 $("#ques2").show();
				$('html, body').animate({
					scrollTop: $("#ques2").offset().top
				}, 1000);
                   // window.location.hash = '#ques2';
                     $("#ques2").focus();
            }
     });

     $("#next2").click(function()
	 {
		var keepyouElems = document.getElementsByName("keepyou[]");
		var getinfoelems = document.getElementsByName("info");
		keepyoucount = 0; 
		getinfocount = 0;   
		for (var i=0; i<keepyouElems.length; i++) 
	   {       
		  if (keepyouElems[i].type == "checkbox" && keepyouElems[i].checked == true) 
		  {
			  keepyoucount++;
			  keepyou.push(keepyouElems[i].value);
		  }
	  }
	  //alert(keepyoucount + keepyou);
	  if(keepyoucount < 1 )
		{
		//document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
		alert("Please select atleast 1 Keep you");
		return false;
		}
		
	 for (var i=0; i<getinfoelems.length; i++) 
	   {       
		  if (getinfoelems[i].type == "radio" && getinfoelems[i].checked == true) 
		  {
			  getinfocount++;
			  getinfo.push(getinfoelems[i].value);
		  }
	  }
	  //alert(getinfocount + getinfo);
	  if(getinfocount == 0 )
		{
		//document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
		alert("Please select atleast 1 information");
		return false;
		}	
		
		else
		{
			$("#ques3").show();
			 $('html, body').animate({
			 scrollTop: $("#ques3").offset().top
			 }, 1000);
			$("#ques3").focus();
             //window.location.hash = '#ques3';        
	 }
		/* information to be given end*/

 });
/* keepyou end*/          

      $("#next3").click(function(){
		   
	  //subtitlefield = ""
	  var subtitlefieldelems = document.getElementsByName("subtitle");
	  subtitlecount = 0;
	   fname =$("#fname").val();
	  lname =$("#lname").val();
	  email =$("#email").val();
	  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	  //var address = document.myform.email.value;
	  //len = document.myform.subtitle.length
	  for (var i=0; i<subtitlefieldelems.length; i++) 
	   {       
		  if (subtitlefieldelems[i].type == "radio" && subtitlefieldelems[i].checked == true) 
		  {
			  subtitlecount++;
			  subtitlefield.push(subtitlefieldelems[i].value);
		  }
	  }
	  //alert(subtitlecount + subtitlefield);
	  if(subtitlecount == 0 )
		{
		//document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
		alert("Please select atleast 1 subtitel");
		return false;
		}	

	  if(fname== '' )
	  {
		  //alert(fname);
	   alert("Please Enter First Name");
	  //document.getElementById('msg1').innerHTML="Please Enter First Name";
	 // document.myform.fname.focus();
	  return false;
	  }
	  if(lname== '' )
	  {
	  alert("Please Enter Last Name");
	 // document.getElementById('msg1').innerHTML="Please Enter Last Name";
	  document.myform.lname.focus();
	  return false;
	  }
	  if(email== '' )
	  {
	  alert("Please Enter email");
	 // document.getElementById('msg1').innerHTML="Please Enter email";
	  document.myform.email.focus();
	  return false;
	  }
	  
	  if(reg.test(email) == false) 
	  {
		alert("Invalid Email Address");
	   //document.getElementById('msg1').innerHTML="Invalid Email Address";
	   return false;
      }
	  //alert(dob);
	  else
		{
			//alert(dob);
			$("#ques7").show();
			 $('html, body').animate({
			 scrollTop: $("#ques7").offset().top
			 }, 1000);
			 $("#ques7").focus();
              //window.location.hash = '#ques7';
        }
     });
	 
	 $("#next4").click(function(){
	    country =$("#country").val();
	    Occupation =$("#Occupation").val();
	    city =$("#city").val();
		var dob =$("#datepicker1").val();
	  if(country== ''|| country == 'A' )
		{
		alert("Please Select Country");
		//document.getElementById('msg1').innerHTML="Please Select Country";
		document.myform.country.focus();
		return false;
		}
	  if(city== ''|| city == 'B' )
		{
		alert("Please Select City");
		//document.getElementById('msg1').innerHTML="Please Select City";
		document.myform.city.focus();
		return false;
		}
	 else{		
			$.ajax({
			 type: "POST",
			 url: ""+base_url+"main/signupdata?username=" + username + "&password=" + password + "&allcat=" + allcat + "&category=" + category + "&keepyou=" + keepyou + "&getinfo=" + getinfo + "&subtitlefield=" + subtitlefield + "&fname=" + fname + "&lname=" + lname + "&dob=" + dob + "&email=" + email + "&country=" + country + "&Occupation=" + Occupation + "&city=" + city + "&auth=" + auth,
			// url: ""+base_url+"Main/signupdata?fname=" + fname + "&lname=" + lname,
		     // data:data,
				//crossDomain:true,
			success: function(html){
			 alert("successfully");
			 //alert(html);
			window.location.href='"+base_url+"main/profile';
		   }
		});   
				 
		 }

	});	 
       
  })
// smart follow banner script sign up page
/*popup script of header settings */
$(document).ready(function() {       
 $(".settings-popup-btn").click(function(e) {
     var effect = 'slide';
     var options = { direction: $('.mySelect').val() };
     var duration = 500;
     $("body").append(''); $(".settings-popup").show("medium"); 
     $(".overlay").show("medium");
     $(".close").click(function(e) {
     $(".settings-popup, .overlay").hide("medium"); 
     }); 
 }); 
});

// script for the settings page
$(function() {
   $('.settings-blocks .edit').click(function() {
     $(this).parent().children("div").slideToggle("slow");
   });
});

//script to toggle icon in specification tab
$(document).ready(function(){ 
  $('.spec-headline').click(function(){
      $(this).find('i').toggleClass('fa-chevron-up');
      $(this).find('i').toggleClass('fa-chevron-down')
  });  
});


// JavaScript Document
