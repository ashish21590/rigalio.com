var base_url = 'http://localhost/rigalio.com/';

function init() {
    window.addEventListener('scroll', function (e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 300,
            nav = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(nav, "smaller");
        } else {
            if (classie.has(nav, "smaller")) {
                classie.remove(nav, "smaller");
            }
        }
    });
}
window.onload = init();
/* ends header onscroll script */
/*  header script to open searchbar */
$(document).on('click', '.search-icon', function () {
    $(".dropdown-search").slideToggle("slow");
});


/* ends header script to open searchbar */

/*popup script of header */
$(document).on('click', '.popup-btn', function () {
    $("body").append('');
    $(".popup").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".popup, .overlay").hide("medium");
    });
});

/*popup script ends of header */
/*popup script of header sign in via */
$(document).on('click', '.signin-popup-btn', function () {
    var effect = 'slide';
    var options = {direction: $('.mySelect').val()};
    var duration = 500;
    $("body").append('');
    $(".signin-popup").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".signin-popup, .overlay").hide("medium");
    });
});

/*popup script of header sign in via ends */

/* script to show hide more sociual icons */
$(document).ready(function () {
    /*$(".show-more").click(function(){
     $(".hidden-more-icons").slideToggle( "slow" );
     });
     $(".fwd-icon-sectn").click(function(){
     $(".hidden-more-icons").slideToggle( "slow" );
     });
     */
});

/* script to show hide more sociual icons ends  */
/* script to show hide fwd icons */
$(document).ready(function () {
    /*$("#fwd-id1").click(function(){
     $(".sec7").slideToggle( "slow" );
     });
     */
});
/* script to show hide fwd icons ends */

/* script to show hide hover cat follow brand button */


$(document).on('mouseover', '.following', function (event) {
    //event.stopPropagation();
    //$(this).hide();
    $(this).parent().find(".following").hide();
    $(this).parent().find(".unfollow").show();
    //$(this).parent().find( ".unfollow" ).show();
    /*   $('.unfollow').show();
     $('.following').hide();
     */
});
$(document).on('mouseleave', '.unfollow', function (event) {
    //event.stopPropagation();

    // unfollow

    if ($(this).parent().find('.follow').is(':visible')) {
        // $(this).parent().find( ".following" ).show();
        $(this).parent().find(".unfollow").hide();
    } else {
        $(this).parent().find(".following").show();
        $(this).parent().find(".unfollow").hide();
    }

});


/* script to show hide hover cat follow button ends */
//script to hide show the follow status of h1cat follow category
$(function () {
    //$(".following-cat").hide();
    //$(".unfollow-cat").hide();
    /*$('.follow-cat').click(function() {
     $(this).hide();
     $(this).next(".following-cat").show();
     });
     $('.following-cat').click(function() {
     $(this).hide();
     $(this).prev(".follow-cat").show();
     });*/

    $(document).ready(function () {
        $('.following-cat').hide();
    });

    $(document).on('mouseover', '.following-cat', function () {
        //$('.unfollow-cat').show();
        $(this).hide();
        $(this).parent().parent().find('.unfollow-cat').show();

    });

    $(document).on('mouseleave', '.unfollow-cat', function () {

        //setTimeout(function () {

        if ($(this).parent().parent().find('.follow-cat').is(':visible')) {


            $(this).hide();
            // $(this).parent().parent().find('.following-cat').show();
        }
        else {
            $(this).hide();
            $(this).parent().parent().find('.following-cat').show();
        }
        //  }, 3000);
        // $('').show();
    });

});
//script to hide show the follow status of h1cat ends


/* script to scroll to top icon */
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
/* script to scroll to top icon ends */
/* script to hide show subcat icons on cat page */
$(document).on('click', '.sub-cat-btn', function () {
    $(".hidden-cat-optns").slideToggle("slow");
});

/* script to hide show subcat icons on cat page ends */
/* category page follow btn script */
$(document).on('hover', '.category_pg .following', function () {
        $(this).addClass('unfollow')
    },
    function () {
        $(this).removeClass('unfollow')
    }
);

$(document).on('mouseover', '.category_pg .following', function () {
    $('.category_pg .following').text("UNFOLLOW");
});

$(document).on('mouseout', '.category_pg .following', function () {
    $('.category_pg .following').text("FOLLOWING");
});


$(function () {

    $(document).on('click', '.category_pg .normal', function () {
        $('.normal').hide();
        $('.following').show();
    });
    $(document).on('click', '.category_pg .following', function () {
        $('.following').hide();
        $('.normal').show();
    });

});
/* category page follow btn script ends */

/* placeholder in category page */
$(document).ready(function () {
    $('.textarea1').find("input[type=textarea], textarea").each(function (ev) {
        if (!$(this).val()) {
            $(this).attr("placeholder", "Type your answer here");
        }
    });
});
/* placeholder in category page ends */

/*  for the follow and crown logic*/
$(document).on('click', '.myan', function () {
    /*alert(this.id);
     var id=this.id;
     //alert($(this).find('a:first'));
     //var button = $(event.target).closest('button').css( "background-color", "red" );;
     console.log("You clicked on:", button);
     // $("button").children().css( "background-color", "red" );
     */
});

//for crown section


// for grtting the tab of feature
function gettab(featureid, productid) {

//alert(featureid);
//alert(productid);
    var data = {
        "productid": productid,
        "featureid": featureid
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "Ajax/get_specification",
        //dataType: 'json',
        data: data,
        success: function (html) {
            var me = html
            //alert(me);
            // $("#"+id).next().css( "background", "yellow" );
            //$(this).next().append(html);
            //$("#acc_res").html(html);//working

            $(".panel-default").after(html);

            console.log(html);

        }
    });  //ajax ends here

}
//fb login code
$(document).on('click', '.fb', function () {
    //alert("hi");
    var imagurl = $(this).parent().parent().parent().parent().parent().find("img").attr('src');
    //alert(imagurl);
    FB.ui(
        {
            method: 'feed',
            name: 'Rigalio',
            caption: "This is testing",
            description: (
                'Rigalio'
            ),
            link: "" + base_url,
            picture: +imagurl
        },
        function (response) {
            if (response && response.post_id) {
                //$("#me";
                //window.location.href="roadsafetywars.php";
                //alert("done");

                //window.open();

            } else {
                // alert('Post was not published.');

                // alert('https://lyxel.net/serious_biker/img/results/<?php //echo $img_result;?>');
            }
        }
    );


});


//for the searchig of the text
function search() {
    alert("hi");
    var serach_text = $('#serach_text').val();
    if (serach_text != '') {
        window.location.href = "" + base_url + "main/search/" + serach_text;
    }
    else {
        $("#alert-msg").text("please write something");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("please write something");
    }


}

//for the following brand
$(document).on('click', '.follow', function (event) {
    event.stopPropagation();
    var userid = $("#myid").html();
    var mythis = this;
    //alert(mythis);
    if (userid != '') {
        var brandid = $(this).parent().attr('id');
        //alert(brandid);
        var data = {
            "brandid": brandid,
            "userid": userid
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/follow_brand",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    $(mythis).parent().find(".following").show();
                    $(mythis).parent().find(".follow").hide();
                    $("#alert-msg").text(" You have chosen to follow the brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("Followed");
                }
                else {

                    $(mythis).parent().find(".follow").hide();
                    $(mythis).parent().find(".following").show();
                    $("#alert-msg").text("You are already following this brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already following');
                }
            }
        });  //ajax ends here
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }

});


//for unfollowing the brand
$(document).on('click', '.unfollow', function (event) {
    event.stopPropagation();
    var userid = $("#myid").html();
    var mythis = this;
    if (userid != '') {
        var brandid = $(this).parent().attr('id');
        //alert(brandid);
        var data = {
            "brandid": brandid,
            "userid": userid
        };

        $.ajax({
            type: "POST",

            url: "" + base_url + "Ajax/unfollow_brand",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val == 0) {
                    $(mythis).parent().find(".following").hide();
                    $
                    $(mythis).parent().find(".follow").show();
                    $(mythis).parent().find(".unfollow").hide();
                    $(mythis).hide();

                    $("#alert-msg").text("You have chosen to unfollow the brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("unfollowed");
                    //$('.follow').show();

                    //$('.following').hide();

                }
                else {
                    $("#alert-msg").text("You are already following this brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already following');
                    $(mythis).parent().find(".unfollow").hide();
                    $(mythis).parent().find(".following").hide();
                    $(mythis).parent().find(".following").hide();
                }
            }
        });  //ajax ends here
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }


});


// for the following category
$(document).on('click', '.follow-cat', function (event) {
    //alert("category click");
    var mythis = this;
    var user_id = $("#myid").html();

    if (user_id != '') {
        var cat_id = $(this).parent().parent().attr('id');

        // alert(cat_id);
        var data = {
            "categoryid": cat_id,
            "userid": user_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/follow_category",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    // $('.following-cat').show();
                    //$('.follow-cat').hide();
                    $(mythis).parent().find('.following-cat').show();
                    $(mythis).parent().find('.follow-cat').hide();
                    $("#alert-msg").text("You have chosen to follow category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("followed category");

                }


                else {


                    $(mythis).parent().find('.following-cat').show();
                    $(mythis).parent().find('.follow-cat').hide();
                    //$('.follow-cat').hide();
                    $("#alert-msg").text("You are already following this category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already following');
                }
            }
        });  //ajax ends here
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }
});


//for the unfollowing category
$(document).on('click', '.unfollow-cat', function () {
    var user_id = $("#myid").html();
    var mythis = this;

    if (user_id != '') {
        var cat_id = $(this).parent().attr('id');

        //alert(cat_id);
        var data = {
            "categoryid": cat_id,
            "userid": user_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/unfollow_category",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {
                    $("#alert-msg").text("You are already following this category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert(" already following category");
                    $(mythis).parent().find('.following-cat').hide();
                    $(mythis).parent().find('.follow-cat').show();

                }
                else {

                    // $('.following-cat').hide();
                    //$('.follow-cat').show();
                    $(mythis).parent().find('.following-cat').hide();
                    $(mythis).parent().find('.follow-cat').show();
                    $("#alert-msg").text("You have chosen to un-follow the category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('un follow');
                    //  $(".following-cat").hide();
                    //$(".follow-cat").show();

                }
            }
        });  //ajax ends here
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }

});


//category page follow category function
$(document).on('click', '.mycatfun', function () {
    var cat_id = this.id;
    //alert(cat_id);
    var user_id = $("#myid").html();
    // alert(user_id);
    if (user_id != '') {
        //  alert(cat_id);
        var data = {
            "categoryid": cat_id,
            "userid": user_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/follow_category",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    // $('.following-cat').show();
                    //$('.follow-cat').hide();
                    //$(mythis).parent().find('.following-cat').show();
                    //$(mythis).parent().find('.follow-cat').hide();
                    $("#alert-msg").text("You have chosen to follow the category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("followed category");

                }
                else {


                    //$(mythis).parent().find('.following-cat').show();
                    //$(mythis).parent().find('.follow-cat').hide();
                    //$('.follow-cat').hide();
                    $("#alert-msg").text("You are already following this category");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already following');
                }
            }
        });  //ajax ends here

    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("login first");
    }
});

// functionality of the brand page follow button
//brand-page-follow
$(document).on('click', '.brand-page-follow', function () {
    var brandid = this.id;
    var userid = $("#myid").html();
    if (userid != '') {
        //var brandid=$(this).parent().attr('id');
        //alert(brandid);
        var data = {
            "brandid": brandid,
            "userid": userid
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/follow_brand",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $("#alert-msg").text("You have chosen to follow the brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("Followed");
                }
                else {

                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $("#alert-msg").text("You are already following this brand");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already following');
                }
            }
        });  //ajax ends here
    }
    else {
        $("#alert-msg").text("You are already following this brand");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }
});

//plus button for the home page
/* script to show hide hover cat icon */
$(function () {
    $(document).on('click', '.cat-zoom-icon', function (event) {
        event.stopPropagation();
        //$(this).hide();
        //$(this).next('.cat-plus-icon').show();
        //$('.cat-plus-icon').show();
    });
    $(document).on('click', '.cat-plus-icon', function (event) {
        event.stopPropagation();
        //$(this).hide();
        //$(this).prev(".cat-zoom-icon").show();
        //$('.cat-zoom-icon').show();
    });
    $(document).on('click', '.cat-minus-icon', function (event) {
        event.stopPropagation();
        //$(this).hide();
        //$(this).next('.cat-plus-icon').show();
        //$('.cat-plus-icon').show();
    });
});
/* script to show hide hover cat icon ends */


$(document).on('click', '.fa-plus', function (e) {

    e.preventDefault();
    var mythis = this;
    var productid = this.id;
    var userid = $("#myid").html();
    var subcat_id = $(this).parent().attr('id');
    //alert(productid);
    if (userid != '') {

        var data = {
            "productid": productid,
            "userid": userid,
            "subcat_id": subcat_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/save_for_later",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $(mythis).parent().parent().find('.cat-zoom-icon').hide();
                    $(mythis).parent().parent().find('.cat-plus-icon').show();
                    $("#alert-msg").text("This product is added to your collections");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("saved for later");
                    return false;

                }
                else {

                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $(mythis).parent().parent().find('.cat-zoom-icon').hide();
                    //$().prev(".cat-plus-icon").show();
                    $(mythis).parent().parent().find('.cat-plus-icon').show();
                    $("#alert-msg").text("This product has already been added to your collections");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('already saved');
                    return false;
                }
            }
        });
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");

        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("please login first");
    }

});


// minus button for the home page
$(document).on('click', '.fa-minus', function (e) {

    e.preventDefault();
    $("#alert-msg").text("This product is removed to your collections");
    $(".alert-sectn").show("medium");
    setTimeout(function () {
        $('.alert-sectn').fadeOut();
    }, 3000);
    //alert("click minus");
    //alert("hi");
    var mythis = this;
    var productid = this.id;
    var userid = $("#myid").html();
    var subcat_id = $(this).parent().attr('id');
    //alert(productid);
    if (userid != '') {

        var data = {
            "productid": productid,
            "userid": userid,
            "subcat_id": subcat_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/minus_for_later",
            //dataType: 'json',
            data: data,
            success: function (html) {
                var get_val = html;
                //checking the response 0 done following 1 done already
                if (get_val > 0) {

                    $(mythis).hide();
                    $(mythis).next('.cat-plus-icon').show();


                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $("#alert-msg").text("This product is added to your collections");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert("saved for later");
                    return false;
                }
                else {
                    // $(mythis).hide();
                    //$(mythis).next('.cat-plus-icon').show();

//$(mythis).parent().hide();
                    //$().prev(".cat-plus-icon").show();
                    $(mythis).parent().parent().find('.cat-zoom-icon').show();

                    $(mythis).parent().parent().find('.cat-plus-icon').hide();

                    //$(mythis).parent().children( ".follow" ).hide();
                    //$(mythis).parent().children( ".following" ).show();
                    $("#alert-msg").text("This product is removed to your collections");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert('removed from save for later');
                    return false;
                }
            }
        });
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("please login first");
    }

});

//for the share button menu

$(document).on('click', '.fwd-icon-sectn', function () {
    //alert("hello");
    var dis = $(this).parent().parent().parent().parent().children('.fwd-social-icons').is(':visible');
    //alert(dis);
    if (dis == false) {
        // alert("ok");
        $('.fwd-social-icons').hide();
        $('.write-comment').hide();
        $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideDown();
    }
    else {
        ///alert("not ok");
        $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideUp();
    }
});


//for the share more button functionality

$(document).ready(function () {
    $(this).parent().parent().children('.hidden-more-icons').hide();
    $(document).on('click', '.show-more', function () {
        var dis = $(this).parent().parent().children('.hidden-more-icons').is(':visible');
        if (dis == false) {
            $(this).parent().parent().children('.hidden-more-icons').slideDown();

        }
        else {
            $(this).parent().parent().children('.hidden-more-icons').slideUp();
        }

    });
});
//for the comment section posting event

$(document).on('click', '.post-comment-btn', function () {
    // alert('hi');
    var comment = $('#comment').val();
    // alert(comment);
    if (comment != '') {
        var productid = $("#productid").html();
        var userid = $("#myid").html();
        //  alert(productid);
        if (userid != '') {

            var data = {
                "productid": productid,
                "userid": userid,
                "comment": comment
            };

            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/user_comment",
                //dataType: 'json',
                data: data,
                success: function (html) {

                    $("#comment_section").html(html);
                    var insertid = html;
                    // $('#comment').empty();
                    $('#comment').val('');
                    /*if(insertid>0)
                     {
                     alert(insertid)
                     }
                     else
                     {
                     alert("error please try again");
                     }
                     */

                }
            });
        }
        else {
            $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("You need to login first to pursue any engagement on Rigalio");
        }
    }
    else {
        $("#alert-msg").text("Please fill your comment");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please fill your comment");
    }
});


//comment section crown

$(document).on('click', '.reply-no', function () {
    var commentid = this.id;
    var user_id = $("#myid").html();
    //alert(user_id);
    if (user_id != '') {

        var data = {
            "commentid": commentid,
            "userid": user_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/like_comment",
            //dataType: 'json',
            data: data,
            success: function (html) {
                alert(html);
                //$("#comment_section").append(html);
                // var insertid=html;
            }
        });
    }
    else {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert('You need to login first to pursue any engagement on Rigalio');
    }
});


//for the image resize in a box
$(document).on('each', '#abc img', function () {
    var maxwidth = 300;
    var ratio = 0;
    var img = $(this);
    if (img.width() > maxwidth) {
        ratio = img.height() / img.width();
        img.css('height', (maxwidth * ratio));
        img.css('width', maxwidth);
    }
});

//for the profile comment post
/*
 $(document).ready(function(){
 $("#post_comment").click(function(){
 alert("i am in post comment");
 var post_data=$("#post").val();
 var user_id=$("#myid").html();
 alert(user_id);

 if(post_data!=''){
 alert(post);
 $("#post").empty();

 var data={
 "post":post_data,
 "userid":user_id
 };

 $.ajax({
 type: "POST",
 url: ""+base_url+"ajax/user_post",
 //dataType: 'json',
 data: data,
 success: function(html){
 // alert(html);

 $( "li" ).removeClass("timeline-inverted");
 $("#main_content").prepend(html);

 $("#timeline ul li:nth-child(odd)").addClass('timeline-inverted');


 }
 });
 }
 else{
 alert("write post");
 }
 });
 });

 */
//for uploading the user status and image
$(function () {
    $(document).on('change', '#userfile', function () {
        //alert("hi");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function () { // set image data as background of div
                $("#user_files").css("background-image", "url(" + this.result + ")");
            }
        }
    });
});

//for sending following request to the user
$(document).on('click', '.follow-profile-btn', function () {
    var myid = $("#myid").html();
    var friendid = $("#friend_id").html();
    //alert(myid+friendid);
    // alert("hi i am in friend following");
    if (friendid != 'undefined') {
        var data = {
            "myid": myid,
            "friendid": friendid
        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "Ajax/user_follow",
            //dataType: 'json',
            data: data,
            success: function (html) {
                alert(html);
            }
        });
    }
    else {
        $("#alert-msg").text("You can not follow yourself");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        // alert("You can not follow yourself.");
    }
});

//for posting status and image upload
$(function () {
    var filename = $('input[name=userfile]');
    var form_url = $("#upload_file").attr('action');

    $(document).on('click', '#post_comment1', function (event) {
        //alert("please run");

        $("#loader_img").show();
        var post = $("#user_status_post").val();
        var user_id = $("#myid").html();
        var visibility=$(".privacy-select :selected").val();
        //alert(post);
        var fileupload = filename[0].files[0];
        if (fileupload != 'undefined') {

            $('body').addClass('loading');
            var formData = new FormData();
            formData.append('userfile', fileupload);
            formData.append('post', post);
            formData.append('user_id', user_id);
            formData.append('visibility', visibility);


            $.ajax({
                url: form_url,
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (html) {

                    $("li").removeClass("timeline-inverted");
                    $("#main_content").prepend(html);
                    $("ul li:nth-child(odd)").addClass('timeline-inverted');
                    $("#post").val(" ");
                    $("#user_files").fadeOut();
                    $("#loader_img").hide();
                    $("#user_status_post").val(" ");
                    $('body').find('.modal-backdrop').fadeOut();
                    $("#img-upload-pop").hide();
                    $(".img-upload-pop").show();
                    $('body').removeClass('loading');
                    $(".img-uploaded").hide();
                    $('#img-upload-pop').modal('hide');
                }

            });
            //console.log(fileupload);
        }
    });
});

/* placeholder in category page ends */
$(document).on('click', '.crown-sectn1', function (event) {
    var id = this.id;
    var currentvla = this;
    var userid = $("#myid").html();
    var getcount = $(this).find('#crowncount').html();
    if (userid == '') {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }
    else {
        //alert(id);
        if (id) {
            var data = {
                "post_id": id,
                "userid": userid
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/post_crown",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    var crown = html;
                    $("#alert-msg").text("You have crowned this");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    if (crown == '1') {
                        $("#alert-msg").text("You have already crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("Aleady is in crown");
                    }
                    else {
                        //var value = 1;
                        $("#alert-msg").text("You have crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("addes a crown");
                        var newno = parseInt(getcount) + parseInt(1);
                        //alert(newno);
                        //$("#crowncount").html(newno);
                        var count_crown=$(".pop_count").html();
                        var current_crown_count=parseInt(count_crown) + parseInt(1);
                        $(currentvla).find('#crowncount').html(newno);
                        $('.increment_comment').parent().parent().find('.category_options').find('#crowncount').html(current_crown_count);
                    }
                    //alert(html);
                    //$("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                }
            });  //ajax ends here
        }
    } //else ends here

});


//post crown functionality on the user friend profile page

$(document).on('click', '.crown-sectn_friend', function (event) {
    var id = this.id;


    var userid = $("#myid").html();


    var friend_id = $("#friend_id").html();
    alert(friend_id);
    if (userid == '') {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        // alert("Please login first");
    }
    else {
        //alert(id);
        if (id) {
            var data = {
                "post_id": id,
                "userid": userid,
                "friend_id": friend_id
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "/ajax/post_friend_crown",
                //dataType: 'json',
                data: data,

                success: function (html) {
                    alert(html);
                    //$("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                }
            });  //ajax ends here
        }
    } //else ends here

});

//script for the timeline comment popup
$(document).ready(function () {
    var post_data = "";
    $(document).on('click', '.tag,.mytag', function () {

//console.log("test");
        $('body').addClass('loading');
        var id = $(this).parent().attr('id');
        $("#thispost_id").html(id);
        var source = $(this).parent().attr('name');
        post_data = $(this).html();
        var user_id_popup = $(this).parent().parent().parent().attr('id');
        // console.log(user_id_popup);
        if (!user_id_popup) {

            user_id_popup = $("#myid").html();

        }
        var data = {
            "user_id": user_id_popup
        }
        $.ajax({
            type: "POST",
            url: "" + base_url + "ajax/get_user",
            //dataType: 'json',
            data: data,
            success: function (html) {
                // alert(html);
                //$("#user_all_comments").html(html);
                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);

                var obj = $.parseJSON(html);
                //console.log(obj[0].firstname);
                $('.comment-popup-picture').attr('src', obj[0].profile_picture);
                $('.comment-popup-username').html(obj[0].firstname);


                console.log(html);
            }
        });  //ajax ends here
        $("#user_post_show").html(post_data);
        var data = {
            "post_id": id
        }
        $.ajax({
            type: "POST",
            url: "" + base_url + "Main/get_comments",
            //dataType: 'json',
            data: data,
            success: function (html) {
                //alert(html);
                $("#all_comments").html(html);
                $('body').removeClass('loading');

                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);

            }
        });  //ajax ends here

        var effect = 'slide';
        var options = {direction: $('.mySelect').val()};
        var duration = 500;
        $("body").append('');
        $(".comment-pop").show("medium");
        $(".overlay").show("medium");
        $(".close").click(function (e) {
            $(".comment-pop,.overlay").hide("medium");
        });

        $("#header_nav").addClass("high-z");
    });
});

//script for the timeline comment img popup functionality
$(document).on('click', '.popimg', function (e) {
    var id = $(this).parent().attr('id');
    var mythis=this;
//alert(id);
    $('body').addClass('loading');
    $(this).addClass('increment_comment');
    var user_id_popup = "";
    user_id_popup = $(this).parent().parent().parent().attr('id');
alert(user_id_popup);
    var friend_id = $('#friend_id').html();
///console.log(user_id_popup);
    if (user_id_popup=="undefined") {
        alert("hi");
        user_id_popup = $("#myid").html();

    }


alert(user_id_popup);
    var data = {
        "user_id": user_id_popup
    }
    $.ajax({
        type: "POST",
        url: "" + base_url + "ajax/get_user",
        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
            //$("#user_all_comments").html(html);
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);

            var obj = $.parseJSON(html);
            //console.log(obj[0].firstname);
            //  console.log(obj);
            $('.popimg-profile-pic').attr('src', obj[0].profile_picture);
            $('.popimg-profile-name').html(obj[0].firstname);
//alert(obj['firstname']);

            console.log(html);
        }
    });  //ajax ends here
    var user_name = $(this).parent().parent().find('h6').html();
    var user_image = $(this).parent().parent().find('img').attr('src');
    $("#img_pop_post_id").html(id);

    var post_image = $(this).children().attr('src');
    $('.pop_comment_img').attr('src', post_image);
    post_data = $(this).next().html();
    ;
    $("#image_popup_status").html(post_data);
    //$("#thispost_id").html(id);
    var data = {
        "post_id": id
    }
    //alert(data);
    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_post_id",
        data: data,
        success: function (html) {
            $("#image_countpopup_comments").html(html);
        }
    });  //ajax ends here

    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_comments",
        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
            $("#image_popup_comments").html(html);
            $('body').removeClass('loading');
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);
        }
    });  //ajax ends here
    var source = $(this).parent().attr('name');
    var effect = 'slide';
    var options = {direction: $('.mySelect').val()};
    var duration = 500;
    $("body").append('');
    $(".img-post").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".img-post, .overlay").hide("medium");
    });
});

//posting comment on the timeline page popup functionality

$(document).on('click', '.write-reply', function () {
    var comment = $('#write_comment').val();
    // alert(comment);
    if (comment != '') {
        var post_id = $("#thispost_id").html();
        var userid = $("#myid").html();
        //alert(post_id);
        if (userid != '') {

            var data = {
                "post_id": post_id,
                "userid": userid,
                "comment": comment
            };

            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/user_comment_popup",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    //alert(html);
                    $("#all_comments").html(html);
                    //var insertid=html;
                    $("#write_comment").val("");
                    /*if(insertid>0)
                     {
                     alert(insertid)
                     }
                     else
                     {
                     alert("error please try again");
                     }
                     */
                }
            });
        }
        else {
            $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            // alert("please login first");
        }
    }
    else {
        $("#alert-msg").text("Please fill your comment");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please fill your comment");
    }

});


//posting comment on the timeline page on the image popup functionality

$(document).on('click', '.img-popup-comment', function () {
    var comment = $('#popup_comment').val();
    //alert(comment);
    if (comment != '') {
        var post_id = $("#img_pop_post_id").html();
        var userid = $("#myid").html();
        //alert(post_id);
        if (userid != '') {

            var data = {
                "post_id": post_id,
                "userid": userid,
                "comment": comment
            };

            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/user_comment_popup",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    //alert(html);
                    $("#image_popup_comments").html(html);
                    //var insertid=html;
                    $("#popup_comment").val("");

                    var comment_count = $("#countcomment").html();
                    var count_crown=$(".pop_count").html();
                    var current_crown_count=parseInt(count_crown) + parseInt(1);
                    var current_count = parseInt(comment_count) + parseInt(1);
                    $("#countcomment").html(current_count);
                    $('.increment_comment').parent().parent().find('.category_options').find('.text').html(current_count);



                    /*if(insertid>0)
                     {
                     alert(insertid)
                     }
                     else
                     {
                     alert("error please try again");
                     }
                     */
                }
            });
        }
        else {
            $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("please login first");
        }
    }
    else {
        $("#alert-msg").text("Please fill your comment");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please fill your comment");
    }
});

/* script to hide show try now form */
$(document).on('click', '.test-drive-btn', function () {
    $(".try-now-con").slideToggle("slow");
    $(".try-now-con").css("margin-top", "-100px");
    $('html,body').animate({
            scrollTop: $(".content_headline-categorypg").offset().top
        },
        'slow');
});

$(document).on('click', '.try-now-tab', function () {
    $(".try-now-con").slideToggle("slow");
    $(".try-now-con").css("margin-top", "-100px");
});

/* ends script to hide show try now form */

/*script for product page slider expand icon width switch */
$(document).on('click', '.expand-slide-img', function () {
    $(".product-carousel-inner").toggleClass("fullwidth");
});


//Place this plugin snippet into another file in your applicationb
(function ($) {
    $.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };
}(jQuery));

$(document).on('click', '#btn-sendform', function () {
    $(".sendquery-form").slideToggle("slow");
    $(".sendquery-form").css("margin-top", "-100px");
    $('html,body').animate({
            scrollTop: $(".btn-sectn").offset().top
        },
        'slow');
});


//for the signup page hide and show
$(document).on('click', '#start', function () {
    //for the ques 1
    $("#upload").show();
    $('html, body').animate({
        scrollTop: $("#upload").offset().top
    }, 1000);
    //alert("hi");
});

//for multiple select category
$(document).ready(function () {

    var category = [];
    var keepyou = "";
    keepyou = new Array();
    var getinfo = "";
    getinfo = new Array();
    var fname = "";
    var lname = "";
    var email = "";
    var username = "";
    var password = "";
    var allcat = "";
    var subtitlefield = "";
    subtitlefield = new Array();
    var auth = $("#auth").val();
    var country = "";
    var Occupation = "";
    var city = "";
    var dob = "";
    var filename = "";

    $(document).on('click', '#abcd', function () {
        username = $("#username123").val();
        password = $("#password123").val();

        if (username == '') {
            //alert(fname);
            $("#alert-msg").text("Please choose your username");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter username ");
            //document.getElementById('msg1').innerHTML="Please Enter First Name";
            // document.myform.fname.focus();
            return false;
        }

        if (password == '') {
            //alert(fname);
            $("#alert-msg").text("Please type in your chosen password");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter password");
            //document.getElementById('msg1').innerHTML="Please Enter First Name";
            // document.myform.fname.focus();
            return false;


        }
        else {
            $("#ques1").show();
            $('html, body').animate({
                scrollTop: $("#ques1").offset().top
            }, 1000);
            // window.location.hash = '#ques2';
            $("#ques1").focus();
        }
        //alert("hi");
    });

    $(document).on('click', '#next1', function () {
        allcat = $("#allcat").val();
        var inputElems = document.getElementsByName("cat[]"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
                category.push(inputElems[i].value);
            }
        }
        //alert(count + category);
        if (count < 3) {
            //document.getElementById('msg1').innerHTML="Please select atleast 3 Luxry Intrest";
            $("#alert-msg").text("Please select at least three luxury interests");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            // alert("Please select atleast 3 Luxry Intrest");
            //document.myform.category.focus();
            return false;
        }
        else {
            $("#ques2").show();
            $('html, body').animate({
                scrollTop: $("#ques2").offset().top
            }, 1000);
            // window.location.hash = '#ques2';
            $("#ques2").focus();
        }
    });

    $(document).on('click', '#next2', function () {
        var keepyouElems = document.getElementsByName("keepyou[]");
        var getinfoelems = document.getElementsByName("info");
        keepyoucount = 0;
        getinfocount = 0;
        for (var i = 0; i < keepyouElems.length; i++) {
            if (keepyouElems[i].type == "checkbox" && keepyouElems[i].checked == true) {
                keepyoucount++;
                keepyou.push(keepyouElems[i].value);
            }
        }
        //alert(keepyoucount + keepyou);
        if (keepyoucount < 1) {
            //document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
            $("#alert-msg").text("Please select at least one way to maintain correspondence");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please select atleast 1 Keep you");
            return false;
        }


        for (var i = 0; i < getinfoelems.length; i++) {
            if (getinfoelems[i].type == "radio" && getinfoelems[i].checked == true) {
                getinfocount++;
                getinfo.push(getinfoelems[i].value);
            }
        }
        //alert(getinfocount + getinfo);
        if (getinfocount == 0) {
            //document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
            $("#alert-msg").text("Please Enter Your Frequency Of Information");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please select atleast 1 information");
            return false;
        }

        else {
            $("#ques3").show();
            $('html, body').animate({
                scrollTop: $("#ques3").offset().top
            }, 1000);
            $("#ques3").focus();
            //window.location.hash = '#ques3';
        }
        /* information to be given end*/

    });
    /* keepyou end*/
    $(document).on('click', '#next3', function () {
        //subtitlefield = ""
        var subtitlefieldelems = document.getElementsByName("subtitle");
        subtitlecount = 0;
        fname = $("#fname").val();
        lname = $("#lname").val();
        email = $("#email12").val();
        dob = $("#datepicker1").val();
        // alert(email);
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        for (var i = 0; i < subtitlefieldelems.length; i++) {
            if (subtitlefieldelems[i].type == "radio" && subtitlefieldelems[i].checked == true) {
                subtitlecount++;
                subtitlefield.push(subtitlefieldelems[i].value);
            }
        }
        //alert(subtitlecount + subtitlefield);
        if (subtitlecount == 0) {
            //document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
            $("#alert-msg").text(" Please select any one title");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please select atleast 1 subtitel");
            return false;
        }

        if (fname == '') {
            //alert(fname);
            $("#alert-msg").text("Please Enter First Name");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter First Name");
            return false;
        }
        if (lname == '') {
            $("#alert-msg").text("Please Enter Last Name");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter Last Name");
            // document.getElementById('msg1').innerHTML="Please Enter Last Name";
            return false;
        }
        if (email == '') {
            $("#alert-msg").text("Please Enter email");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter email");
            // document.getElementById('msg1').innerHTML="Please Enter email";
            return false;
        }

        if (reg.test(email) == false) {
            $("#alert-msg").text("Invalid Email Address");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Invalid Email Address");
            //document.getElementById('msg1').innerHTML="Invalid Email Address";
            return false;
        }

        if (dob == '') {
            $("#alert-msg").text("Please Enter Date of birth");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            // alert("Please Enter Date of birth");
            // document.getElementById('msg1').innerHTML="Please Enter email";
            return false;
        }

        //alert(dob);
        else {
            //alert(dob);
            $("#ques7").show();
            $('html, body').animate({
                scrollTop: $("#ques7").offset().top
            }, 1000);
            $("#ques7").focus();
            //window.location.hash = '#ques7';
        }
    });

    $(document).on('click', '#next4', function () {
        country = $("#country").val();
        state = $("#state").val();
        Occupation = $("#Occupation").val();
        city = $("#city").val();
        var filename = $('input[name=userfile]');

        if (country == '' || country == 'A') {
            $("#alert-msg").text("Please Select Country");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            return false;
        }
        if (state == '' || state == 'C') {
            $("#alert-msg").text("Please Select State");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            return false;
        }
        if (city == '' || city == 'B') {
            $("#alert-msg").text("Please Select City");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            return false;
        }
        else {
            $('#submit').on('click', function (event) {
                $('body').addClass('loading');
                var filename = $('input[name=userfile]');
                var fileupload1 = filename[0]['value'];
                // alert(fileupload1);
                if (fileupload1 == "") {
                    var formData = new FormData();
                    formData.append('username', username);
                    formData.append('password', password);
                    formData.append('allcat', allcat);
                    formData.append('category', category);
                    formData.append('keepyou', keepyou);
                    formData.append('getinfo', getinfo);
                    formData.append('subtitlefield', subtitlefield);
                    formData.append('fname', fname);
                    formData.append('lname', lname);
                    formData.append('dob', dob);
                    formData.append('email', email);
                    formData.append('country', country);
                    formData.append('state', state);
                    formData.append('Occupation', Occupation);
                    formData.append('city', city);
                    formData.append('auth', auth);
                    $.ajax({
                        type: 'post',
                        url: "" + base_url + 'main/signupwithoutimage',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (html) {
                            $('body').removeClass('loading');
                            // alert("successfully");
                            window.location.href = "" + base_url + 'profile';
                        }
                    });
                    //console.log(fileupload);
                }
                else {
                    var fileupload = filename[0].files[0];
                    //alert (fileupload);
                    var formData = new FormData();
                    formData.append('username', username);
                    formData.append('password', password);
                    formData.append('allcat', allcat);
                    formData.append('category', category);
                    formData.append('keepyou', keepyou);
                    formData.append('getinfo', getinfo);
                    formData.append('subtitlefield', subtitlefield);
                    formData.append('fname', fname);
                    formData.append('lname', lname);
                    formData.append('dob', dob);
                    formData.append('email', email);
                    formData.append('country', country);
                    formData.append('state', state);
                    formData.append('Occupation', Occupation);
                    formData.append('city', city);
                    formData.append('auth', auth);
                    formData.append('userfile', fileupload);
                    $.ajax({
                        type: 'post',
                        url: "" + base_url + 'main/signupdata',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (html) {
                            $('body').removeClass('loading');
                            //alert("successfully");
                            window.location.href = "" + base_url + 'profile';
                        }
                    });
                    //console.log(fileupload);
                }
            });

        }

    });
})
// smart follow banner script sign up page
/*popup script of header settings */
$(document).on('click', '.settings-popup-btn', function (e) {
    var effect = 'slide';
    var options = {direction: $('.mySelect').val()};
    var duration = 500;
    $("body").append('');
    $(".settings-popup").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".settings-popup, .overlay").hide("medium");
    });
});


// script for the settings page
$(function () {
    $(document).on('click', '.settings-blocks .edit', function (e) {
        $(this).parent().children("div").slideToggle("slow");
    });
});

// script for the adding new email address on settings page
$(function () {
    $(document).on('click', '.add-email-click,.add-ph-click', function (e) {
        $(this).siblings("div").slideToggle("slow");
    });
});

//script to toggle icon in specification tab
$(document).on('click', '.spec-headline', function () {
    $(this).find('i').toggleClass('fa-chevron-up');
    $(this).find('i').toggleClass('fa-chevron-down')
});

//for accpting friend request
$(document).on('click', '.accept-btn', function () {
    //alert("hi");
    var scope_id = this;
    var user_id = (this).id;
    ;
    var follower_id = $("#myid").html();
    //   var follower_id=(this).id;
    if (user_id && follower_id) {

        var data = {
            "user_id": user_id,
            "follower_id": follower_id
        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "/ajax/follow_request",
            //dataType: 'json',
            data: data,
            success: function (html) {
                //alert(html);
                $(scope_id).parent().parent().hide();

            }
        });

    }
});

$(document).on('click', '.comment-sectn', function () {
    //alert("hello");
    var dis = $(this).parent().parent().parent().parent().children('.write-comment').is(':visible');
    //alert(dis);
    if (dis == false) {
        // alert("ok");
        $('.fwd-social-icons').hide();
        $('.write-comment').hide();
        $(this).parent().parent().parent().parent().children('.write-comment').slideDown();
    } else {
        ///alert("not ok");
        $(this).parent().parent().parent().parent().children('.write-comment').slideUp();
    }
});

//for the post on homepage of user
$(document).on('click', '.icon-reply-home', function () {
    var comm_id = $(this).parent().parent().find('.home_comment').attr('id');
    var comment = $("#" + comm_id).val();
    var post_id = $(this).parent().parent().attr('id');
    var scope = this;
    if (comment != '') {
        var userid = $("#myid").html();
        if (userid != '') {

            var data = {
                "post_id": post_id,
                "userid": userid,
                "comment": comment
            };

            $.ajax({
                type: "POST",
                url: "" + base_url + "/ajax/user_comment_popup",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    $('.home_comment').val('');
                    $("#alert-msg").text("Your comment has been posted");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);

                    var sount = $(scope).parent().parent().parent().children().siblings().children().find("tr").find('.comment-sectn').find('.text').html();
                    var newno = parseInt(sount) + parseInt(1);
                    //alert(newno);
                    $(scope).parent().parent().parent().children().siblings().children().find("tr").find('.comment-sectn').find('.text').html(newno);
                    //$("#comment_section").html(html);

                    //var insertid=html;
                    //$('#home_comment').val(" ");
                    //if(insertid>0)
//					{
//
//					  alert(insertid)
//					}
//					else
//					{
//					  alert("error please try again");
//					}
//

                }
            });
        }
        else {
            alert("please login first");
        }
    }
    else {
        alert("Please fill your comment");
    }
});

$(document).on('click', '.post', function (event) {


    var id = this.id;
    var userid = $("#myid").html();
    // var source=$(this).parent().attr('id');

    // var userid=$("#myid").html();
    // var friend_id=$("#friend_id").html();
    // alert(friend_id);
    if (userid == '') {
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please login first");
    }
    else {
        //alert(id);
        if (id) {
            var data = {
                "post_id": id,
                "userid": userid
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "/ajax/post_crown",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    // alert(html);
                    //$("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                }
            });  //ajax ends here
        }
    } //else ends here

});


// script for user setting page

// code for who can see your profile
$(document).on('click', 'input:radio[name=seeprofile]', function () {
    profile();
});

function profile() {
    var seeprofile = $('input[name=seeprofile]:checked').val();
    //alert(seeprofile);
    var data = {
        "seeprofile": seeprofile


    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/userpassexist",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert(html);
            var inviteuser = html;
            if (inviteuser == 0) {
                //alert(html);
                document.getElementById('usererror').innerHTML = "Password Not matched!!";
                $("#usererror").html("Password Not matched!!");
            }
            else {
                //$("#usererror").html("Password Not matched!!");
                $("#usererror").html("");
            }
        }
    });

}

// code for who can see your crown
$(document).on('click', 'input:radio[name=seecrown]', function () {
    crown();
    ///alert('mohini');
});


function crown() {
    var seecrown = $('input[name=seecrown]:checked').val();
    //alert(seecrown);
    var data = {
        "seecrown": seecrown
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseecrown",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert("thank you");
            window.location.href = "" + base_url + 'usersetting/setting';
        }
    });
}

// code for who can see your comment
$(document).on('click', 'input:radio[name=seecomment]', function () {
    comment();
});
function comment() {
    var seecomment = $('input[name=seecomment]:checked').val();
    //alert(seecomment);
    var data = {
        "seecomment": seecomment
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseecomment",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert("thank you");
            window.location.href = "" + base_url + 'usersetting/setting';
        }

    });
}

// code for who can see your status
$(document).on('click', 'input:radio[name=seestatus]', function () {
    status();
});

function status() {
    var seestatus = $('input[name=seestatus]:checked').val();
    //alert(seestatus);
    var data = {
        "seestatus": seestatus
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseestatus",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert(html);
            window.location.href = "" + base_url + 'usersetting/setting';
        }
    });
}

// code for who can see your photos
$(document).on('click', 'input:radio[name=seephotos]', function () {
    photos();
});

function photos() {
    var seephotos = $('input[name=seephotos]:checked').val();
    //alert(seephotos);
    var data = {
        "seephotos": seephotos
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseephotos",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert(html);
            window.location.href = "" + base_url + 'usersetting/setting';
        }
    });
}

// code for who can see your followers
$(document).on('click', 'input:radio[name=seefollower]', function () {
    follower();
});

function follower() {
    var seefollower = $('input[name=seefollower]:checked').val();
    //alert(seefollower);
    var data = {
        "seefollower": seefollower
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseefollower",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            // alert(html);


            window.location.href = "" + base_url + 'usersetting/setting';
        }
    });
}

// code for who can see your brand
$(document).on('click', 'input:radio[name=seebrand]', function () {
    brand();
});

function brand() {
    var seebrand = $('input[name=seebrand]:checked').val();
    //alert(seebrand);
    var data = {
        "seebrand": seebrand
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/canseebrand",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            // alert(html);
            window.location.href = "" + base_url + 'usersetting/setting';
        }
    });
}

//check password exist or not
$(document).on('click', '#newpass', function () {
    existpassword();
});


function existpassword() {
    var currentpass = $("#currentpass").val();
    var data = {
        "currentpass": currentpass
    };
    $.ajax({
        type: "POST",
        url: "" + base_url + "usersetting/userpassexist",
        // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
        data: data,
        //crossDomain:true,
        success: function (html) {
            //alert(html);
            var inviteuser = html;
            if (inviteuser == 0) {
                //alert(html);
                document.getElementById('usererror').innerHTML = "Password Not matched!!";
                $("#usererror").html("Password Not matched!!");
            }
            else {
                //$("#usererror").html("Password Not matched!!");
                $("#usererror").html("");
            }
        }
    });
}

// save new password

$(document).ready(function () {
    $(document).on('click', '#savepass123', function () {
        $('body').addClass('loading');
        var newpass = $("#newpass").val();
        var currentpass = $("#currentpass").val();
        // var dob =$("#datepicker1").val();
        // alert(newpass);
        if (currentpass == '') {
            $("#alert-msg").text("Please Enter Current Password");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter Current Password");
            return false;
        }

        if (newpass == '') {
            $("#alert-msg").text("Please Enter New Password");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter New Password");
            return false;
        }
        else {
            var data = {
                newpass: newpass
            };
            $.ajax({
                type: "POST",
                url: "" + base_url + "usersetting/passwordchange",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                success: function (response) {

                    $('body').removeClass('loading');
                    //alert(response);
                    window.location.href = "" + base_url + "usersetting/logout";
                    if (html == "logout") {
                        //alert(html);
                        window.location.href = "" + base_url + 'usersetting/logout';
                        //alert("successfully");
                        //alert(html);
                        //window.location.href='http://rigalio.com/x/no_access/main/logout';
                    }
                }
            });
        }
    });

// change DOB
    $(document).on('click', '#savedob', function () {
        $('body').addClass('loading');
        var newdob = $("#newdob").val();
        // var currentpass =$("#currentpass").val();
        //alert(newdob);
        if (newdob == '') {


            $("#alert-msg").text("Please Enter Current DOB");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter Current DOB");
            return false;
        }

        else {
            var data = {
                newdob: newdob
            };
            $.ajax({
                type: "POST",
                url: "" + base_url + "usersetting/dobchange",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                success: function (html) {
                    $('body').removeClass('loading');
                    //alert("successfully");
                    window.location.href = "" + base_url + 'usersetting/setting';
                    //alert(html);
                    //window.location.href='http://localhost/finalrigalio/main/profile';
                }
            });

        }
    });
// add phone no.
    $(document).on('click', '#savephone', function () {
        $('body').addClass('loading');
        var addphone = $("#addphone").val();
        // var currentpass =$("#currentpass").val();
        //alert(addphone);
        if (addphone == '') {
            $("#alert-msg").text("Please Enter Your Phone No.");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            // alert("Please Enter Your Phone No.");
            return false;
        }
        else {
            var data = {
                addphone: addphone
            };
            $.ajax({
                type: "POST",
                url: "" + base_url + "usersetting/addphoneno",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                success: function (html) {
                    //alert("successfully");
                    $('body').removeClass('loading');
                    window.location.href = "" + base_url + 'usersetting/setting';
                    //alert(html);
                    //window.location.href='http://localhost/finalrigalio/main/profile';
                }
            });

        }
    });

// add E-mail id
    $(document).on('click', '#saveemail', function () {
        $('body').addClass('loading');
        var addemail = $("#addemail").val();
        // var currentpass =$("#currentpass").val();
        //alert(addemail);

        if (addemail == '') {
            $("#alert-msg").text("Please Enter Your Email");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter Your Email");
            return false;
        }

        else {
            var data = {
                addemail: addemail
            };
            $.ajax({
                type: "POST",
                url: "" + base_url + "usersetting/addemail",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                success: function (html) {
                    //alert(html);
                    var value = html;
                    $('body').removeClass('loading');
                    if (value == '0') {
                        window.location.href = "" + base_url + 'usersetting/setting';
                    }
                    else {
                        $(".addemail").val('');
                        $("#alert-msg").text("Email Id already exist");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);

                        //alert("alrady exist");
                    }

                    //alert(html);
                    //
                }
            });

        }
    });

//update information
    $(document).on('click', '#saveinfo', function () {
        var updatedinfo = $('input[name=info]:checked').val();
        //alert(currentpass);
        //alert(updatedinfo);
        if (updatedinfo == '') {
            $("#alert-msg").text("Please Enter Your Frequency Of Information");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please Enter Your Frequency Of Information");
            return false;
        }
        else {
            var data = {
                updatedinfo: updatedinfo
            };
            $.ajax({
                type: "POST",
                url: "" + base_url + "usersetting/updateinfo",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                success: function (html) {
                    // alert("successfully");
                    window.location.href = "" + base_url + 'usersetting/setting';
                }
            });

        }
    });

//update mode of communication
    $(document).on('click', '#savecommunication', function () {
        var keepyou = new Array();
        var keepyouElems = document.getElementsByName("keepyou[]");
        var keepyoucount = 0;
        for (var i = 0; i < keepyouElems.length; i++) {
            if (keepyouElems[i].type == "checkbox" && keepyouElems[i].checked == true) {
                keepyoucount++;
                keepyou.push(keepyouElems[i].value);
            }
        }
        // alert(keepyou);
        if (keepyoucount < 1) {
            //document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
            $("#alert-msg").text("Please select at least one way to maintain correspondence");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("Please select atleast 1 Keep you");
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                // url: ""+base_url+"usersetting/updateinfo",
                url: "" + base_url + "usersetting/updatecommunication?keepyou=" + keepyou,
                // data:data,
                success: function (html) {
                    //var me = html
                    //alert(html);
                    window.location.href = "" + base_url + 'usersetting/setting';
                }
            });
        }
    });

})

//index page comment section
$(document).on('click', '.icon-reply', function () {
    //alert('hi');
    var comm_id = $(this).parent().parent().find('.home_comment').attr('id');
    var comment = $("#" + comm_id).val();
    var scope = this;
    //var comment=$('#comment').val();
    var product_id = $(this).parent().parent().attr('id');
    ;
    //var comment=$("#"+comment_id).val();
    //alert(comment); alert(product_id);
    if (comment != '') {
        //var productid=$("#productid").html();
        var userid = $("#myid").html();
        //alert(userid);
        if (userid != '') {

            var data = {
                "productid": product_id,
                "userid": userid,
                "comment": comment
            };
            //alert(data);
            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/user_comment",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    //$('.home_comment').empty();
                    $('.home_comment').val('');
                    $("#alert-msg").text("Your comment has been posted");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    var sount = $(scope).parent().parent().parent().children().siblings().children().find("tr").find('.comment-sectn').find('.text').html();
                    var newno = parseInt(sount) + parseInt(1);
                    //alert(newno);
                    $(scope).parent().parent().parent().children().siblings().children().find("tr").find('.comment-sectn').find('.text').html(newno);
                    //$("#comment_section").html(html);
                    // var insertid=html;
                    // alert(insertid);
                    // $('.home_comment').empty();
                    /*if(insertid>0)
                     {
                     alert(insertid)
                     }
                     else
                     {
                     alert("error please try again");
                     }
                     */

                }
            });
        }
        else {
            $("#alert-msg").text(" You need to login first to pursue any engagement on Rigalio");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("please login first");
        }
    }
    else {
        $("#alert-msg").text("Please fill your comment");
        $(".alert-sectn").show("medium");

        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        //alert("Please fill your comment");
    }

});


//for sending following request to the user
$(document).on('click', '.follow-profile-btn-home', function () {
    var myid = $("#myid").html();
    var friendid = this.id;
    // alert(myid+friendid);
    // alert("hi i am in friend following");

    if (myid != friendid) {

        if (friendid != 'undefined') {
            var data = {
                "myid": myid,
                "friendid": friendid
            };
            $.ajax({
                type: "POST",

                url: "" + base_url + "ajax/user_follow",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    var value = html;
                    if (value == '1') {
                        $(".alert-sectn").show("medium");
                        $("#alert-msg").text("You are already following");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                    }
                    else if (value == '2') {
                        $(".alert-sectn").show("medium");
                        $("#alert-msg").text("Private Profile");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                    }
                    else if (value == '3') {
                        $(".alert-sectn").show("medium");
                        $("#alert-msg").text("You have Started following");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                    }
                    else {
                        $(".alert-sectn").show("medium");
                        $("#alert-msg").text("Error");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                    }


//alert(html);

                }
            });
        }
        else {
            $("#alert-msg").text("You can not follow yourself");
            $(".alert-sectn").show("medium");
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
            //alert("You can not follow yourself.");
        }
    }
    else {
        $("#alert-msg").text("You can not follow yourself");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);

    }


});


$(document).on('click', '.icon-send', function (event) {
    $('body').addClass('loading');
    //alert("please run");
    var post = $("#post1").val();
    // alert(post);
    var user_id = $("#myid").html();
    //alert(post);
    if (post == '') {
        $('body').removeClass('loading');
        $("#alert-msg").html("please include some status");
        $(".alert-sectn").show("medium");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
        // alert("please include some status")

    }
    else {
        var data = {
            "post": post,
            "user_id": user_id

        };
        $.ajax({

            type: "POST",
            url: "" + base_url + "upload/upload_file",
            //dataType: 'json',
            data: data,

            success: function (html) {
                $("li").removeClass("timeline-inverted");
                $("#main_content").prepend(html);
                $("ul li:nth-child(odd)").addClass('timeline-inverted');
                $("#post1").val(" ");


                placeholder();


                //$("#post1").attr("placeholder","Post a status or upload an image").blur();
                //$("#upload_file").reset();
                $('body').removeClass('loading');
                //$("#posting_data").html('<form method="post" action="http://localhost/finalrigalio/upload/upload_file" id="upload_file"> <input type="text" name="post" id="post" value="" placeholder="Post a status or upload an image" class="textbox1"><a class="post-status"><span id="post_comment1">Post</span></a> <a class="upload-btn"><span><i class="fa fa-camera"><input type="file" name="userfile" id="userfile" onchange="previewImage();"/></i></span> </a><img src="" id="user_files" style="height: 100px;width: 100px;display: none;"/>');
                $("#user_files").fadeOut();
            }

        });
        //console.log(fileupload);
    }

});


// JavaScript Document

$(document).on('click', '.fa-lock', function (e) {
    var mythis = this;
    var lock_subcat = $(this).parent().parent().parent().attr('id');
    var user_id = $("#myid").html();


    var data = {
        "lock_subcat": lock_subcat,
        "user_id": user_id


    };

    $.ajax({
        type: "POST",
        url: "" + base_url + "ajax/collection_status",
        data: data,
        success: function (html) {
            //alert(html);
            $(mythis).parent().find('.fa-lock').hide();
            $(mythis).parent().find('.fa-unlock-alt').show();

            //$('#collection_result').html(html);
        }

    });


});


$(document).on('click', '.fa-unlock-alt', function (e) {

    var mythis = this;
    var lock_subcat = $(this).parent().parent().parent().attr('id');
    var user_id = $("#myid").html();

    var data = {
        "lock_subcat": lock_subcat,
        "user_id": user_id

    };

    $.ajax({
        type: "POST",
        url: "" + base_url + "ajax/collection_status_lock",
        data: data,
        success: function (html) {
            //alert(html);
            $(mythis).parent().find('.fa-lock').show();
            $(mythis).parent().find('.fa-unlock-alt').hide();

            //$('#collection_result').html(html);
        }

    });
});

//image upload

$(document).ready(function () {
    $('.pull-left').click(function () {

        $('#user_files').attr('src', 'abc');
        $('#user_files').css("background", "url(/images/r-srchbg_white.png) no-repeat");
        $(".img-upload-pop").show();
        $(".img-uploaded").hide();

    });
});

//script for the timeline comment popup

$(document).ready(function () {
    $(".tag").click(function (e) {
        var effect = 'slide';
        var options = {direction: $('.mySelect').val()};
        var duration = 500;
        $("body").append('');
        $(".comment-pop").show("medium", "linear", function () {
            $("#comment-post1").mCustomScrollbar({
                autoHideScrollbar: true,
                theme: "light-thin"
            });
        });
        $(".overlay").show("medium");
        $(".close").click(function (e) {
            $(".comment-pop,.overlay").hide("medium", "linear", function () {
                $(".mCSB_scrollTools").css({"display": "none"});
            });

        });
        $("#header_nav").addClass("high-z");
    });
});

// update profile picture

$(document).on('click', '#updateprofile_pic', function () {
    //$("#loader_img").show();
    $('body').addClass('loading');
    var filename = $('input[name=userfile1]');
    var fileupload1 = filename[0]['value'];

    if (fileupload1 == "") {
        alert("please select your image");
    }
    else {
        var fileupload = filename[0].files[0];
        var formData = new FormData();
        formData.append('userfile', fileupload);
        $.ajax({
            type: 'post',
            url: "" + base_url + "main/updateprofilepic",
            data: formData,
            processData: false,
            contentType: false,
            success: function (html) {
                  alert("successfully");
                $('body').removeClass('loading');
               // window.location.href = "" + base_url + 'timeline';
            }
        });
        //console.log(fileupload);
    }


});

//request and invite

$(document).on('click', '.userinvite', function () {
    //alert('hi');
    var name = $("#name").val();
    var email = $("#email").val();
    var userinkedin = $("#userinkedin").val();
    var phoneno = $("#phoneno").val();
    var company = $("#company").val();
    var designation = $("#designation").val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var pattern = /^\d{10}$/;
    //alert(name + email + userinkedin + phoneno + company + designation );
    //alert(phoneno);
    if (name == '') {
        document.getElementById('msg2').innerHTML = "Please Enter Name";
        return false;
    }

    if (email == '') {
        document.getElementById('msg2').innerHTML = "Please Enter email";
        return false;
    }

    if (reg.test(email) == false) {
        document.getElementById('msg2').innerHTML = "Invalid Email Address";
        return false;
    }

    if (userinkedin == '') {
        document.getElementById('msg2').innerHTML = "Please Enter userinkedin";
        return false;
    }

    if (isNaN(phoneno)) {
        document.getElementById('msg2').innerHTML = "Please Enter Only Numeric Number";
        return false;
    }
    if (phoneno == '') {
        document.getElementById('msg2').innerHTML = "Enter Phone No.";
        return false;
    }
    if (pattern.test(phoneno) == false) {
        document.getElementById('msg2').innerHTML = "Enter 10 digit Phone No.";
        return false;
    }

    if (company == '') {
        document.getElementById('msg2').innerHTML = "Please Enter company";
        return false;
    }
    if (designation == '') {
        document.getElementById('msg2').innerHTML = "Please Enter designation";
        return false;
    }
    else {
        var data =
        {
            "name": name,
            "email": email,
            "userinkedin": userinkedin,
            "phoneno": phoneno,
            "company": company,
            "designation": designation,

        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "main/requestndinvite",
            data: data,
            success: function (html) {
                //alert('successfully');
                $('.aftersubmit').removeClass('aftersubmit').hide()
                document.getElementById('msg2').innerHTML = "We at Rigalio welcome your interest to be a part of the world of infinite luxury.";
                // $('.invite-request current').hide()
            }

        });
    }


});
$(document).on('click', '.try-now-submit', function () {

    var name = $("#namepro").val();
    var contactno = $("#contactnopro").val();
    var emailid = $("#emailidpro").val();
    var date = $("#date1").val();
    var time = $("#timepro").val();
    var query = $("#querypro").val();
    var formname = $("#formnamepro").val();
    var pattern = /^\d{10}$/;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    //alert(name+emailid+contactno+date+time+query);
    //alert(name);
    if (name == '' || name == 'NAME') {
        document.getElementById('msg123').innerHTML = "Please Enter Name";
//alert("Please Enter Name");
        return false;
    }
//alert(name);
    if (contactno == '') {
        document.getElementById('msg123').innerHTML = "Please Enter contactno";
//alert("Please Enter Your Mobile No.");
        return false;
    }
    if (isNaN(contactno)) {
        document.getElementById('msg123').innerHTML = "Please Enter Only Numeric Number";
        //alert("Please Enter Only Numeric Number");
        return false;
    }
    if (pattern.test(contactno) == false) {
        //alert("Enter 10 digit Phone No.");
        document.getElementById('msg123').innerHTML = "Enter 10 digit Phone No.";
        return false;
    }

    if (emailid == '' || emailid == 'EMAIL ID') {
//alert("Please Enter Email");
        document.getElementById('msg123').innerHTML = "Please Enter Email";
        return false;
    }
    if (reg.test(emailid) == false) {
        document.getElementById('msg123').innerHTML = "Invalid Email Address";
        return false;
    }

    else {
        var data =
        {
            "name": name,
            "emailid": emailid,
            "contactno": contactno,
            "date": date,
            "time": time,
            "query": query,
            "formname": formname,

        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "main/trynow",
            data: data,
            success: function (html) {
                alert("The requisite authorities will get in touch with you");
                $('.myhide').removeClass('myhide').hide()
                document.getElementById('msg123').innerHTML = "The requisite authorities will get in touch with you";
            }

        });
        return false;
    }


});
$(document).on('click', '.try-now-submit1', function () {
    var name = $("#namebrand").val();
    var contactno = $("#contactnobrand").val();
    var emailid = $("#emailidbrand").val();
    // var date = $("#date").val();
    //var time = $("#time").val();
    var query = $("#querybrand").val();
    var formname = $("#formnamebrand").val();
    var pattern = /^\d{10}$/;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    // alert(name+emailid+contactno+date+time+query);

    if (name == '' || name == 'NAME') {
        document.getElementById('msgbrand').innerHTML = "Please Enter Name";
        return false;
    }

    if (contactno == '' || contactno == 'CONTACT NO.') {
        document.getElementById('msgbrand').innerHTML = "Please Enter Your Mobile No.";
        return false;
    }
    if (isNaN(contactno)) {

        document.getElementById('msgbrand').innerHTML = "Please Enter Only Numeric Number";
        return false;
    }
    if (pattern.test(contactno) == false) {
        document.getElementById('msgbrand').innerHTML = "Enter 10 digit Phone No.";
        return false;
    }

    if (emailid == '' || emailid == 'EMAIL ID') {
        document.getElementById('msgbrand').innerHTML = "Please Enter Email";
        return false;
    }
    if (reg.test(emailid) == false) {
        document.getElementById('msgbrand').innerHTML = "Invalid Email Address";
        return false;
    }

    else {
        var data =
        {
            "name": name,
            "emailid": emailid,
            "contactno": contactno,

            "query": query,
            "formname": formname,

        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "main/query",
            data: data,
            success: function (html) {
                //alert("You will be contacted shortly by the concerned brand");
                $('.myhide1').removeClass('.myhide1').hide()
                document.getElementById('msgbrand').innerHTML = "You will be contacted shortly by the concerned brand";
            }

        });
        return false;
    }
});
$(document).on('click', '.contact-submit', function () {
    var name = $("#namectact").val();
    var contactno = $("#contactnoctact").val();
    var emailid = $("#emailidctact").val();
    // var date = $("#date").val();
    //var time = $("#time").val();
    var query = $("#queryctact").val();
    var formname = $("#formnamectact").val();
    var pattern = /^\d{10}$/;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    // alert(name+emailid+contactno+date+time+query);

    if (name == '' || name == 'NAME') {
        document.getElementById('msgctact').innerHTML = "Please Enter Name";
        return false;
    }

    if (contactno == '' || contactno == 'CONTACT NO.') {
        document.getElementById('msgctact').innerHTML = "Please Enter Your Mobile No.";
        return false;
    }
    if (isNaN(contactno)) {
        document.getElementById('msgctact').innerHTML = "Please Enter Only Numeric Number";
        return false;
    }
    if (pattern.test(contactno) == false) {
        document.getElementById('msgctact').innerHTML = "Enter 10 digit Phone No.";
        return false;
    }

    if (emailid == '' || emailid == 'EMAIL ID') {
        document.getElementById('msgctact').innerHTML = "Please Enter Email";
        return false;
    }
    if (reg.test(emailid) == false) {
        document.getElementById('msgctact').innerHTML = "Invalid Email Address";
        return false;
    }

    else {
        var data =
        {
            "name": name,
            "emailid": emailid,
            "contactno": contactno,
            "query": query,
            "formname": formname,

        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "main/contactussubmit",
            data: data,
            success: function (html) {
                //alert("You will be contacted shortly by the concerned brand");
                $('.myhide2').removeClass('.myhide2').hide()
                document.getElementById('msgctact').innerHTML = "Rigalio will get back to you with a feedback at the earliest";
            }

        });
        return false;
    }
});

// count comment on image popup in home page
$(document).ready(function () {
    $('.myclose').click(function () {
        $('.popimg').removeClass('increment_comment');
    });
});

// JavaScript Document


//     $(document).on('mouseover', '.cat-plus-icon', function()
//{
$(document).ready(function (e) {

    $('.cat-plus-icon').mouseover(function () {

        $('.fa-minus').css("z-index", "999999");
        $('.fa-minus').css("position", "relative");
        $('.fa-plus').css("z-index", "2");
        $('.category,.hover-content-cat').css("z-index", "1");
    });

});

$(document).ready(function (e) {
    // $(document).on('mouseover','.cat-zoom-icon', function()
//{

    $('.cat-zoom-icon').mouseover(function () {

        $('.fa-plus').css("z-index", "999999");
        $('.category,.hover-content-cat').css("z-index", "1");
    });

});


$(document).ready(function () {
    var post_data = "";
    $(document).on('click', '.tag-friend', function () {

//console.log("test");
        $('body').addClass('loading');
        var id = $(this).parent().attr('id');
        $("#thispost_id").html(id);
        var source = $(this).parent().attr('name');
        post_data = $(this).html();
        var user_id_popup = $(this).parent().parent().parent().attr('id');
        // console.log(user_id_popup);
        if (!user_id_popup) {

            user_id_popup = $("#friend_id").html();

        }
        var data = {
            "user_id": user_id_popup
        }
        $.ajax({
            type: "POST",
            url: "" + base_url + "ajax/get_user",
            //dataType: 'json',
            data: data,
            success: function (html) {
                // alert(html);
                //$("#user_all_comments").html(html);
                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);

                var obj = $.parseJSON(html);
                //console.log(obj[0].firstname);
                $('.comment-popup-picture').attr('src', obj[0].profile_picture);
                $('.comment-popup-username').html(obj[0].firstname);


                console.log(html);
            }
        });  //ajax ends here
        $("#user_post_show").html(post_data);
        var data = {
            "post_id": id
        }
        $.ajax({
            type: "POST",
            url: "" + base_url + "Main/get_comments",
            //dataType: 'json',
            data: data,
            success: function (html) {
                //alert(html);
                $("#all_comments").html(html);
                $('body').removeClass('loading');

                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);

            }
        });  //ajax ends here

        var effect = 'slide';
        var options = {direction: $('.mySelect').val()};
        var duration = 500;
        $("body").append('');
        $(".comment-pop").show("medium");
        $(".overlay").show("medium");
        $(".close").click(function (e) {
            $(".comment-pop,.overlay").hide("medium");
        });

        $("#header_nav").addClass("high-z");
    });
});


$(document).on('click', '.crown-sectn', function () {


    var id = this.id;
    var currentvla = this;
    var userid = $("#myid").html();
    //var getcount = $("#abc123").val();
    var getcount = $(this).find('#crowncount').html();
    //alert(getcount);
    if (userid == '') {
        $(".alert-sectn").show("medium");
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio.");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);


        //alert("Please login first");
    }

    else {
        //alert(id);
        if (id) {
            var data = {
                "productid": id,
                "userid": userid
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/index12",

                data: data,
                success: function (html) {
                    var crown = html;
//alert(html);
                    $("#alert-msg").text("crown");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert(crown);
                    if (crown == '1') {
                        $("#alert-msg").text("You have already crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("Aleady is in crown");
                    }
                    else {
                        //var value = 1;
                        $("#alert-msg").text("You have crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("addes a crown");
                        var newno = parseInt(getcount) + parseInt(1);
                        //alert(newno);
                        //$("#crowncount").html(newno);
                        $(currentvla).find('#crowncount').html(newno);
                    }
                    // $("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                }
            });  //ajax ends here
        }
    } //else ends here

});








$(window).load(function () {
    $('#masonry-grid').masonry({
        itemSelector: '.grid-item',
        //columnWidth : 310,
        isAnimated: true,
        animationOptions: {
            duration: 700,
            easing: 'linear',
            queue: false
        }
    });
});


//for delete own product

$(document).on('click', '.delete_my_post', function (e) {
    // $('.delete_my_post').on("click",function(e){
//console.log("hello");
    var post_id = $(this).parent().parent().parent().find('.profile-main-con').attr('id');
    var user_id = $("#myid").html();
    if (post_id != '' && user_id != '') {
        //alert(post_id);

        var data = {
            "user_id": user_id,
            "post_id": post_id

        };

        $(this).parent().parent().parent().parent().parent().parent().hide();

        $.ajax({
            type: "POST",
            url: "" + base_url + "ajax/delete_post",
            data: data,
            success: function (html) {
                //alert(html);
                if (html == "done") {
                    // $(mythis).parent().parent().parent().parent().hide();
                    $("li").removeClass("timeline-inverted");
                    $("#timeline ul li:nth-child(odd)").addClass('timeline-inverted');
                }
                else {
                    //alert(html);
                }
                //$('#collection_result').html(html);
            }
        });


    }
});


// for hide product in user home page
$(document).on('click', '.hide-product', function (event) {
//alert("hi")

    var mythis = this;
    var user_id = $("#myid").html();
    if (user_id != '') {

        var product_id = this.id;
        var data = {
            "user_id": user_id,
            "product_id": product_id

        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "ajax/hide_product",
            data: data,
            success: function (html) {
                //alert(html);
                if (html == "done") {
                    $(mythis).parent().parent().parent().parent().hide();
                }
                else {
                    //alert(html);
                }
                //$('#collection_result').html(html);
            }
        });

    } else {
        alert("Please login first");
    }

});

// for hide post in user home page
$(document).on('click', '.hide-post1', function (event) {
    //alert("hi")

    var mythis = this;
    var user_id = $("#myid").html();
    if (user_id != '') {

        var post_id = this.id;
        var data = {
            "user_id": user_id,
            "post_id": post_id

        };

        $.ajax({
            type: "POST",
            url: "" + base_url + "ajax/hide_post",
            data: data,
            success: function (html) {
                //alert(html);
                if (html == "done") {
                    $(mythis).parent().parent().parent().parent().hide();
                }
                else {
                    //alert(html);
                }
                //$('#collection_result').html(html);
            }
        });

    } else {
        alert("Please login first");
    }

});

// script for delete post toggle

$(document).on('click', '.hide-post', function (event) {
    //$(".del-button").slideToggle();
    event.stopPropagation();
    $(this).next(".del-button").slideToggle();

});

//for the image gallery popup

$(document).on('click', '.popimg_gallery', function (e) {
    // var id = $(this).parent().attr('id');
    //alert("hello");
    $('body').addClass('loading');
    //$(this).addClass('increment_comment');
    var user_id_popup = $("#myid").html();

    var id=this.id;
    //user_id_popup = $(this).parent().parent().parent().attr('id');
//alert(user_id_popup);
    //var friend_id = $('#friend_id').html();
///console.log(user_id_popup);

    var data = {
        "user_id": user_id_popup
    }


    $("#img_pop_post_id").html(id);

    var post_image = $(this).children().attr('src');
    $('.pop_comment_img').attr('src', post_image);
    post_data = $(this).next().html();

    //$("#image_popup_status").html(post_data);
    //$("#thispost_id").html(id);
    var data = {
        "post_id": id
    }
    //alert(data);




    $.ajax({
        type: "POST",
        url: "" + base_url + "ajax/get_post_byid/"+id,
        data: data,
        success: function (html) {
            var obj = $.parseJSON(html);
            $(".pop_comment_img").attr('src', base_url+'/content/'+obj[0].image);
            $("#image_popup_status").html(obj[0].post_data);
            //console.log(obj);
            //alert(html);
            // $("#image_countpopup_comments").html(html);
        }
    });  //ajax ends here




    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_post_id",
        data: data,
        success: function (html) {
            $("#image_countpopup_comments").html(html);
        }
    });  //ajax ends here

    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_comments",
        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
            $("#image_popup_comments").html(html);
            $('body').removeClass('loading');
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);
        }
    });  //ajax ends here
    var source = $(this).parent().attr('name');
    var effect = 'slide';
    var options = {direction: $('.mySelect').val()};
    var duration = 500;
    $("body").append('');
    $(".img-post").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".img-post, .overlay").hide("medium");
    });
});


//popup functionality for the friend
$(document).on('click', '.popimg-friend', function (e) {
    var id = $(this).parent().attr('id');
//alert(id);
    $('body').addClass('loading');
    $(this).addClass('increment_comment');
    var user_id_popup = "";

        user_id_popup = $("#myid").html();



    alert(user_id_popup);
    var data = {
        "user_id": user_id_popup
    }
    $.ajax({
        type: "POST",
        url: "" + base_url + "ajax/get_user",
        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
            //$("#user_all_comments").html(html);
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);

            var obj = $.parseJSON(html);
            //console.log(obj[0].firstname);
              console.log(obj);
            alert(obj[0].profile_picture);
            $('.commentor-img img').css('background-image',"url(" + base_url + ""+obj[0].profile_picture+")");
           // $('.popimg-profile-name').html(obj[0].firstname);
//alert(obj['firstname']);

            console.log(html);
        }
    });  //ajax ends here
    var user_name = $(this).parent().parent().find('h6').html();
    var user_image = $(this).parent().parent().find('img').attr('src');
    $("#img_pop_post_id").html(id);

    var post_image = $(this).children().attr('src');
    $('.pop_comment_img').attr('src', post_image);
    post_data = $(this).next().html();
    ;
    $("#image_popup_status").html(post_data);
    //$("#thispost_id").html(id);
    var data = {
        "post_id": id
    }
    //alert(data);
    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_post_id",
        data: data,
        success: function (html) {
            $("#image_countpopup_comments").html(html);
        }
    });  //ajax ends here

    $.ajax({
        type: "POST",
        url: "" + base_url + "main/get_comments",
        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
            $("#image_popup_comments").html(html);
            $('body').removeClass('loading');
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);
        }
    });  //ajax ends here
    var source = $(this).parent().attr('name');
    var effect = 'slide';
    var options = {direction: $('.mySelect').val()};
    var duration = 500;
    $("body").append('');
    $(".img-post").show("medium");
    $(".overlay").show("medium");
    $(".close").click(function (e) {
        $(".img-post, .overlay").hide("medium");
    });
});



$(document).on('click', '.crown-news', function () {


    var id = this.id;
    var currentvla = this;
    var userid = $("#myid").html();
    //var getcount = $("#abc123").val();
    var getcount = $(this).find('#crowncount').html();
    //alert(getcount);
    if (userid == '') {
        $(".alert-sectn").show("medium");
        $("#alert-msg").text("You need to login first to pursue any engagement on Rigalio.");
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);


        //alert("Please login first");
    }

    else {
        //alert(id);
        if (id) {
            var data = {
                "newsid": id,
                "userid": userid
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "Ajax/news_crown",

                data: data,
                success: function (html) {
                    var crown = html;
//alert(html);
                    $("#alert-msg").text("crown");
                    $(".alert-sectn").show("medium");
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                    //alert(crown);
                    if (crown == '1') {
                        $("#alert-msg").text("You have already crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("Aleady is in crown");
                    }
                    else {
                        //var value = 1;
                        $("#alert-msg").text("You have crowned this");
                        $(".alert-sectn").show("medium");
                        setTimeout(function () {
                            $('.alert-sectn').fadeOut();
                        }, 3000);
                        //alert("addes a crown");
                        var newno = parseInt(getcount) + parseInt(1);
                        //alert(newno);
                        //$("#crowncount").html(newno);
                        $(currentvla).find('#crowncount').html(newno);
                    }
                    // $("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                }
            });  //ajax ends here
        }
    } //else ends here

});



    function placeholder(){
        $("input[type=text]").each(function(){
            var phvalue = $(this).attr("placeholder");
            $(this).val(phvalue);
        });
    }
    placeholder();
    $("input[type=text]").focusin(function(){
        var phvalue = $(this).attr("placeholder");
        if (phvalue == $(this).val()) {
            $(this).val("");
        }
    });
    $("input[type=text]").focusout(function(){
        var phvalue = $(this).attr("placeholder");
        if ($(this).val() == "") {
            $(this).val(phvalue);
        }
    });


    $(document).on('click','.popusergallery',function(){

        var id = this.id;
        var mythis=this;
   //alert(id);
        $('body').addClass('loading');
        $(this).addClass('increment_comment');
        var user_id_popup = "";
        user_id_popup = $("#myid").html();
        alert(user_id_popup);

        if(user_id_popup!='') {


            //alert(user_id_popup);
            var data = {
                "user_id": user_id_popup
            }
            $.ajax({
                type: "POST",
                url: "" + base_url + "ajax/get_user",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    // alert(html);
                    //$("#user_all_comments").html(html);
                    //$("#"+id).next().css( "background", "yellow" );
                    //alert(html);

                    var obj = $.parseJSON(html);
                    //console.log(obj[0].firstname);
                    //  console.log(obj);
                    $('.popimg-profile-pic').attr('src', obj[0].profile_picture);
                    $('.popimg-profile-name').html(obj[0].firstname);
//alert(obj['firstname']);

                    console.log(html);
                }
            });  //ajax ends here



            $.ajax({
                type: "POST",
                url: "" + base_url + "ajax/get_post_byid/"+id,
                data: data,
                success: function (html) {
                    var obj = $.parseJSON(html);
                    $(".pop_comment_img").attr('src', base_url+'/content/'+obj[0].image);
                    $("#image_popup_status").html(obj[0].post_data);
                    //console.log(obj);
                    //alert(html);
                    // $("#image_countpopup_comments").html(html);
                }
            });  //ajax ends here




            var user_name = $(this).parent().parent().find('h6').html();
            var user_image = $(this).parent().parent().find('img').attr('src');
            $("#img_pop_post_id").html(id);

            var post_image = $(this).children().attr('src');
            $('.pop_comment_img').attr('src', post_image);
            post_data = $(this).next().html();

            $("#image_popup_status").html(post_data);
            //$("#thispost_id").html(id);

            var data = {
                "post_id": id
            }
            //alert(data);
            $.ajax({
                type: "POST",
                url: "" + base_url + "main/get_post_id",
                data: data,
                success: function (html) {
                    $("#image_countpopup_comments").html(html);
                }
            });  //ajax ends here

            $.ajax({
                type: "POST",
                url: "" + base_url + "main/get_comments",
                //dataType: 'json',
                data: data,
                success: function (html) {
                    // alert(html);
                    $("#image_popup_comments").html(html);
                    $('body').removeClass('loading');
                    //$("#"+id).next().css( "background", "yellow" );
                    //alert(html);
                }
            });  //ajax ends here
            var source = $(this).parent().attr('name');
            var effect = 'slide';
            var options = {direction: $('.mySelect').val()};
            var duration = 500;
            $("body").append('');
            $(".img-post").show("medium");
            $(".overlay").show("medium");
            $(".close").click(function (e) {
                $(".img-post, .overlay").hide("medium");
            });


        }

});