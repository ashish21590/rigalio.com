<!-- Carousel-->
<div style="width: 100%; display: block; clear: both;">
    <div style="width: 100%; display: block; clear: both;">
        <div class="main-slider-container hidden-xs screen-flip-slider">
            <div class=" main-slider">
                <div class="ui grid invisible">
                    <div class="five column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Puiforcat launches crystal glassware</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Dreams Venice has to offer</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The epitome of fine dining </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The new 7 Series is loaded with luxury</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">






                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Berluti: Fashioning Italian style </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>LVMH endorses sustainable luxury</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Handcrafted luxury from Bentley</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Soak in the sights in downtown London</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 2.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A rare collection of exquisite tableware</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A slice of heaven in exotica</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The rarest manuscripts up for auction at Christie’s</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The most expensive restaurant in the world</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A dessert crafted from truffles</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" column hidden-sm">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 3.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The Mediterranean beckons the luxury traveller</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 4.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The rise of a fashion czarina</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 5.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Experience Michelin Star restaurants in the city of love</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column hidden-sm">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>An I Phone app for curious voyagers</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>This rare Kodak is a collector’s dream</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Apple Watch is the newest kid on the horology kaleidoscope</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A boutique hotel nestled in a coffee plantation</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>


        <!--slider for mobile -->
        <div class="main-slider-container mobile-flip-slider hidden-lg hidden-md hidden-sm">
            <div class=" main-slider">
                <div class="ui grid invisible">
                    <div class="two column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">

                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.jpg" class='ui image'>


                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg" class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>

        <!--slider for mobile ends -->
    </div>
</div>
<div class="main_content">
    <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
                        <h2 ng-model="heading"><?php print_r($heading); ?></h2>
                        <hr>
                    </div>
                </div> <!--/content tagline-->
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content">
                    <div class="grid" id="masonry-grid">
                        <?php
                        $sr = 0;
                        foreach ($products as $product) {
                            ?>

                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category <?php echo $product['category_color']; ?>">
                                        <div class="category_img">

                                            <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive" alt="<?php echo $product['product_Name']; ?>"/>
                                            <!--<div class="hover-content-cat"> -->
                                            <div class="hover-content-cat" onclick = "location.href = '<?php echo base_url(); ?>product/<?php echo  $s1=str_replace(" ","_",$product['product_Name']); ?>/<?php echo $product['productId']; ?>/overview'">
                                                <?php
                                                $regid = $this->session->userdata('registrationid');
                                                if($regid !='')
                                                { ?>
                                                    <div class="del-area">
                                                        <span class="hide-post"><i class="fa fa-angle-down"></i></span>
                                                        <div class="del-button hide-product" id="<?php echo $product['productId']; ?>">Hide Post </div>

                                                    </div>
                                                <?php } ?>

                                                <div class="tab">
                                                    <div class="tab-in">
                                                        <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa-plus icomoon  icon-fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                        <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                                            <span class="fa-minus icomoon icon-fa-minus" id="<?php echo $product['productId']; ?>"> </div>
                                                        <div class="cat-minus-icon" style="display:none;"><span class="fa-minus icomoon icon-fa-minus"></span> </div>


                                                        <h4><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","_",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>/legacy"><?php echo $product['brand_Name']; ?></a></h4>
                                                        <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                                                            <?php
                                                            $me = [];
                                                            foreach($user_follow as $user_foll){

                                                                $me[] = $user_foll['brand_id'];
                                                            }
                                                            $flag=1;
                                                            for($i=0;$i<count($me);$i++){
                                                                if($product['brandId'] ==$me[$i]){
                                                                    $flag=0;
                                                                    break;
                                                                }
                                                            }
                                                            if($flag==0)
                                                            {?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
                                                        <?php }
                                                        else { ?>
                                                            <button class="follow-brand-btn follow" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
                                                        <?php } ?>

                                                    </div>
                                                    </a>
                                                </div> <!--/tab structure -->

                                            </div> <!--/hover-content-cat -->
                                        </div> </a> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="relative-struct">
                                                <h1 class="hea"><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","_",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a>
                                                    <div class="tooltip cat-follow-popup">
                                                        <div class="tab">
                                                            <div class="tab-cell">
                                                                <div class="img-part">  <span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                                                                <div class="follow-con">
                                                                    <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                                    <h1 class=""><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1>


                                                                    <div class="follow-status" id="<?php echo $product['categoryId']; ?>">
                                                                        <?php
                                                                        $me = [];
                                                                        foreach($user_follow as $user_foll){

                                                                            $me[] = $user_foll['category_id'];
                                                                        }
                                                                        $flag=1;
                                                                        for($i=0;$i<count($me);$i++){
                                                                            if($product['categoryId'] ==$me[$i]){
                                                                                $flag=0;
                                                                                break;
                                                                            }
                                                                        }
                                                                        if($flag==0)
                                                                        {?>
                                                                            <button data-text="Follow category"
                                                                                    class="follow-cat-btn normal following-cat">
                                                                                <i class="fa fa-check"></i></button>
                                                                        <?php }
                                                                        else { ?>
                                                                            <button data-text="Follow category"
                                                                                    class="follow-cat-btn normal follow-cat">
                                                                                <i class="fa fa-check"></i></button>
                                                                        <?php } ?>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>  </h1>
                                            </div> <!--/relative-struct -->

                                            <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                                            <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                                        </div> <!--/category_content-->
                                        <?php
                                        if($this->session->userdata('registrationid')){
                                            $count_people=[];
                                            $comment_people = [];
                                            foreach($this->getdata->crownuser_detail($product['productId']) as $count_no)
                                            {
                                                $count_people[]=$count_no;
                                            }
                                            foreach($this->getdata->commentuser_detail($product['productId']) as $countcomment_no)
                                            {
                                                $comment_people[]=$countcomment_no;
                                            }

                                            $sum_total = count($count_people) + count($comment_people);
                                            if ($sum_total != '0')
                                            {
                                                ?>

                                                <div class="hidden-status">
                                                    <h6><?php
                                                        //print_r($count_people);
                                                        $m=[];
                                                        foreach($count_people as $me){

                                                            $m[]=$me['user_id'];
                                                        }

                                                        $m1=[];
                                                        foreach($comment_people as $me){

                                                            $m1[]=$me['user_id'];
                                                        }
                                                        // echo count(array_unique(array_merge($m,$m1)));
                                                        //print_r(array_merge($count_people,$comment_people));
                                                        //$array1 = array_merge ($count_people,$comment_people);
                                                        //$array2 = array_unique($array1);
                                                        //$array3 = array_keys($array2);
                                                        //print_r($array2);
                                                        echo count(array_unique(array_merge($m,$m1)));;  ?> People Reacted On This </h6>  <span class="profile-angle"><i class="fa fa-angle-down"></i></span></div>
                                            <?php  }
                                        }?>



                                        <div class="category_options">
                                            <table>
                                                <tr>
                                                    <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff/(60*60*24))+1);?>days ago</p> </td>
                                                    <?php
                                                    $me = [];
                                                    foreach($user_crown as $user_foll){

                                                        $me[] = $user_foll['product_id'];
                                                    }
                                                    $flag=1;
                                                    for($i=0;$i<count($me);$i++){
                                                        if($product['productId'] ==$me[$i]){
                                                            $flag=0;
                                                            break;
                                                        }
                                                    }
                                                    if($flag==0)
                                                    {?>
                                                    <td class="crown-sectn active <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span>
                                                        <?php }
                                                        else { ?>
                                                    <td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span>
                                                        <?php } ?>
                                                        <span class="icomoon icon-crown"> </span></td>
                                                    <td class="comment-sectn <?php echo $product['category_color']; ?>">
                                                        <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview#comment">
                                                            <span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?></span> <span class="icomoon icon-chat"></span></a> </td>
                                                    <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span></span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                            </table>
                                            <div class="fwd-social-icons sec7" style="display:none;">
                                                <ul class="cat-follow-icons">
                                                    <li>
                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="fb"></a>
                                                    </li>
                                                    <li>


                                                        <a target="_blank" href="https://twitter.com/home?status=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="twitter" ></a>
                                                    </li>
                                                    <?php /* <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li> */ ?>
                                                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                                                </ul>
                                                <div class="hidden-more-icons" style="display:none;">
                                                    <ul class="follow-icons-more">
                                                        <li>

                                                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="linkdin"></a>
                                                        </li>
                                                        <li>
                                                            <a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="gplus"></a>

                                                        </li>
                                                        <li>

                                                            <a target="_blank" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&description=Testing" class="pini"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                                <?php
                                                $regif = $this->session->userdata('registrationid');
                                                if(!$regif || $userdata[0]['profile_picture']=='')
                                                {
                                                    ?>
                                                    <span ><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                                <?php } else{ ?>
                                                    <span style="background:url(<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>);" class="pf-img" > </span>
                                                <?php }  ?>
                                                <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div> <!--/category_options-->

                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <?php $sr++ ;
                        }
                        $this->session->set_userdata('sr',$sr);
                        //$ses_var = $this->session->userdata('sr);
                        ?>
                    </div> <!--/grid ends -->
                </div>   <!--/whats_new_content -->

                <!--/column-->

                <!-- mywork place ends here-->
                <div id="total_product_count" style="display:none;"><?php print_r($all_products_count); ?></div>

                <div class="container loading-sectn" style="text-align: center"><button class="btn" id="load_more" style="background:none" data-val = "2"><img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>loader.gif" class="img-responsive"> </button>

                </div>
                <div id="show" style="display:none" class="nomore">No more products to showcase</div>
            </div> <!--/Whats_new_content-->
        </div> <!--/row-->
    </div> <!--/container-fluid-->
</div> <!--/main_content--> <!--/main_content-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->



<script type="text/javascript" src="<?php echo base_url(); ?>content/js/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/semantic.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/myflip.js"></script>

<script type="text/javascript">
    // jQuery
    var fixHeight = function(images){

        var w = 345;
        var h = 672;
        var W = window.innerWidth * images;
        var H = (h/w) * W;
        $('.main-slider').height(H)

    }
    function checkWidth() {



        if ($(window).width() > 1800) {

            $('.screen-flip-slider .ui.grid>.row').attr('five1 column row');
            fixHeight(.19);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }

        else if ($(window).width() <= 768 && $(window).width() > 568) {
            console.log('33');
            $('.screen-flip-slider .ui.grid>.row').attr('class','three1 column row gridabc');
            fixHeight(.33);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }
        else if ($(window).width() <= 568 && $(window).width() > 300) {
            console.log('50');
            //$('.main-slider').removeAttr("style") ;

            //$('.screen-flip-slider .ui.grid>.row').attr('class','two column row gridabc');
            $('.screen-flip-slider').hide();
            $('.mobile-flip-slider').show();
            $('.mobile-flip-slider .ui.grid>.row').attr('class','two1 column row gridabc');
        }

        else{
            $('.screen-flip-slider .ui.grid>.row').attr('class','five column row');
            console.log('20');
            fixHeight(.20);
        }

    }
    window.onresize = checkWidth;
    checkWidth();
    var page = $("#load_more").data('val');

    var total_product_count="<?php print_r($all_products_count); ?>";
    var per_page=3;
    //alert(page);

    var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
    //alert(total_pages);
    if(page<=total_pages){
        //alert('onload');
        //getcountry(page);
    }



    $(window).scroll(function(e){

        //if ($(window).scrollTop() == $(document).height() - $(window).height()){
        if (parseInt($(window).scrollTop()) == $(document).height() - $(window).height()){
            //alert("hi");
            e.preventDefault();
            // $("#load_more").data('val');
            page = $("#load_more").data('val');
            var total_product_count="<?php print_r($all_products_count); ?>";
            var per_page=4;
            // alert(page);
            var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<=total_pages){
                getcountry(page);
            }else{
                $("#show").show();
            }
        }
    });

</script>
<script src="<?php echo base_url(); ?>content/js/hover.js"></script>

<script>
    // very simple to use!
    $(document).ready(function() {
        $('.js-activated').dropdownHover().dropdown();
    });
</script>
