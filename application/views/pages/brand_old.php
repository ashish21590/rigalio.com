<?php
//print_r($products); exit;
$brand_id=print_r($brand);
?>
<div class="brand_pg_tagline">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 content_headline">
            <div class="brand-info">
                <div class="brand-img col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <img src="<?php echo base_url().$brand_details[0]['brand_image']; ?>" class="img-responsive">
                </div> <!--/ brand-img -->
                <!--<div class="brand-name col-lg-7 col-md-7 col-sm-6 col-xs-12">
              <h2><?php echo $brand_details[0]['brand_Name']; ?> </h2>
           </div> --> <!--/brand-name -->
            </div> <!--brand-info -->
            <div class="product-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><label>Brand <span class="brand-name"><?php echo $brand_details[0]['brand_Name']; ?>
                                <div class="tooltip cat-follow-popup">
                                    <div class="tab">
                                        <div class="tab-cell">
                                            <div class="img-part"> <span><img class="img-responsive" src="<?php echo base_url();?><?php  echo $products[0]['brand_image']; ?>"> </span></div>
                                            <div class="follow-con <?php  echo $products[0]['category_color']; ?>">
                                                <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                <h1 class=""><a href="<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>"><?php  echo $products[0]['brand_Name']; ?></a></h1>
                                                <div class="follow-status" id="<?php echo $products[0]['brandId']; ?>">
                                                    <button class="follow-brand-btn follow">Follow brand</button><!--<button class="follow-brand-btn follow-hover" style="display:none;">Follow brand</button> -->
                                                    <button class="follow-brand-btn following" style="display:none;"><span><i class="fa fa-check"></i>
                         </span>Following</button>
                                                    <button class="follow-brand-btn unfollow"><span><i class="fa fa-times"> </i> </span>unfollow</button> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
             </span> </label> </li>
                    <li><label>Established<span><?php echo $brand_details[0]['Establish']; ?> </span> </label> </li>
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 brand-follow">
                    <button class="follow-cat-btn normal brand-page-follow" id="<?php echo $brand_details[0]['brandId']; ?>">follow brand</button>
                </div>
            </div> <!--/product-info -->
        </div>
    </div>
</div> <!--/brand_pg_tagline -->
<div class="basic-cat-info brand-pg bg-white col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="basic-cat-info-inner col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 follow-list">
            <table>
                <tr>
                    <td class="fwd-icon-sectn">  <span> </span><span class="icomoon icon-sharing"></span></td>
                    <td><span><?php foreach($this->getdata->count_brandfollower($brand_details[0]['brandId']) as $count_no){
                                if($count_no['no'] != ''){
                                    echo $count_no['no'];
                                }
                                else
                                {
                                    echo '0';
                                }
                            }?>  </span> <span><img src="<?php echo base_url(); ?>content/images/icons/followers-icon.png"> </span></td>
                </tr>
            </table>
            <div class="fwd-social-icons sec7" style="display:none;">
                <ul class="cat-follow-icons">
                    <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>" class="fb"></a>
                    </li>
                    <li>

                        <a href="https://twitter.com/home?status=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>" class="twitter" ></a>
                    </li>
                    <?php /*?> <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */?>
                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                </ul>
                <div class="hidden-more-icons" style="display:none;">
                    <ul class="follow-icons-more">
                        <li>

                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>brand/<?php echo str_replace(" ","_",strtolower($products[0]['brand_Name']));?>/<?php echo $products[0]['brandId'];?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>brand/<?php echo str_replace(" ","_",strtolower($products[0]['brand_Name']));?>/<?php echo $products[0]['brandId'];?>" class="linkdin"></a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/share?url=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>" class="gplus"></a>

                        </li>
                        <li>

                            <a href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>&description=Testing" class="pini"></a>
                        </li>
                    </ul>
                </div>
            </div> <!--/inner forward icons social -->
        </div>

    </div>  <!--/basic-cat-info-inner -->
</div>  <!--/basic-cat-info -->
<!--category-pg-con -->
<div class="category-pg-con brand-pg-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="brand-tab-panel col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <ul  class="nav nav-pills">
                    <?php if(($management_details) !='' && ($brand_details[0]['legacy']) !=''){?>
                        <li><a href="#brand-legacy" data-toggle="tab">Legacy</a></li>
                    <?php } ?>
                    <li class="active"><a  href="#brand-product" data-toggle="tab" aria-expanded="true">Products</a></li>
                    <?php if($brand_details[0]['upcoming'] !=''){?>
                        <li><a href="#brand-upcoming" data-toggle="tab">Upcoming</a></li>

                    <?php } ?>
                    <li><a href="#social-feed" data-toggle="tab">Social Feed</a></li>
                </ul>
            </div> <!--/category-tab-panel -->

            <div id="myTabContent1" class="tab-content">
                <?php
                echo $brand_details[0]['legacy'];
                if($management_details){
                    foreach ($management_details as $review) {
                        ?>
                        <div class="legacy-sectn col-lg-12 col-md-12 col-sm-11 col-xs-11 nopadding">
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                                <h2><?php echo $review['heading']; ?></h2> <hr>
                            </div>
                            <div class="legacy-tagline col-lg-10 col-md-10 col-sm-11 col-xs-12 nopadding">
                                <div class="cxo-image col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding">
                                    <div class="cxo-img-border"><img src="<?php echo base_url().$review['author_image']; ?>"></div>
                                    <h5> <?php echo $review['author_name']; ?></h5>
                                    <h6> <?php echo $review['author_designation']; ?></h6>
                                </div> <!--/cxo-image -->
                                <div class="legacy-text col-lg-8 col-md-8 col-sm-8 col-xs-12 nopadding">
                                    <p class="nomargin"><?php echo $review['description']; ?></p>
                                </div> <!--/legacy-text -->
                            </div> <!-- /legacy-tagline -->
                        </div>
                    <?php } }?> <!-- /legacy-sectn ends -->
            </div> <!-- brand legacy tab content ends -->
            <div class="brand-product-con tab-pane fade active in" id="brand-product">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                        <h2>More posts like this one </h2>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 whats_new_content">
                    <div class="grid" id="masonry-grid">
                        <?php
                        $sr = 0;
                        foreach ($products as $product) {
                            ?>
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category <?php echo $product['category_color']; ?>">
                                        <div class="category_img">

                                            <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive">
                                            <div class="hover-content-cat">
                                                <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">

                                                    <div class="tab">
                                                        <div class="tab-in">
                                                            <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                            <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                                                <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"></div>
                                                            <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span></div>
                                                            <h4><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","-",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>"><?php echo $product['brand_Name']; ?></a></h4>
                                                            <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                                                                <!-- <button class="follow-btn follow">Follow brand</button>-->
                                                                <button class="follow-brand-btn follow">Follow brand</button>
                                                                <button class="follow-brand-btn following" style="display:none;"><span><i class="fa fa-check"></i>
                                 </span>Following</button>
                                                                <button class="follow-brand-btn unfollow"><span><i class="fa fa-times"> </i> </span>unfollow</button> </a>
                                                        </div>  <!--/tab-in -->
                                                    </div> <!--/tab -->
                                                </a>
                                            </div> <!--/hover-content-cat -->
                                        </div>
                                        </a>
                                        <div class="category_content">
                                            <div class="relative-struct">
                                                <h1 class="hea"><a href="<?php echo base_url(); ?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>/overview"><?php echo $product['category_Name'];?></a>
                                                    <div class="tooltip cat-follow-popup">
                                                        <div class="tab">
                                                            <div class="tab-cell">
                                                                <div class="img-part"><span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                                                                <div class="follow-con">
                                                                    <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                                    <h1 class=""><a href="<?php echo base_url();?>subcategory/<?php echo str_replace(" ","-",strtolower($product['subCategory_Name']));?>/<?php echo $product['subCategoryId'];?> "><?php echo $product['category_Name']; ?></a> </h1>
                                                                    <div class="follow-status" id="<?php echo $product['categoryId']; ?>">
                                                                        <a href="javascript:void(0)"><button class="follow-cat">Follow category</button>
                                                                            <button class="following-cat" style="display:none !important;"><span><i class="fa fa-check"></i></span>following</button></a>
                                                                        <!-- <a href="javascript:void(0)"><button class="follow-hover" style="diplay:none;">Follow category</button></a>-->
                                                                        <!--<a href="javascript:void(0)"><button class="following bg-green" style="diplay:none;"><span><i class="fa fa-check"></i></span>following</button></a>
                                                                        <a href="javascript:void(0)"><button class="unfollow border-green font-green" style="diplay:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a> -->
                                                                        <button class="unfollow-cat" style="display:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div> </h1>
                                            </div> <!--/relative-struct -->

                                            <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                                        </div> <!--/category_content-->
                                        <?php

                                        $count_people=[];
                                        $comment_people = [];
                                        foreach($this->getdata->crownuser_detail($product['productId']) as $count_no)
                                        {
                                            $count_people[]=$count_no;
                                        }
                                        foreach($this->getdata->commentuser_detail($product['productId']) as $countcomment_no)
                                        {
                                            $comment_people[]=$countcomment_no;
                                        }

                                        $sum_total = count($count_people) + count($comment_people);
                                        if ($sum_total != '0')
                                        {
                                            ?>

                                            <div class="hidden-status">
                                                <h6><?php
                                                    //print_r($count_people);
                                                    $m=[];
                                                    foreach($count_people as $me){

                                                        $m[]=$me['user_id'];
                                                    }

                                                    $m1=[];
                                                    foreach($comment_people as $me){

                                                        $m1[]=$me['user_id'];
                                                    }
                                                    // echo count(array_unique(array_merge($m,$m1)));
                                                    //print_r(array_merge($count_people,$comment_people));
                                                    //$array1 = array_merge ($count_people,$comment_people);
                                                    //$array2 = array_unique($array1);
                                                    //$array3 = array_keys($array2);
                                                    //print_r($array2);
                                                    echo count(array_unique(array_merge($m,$m1)));;  ?> People Reacted On This </h6>  <span class="profile-angle"><i class="fa fa-angle-down"></i></span></div>
                                        <?php  } ?>

                                        <div class="category_options">
                                            <table>
                                                <tr>
                                                    <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff/(60*60*24))+1);?>days ago</p> </td>
                                                    <td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-crown"> </span></td>
                                                    <td class="comment-sectn <?php echo $product['category_color']; ?>"><span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-chat"> </span> </td>
                                                    <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span> </span> <span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                            </table>

                                            <div class="fwd-social-icons sec7" style="display:none;">
                                                <ul class="cat-follow-icons">
                                                    <li>
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="fb"></a>
                                                    </li>
                                                    <li>


                                                        <a href="https://twitter.com/home?status=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="twitter" ></a>
                                                    </li>
                                                    <?php /*?><li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */?>
                                                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                                                </ul>
                                                <div class="hidden-more-icons" style="display:none;">
                                                    <ul class="follow-icons-more">
                                                        <li>

                                                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="linkdin"></a>
                                                        </li>
                                                        <li>
                                                            <a href="https://plus.google.com/share?url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="gplus"></a>

                                                        </li>
                                                        <li>

                                                            <a href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&description=Testing" class="pini"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                                <?php
                                                $regif = $this->session->userdata('registrationid');
                                                if(!$regif || $userdata[0]['profile_picture']=='')
                                                {
                                                    ?>
                                                    <span><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                                <?php } else{ ?>
                                                    <span><img src="<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>" class="img-responsive img-circle"> </span>
                                                <?php }  ?>
                                                <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div> <!--/category_options-->

                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <?php $sr++ ;
                        }
                        $this->session->set_userdata('sr',$sr);
                        //$ses_var = $this->session->userdata('sr);
                        ?>
                    </div>


                </div> <!--ends whtas new content -->
                <!--/column-->

                <!-- mywork place ends here-->
                <div id="brand_count" style="display:none;"><?php echo $brand_count[0]['no']; ?></div>
                <div class="container loading-sectn" style="text-align: center"><button class="btn" id="load_more" style="background:none" data-val = "1"><img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>loader.gif" class="img-responsive"> </button></div>
                <div id="show" style="display:none" class="nomore">No more products to showcase</div>

            </div> <!-- brand products tab content ends -->
            <div class="upcoming-con tab-pane fade" id="brand-upcoming">
            </div> <!-- brand products tab content ends -->
            <div class="social-feed-con tab-pane fade" id="social-feed">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                    <h2>social feed</h2>
                    <hr>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content">
                    <div class="grid" id="masonry-grid">
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-facebook" aria-hidden="true"></i></span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="date-status"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>
                                                <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">

                                            <div class="few-comm">
                                                <table>
                                                    <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                    <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span> </td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                    <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                </table>
                                            </div>
                                            <div class="see-all-com"> see all  </div>
                                            <span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>

                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/fashionbig.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-twitter" aria-hidden="true"></i> </span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="date-status"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">

                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-instagram" aria-hidden="true"></i></span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr class="">
                                                <td class="date-status cat_travel"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/travel.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="date-status"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="date-status"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/fashion.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr class="">
                                                <td class="date-status cat_travel"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category">
                                    <div class="category_img">
                                        <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                    </div> <!--/category_img-->
                                    <div class="category_content">
                                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                        <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                    </div> <!--/category_content-->
                                    <div class="category_options">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="date-status"><p>5 days ago </p>  </td>
                                                <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="display:none;" class="fwd-social-icons sec7">
                                            <ul class="cat-follow-icons">
                                                <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                            </ul>
                                            <div style="display:none;" class="hidden-more-icons">
                                                <ul class="follow-icons-more">
                                                    <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                    <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                </ul>
                                            </div>
                                        </div> <!--/inner forward icons social -->
                                        <div style="display: none;" class="write-comment">
                                            <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                            <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                        </div> <!--/write-comment -->
                                    </div><!--/category_options-->
                                </div> <!--/category-->
                            </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                    </div>
                </div>
                <!--/whats_new_content -->
            </div><!--/social-feed-con tab content ends -->



        </div> <!--/tab-content -->
        <div class="product-share-sectn brand-pg col-lg-7 col-md-7 col-sm-11 col-xs-12">
            <h5>Share This </h5>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                <div class="product-inner-social col-lg-5 col-md-5 col-sm-12 col-xs-12 nopadding">
                    <ul class="follow-icons">
                        <li><a class="facebook-follow" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>"></a></li>
                        <li><a class="twitter-follow" href="https://twitter.com/home?status=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>"></a></li>
                        <?php /*?> <li><a class="instagram-follow" href="" target="_blank"></a></li><?php */?>
                        <li><a class="linkedin-follow" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>brand/<?php echo str_replace(" ","_",strtolower($products[0]['brand_Name']));?>/<?php echo $products[0]['brandId'];?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>brand/<?php echo str_replace(" ","_",strtolower($products[0]['brand_Name']));?>/<?php echo $products[0]['brandId'];?>"></a></li>
                        <li><a class="rss-follow" href="https://plus.google.com/share?url=<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($products[0]['brand_Name'])); ?>/<?php echo $products[0]['brandId'];?>"></a></li>

                    </ul>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding btn-sectn"><a href="<?php echo $brand_details[0]['official_link']; ?>"><button class="goto-web-btn">Go to official website</button> </a><button class="query-btn" id="btn-sendform">Send a query</button></div>

            </div>
        </div> <!-- /product-share-sectn -->
        <div class="sendquery-form" style="display: none;">
            <form class="col-lg-6 col-md-7 col-sm-10 col-xs-12" name="sendaquery" method="post" >
                <label>Send a query</label>
                <p id="msgbrand"> </p>
                <div id="myhide1" class="myhide1">

                    <input type="text" name="name" id="namebrand" value="" placeholder="NAME" class="textbox textbox2">
                    <input type="text" name="contactno" id="contactnobrand" value="" placeholder="CONTACT NO." class="textbox textbox2">
                    <input type="text" name="emailid" id="emailidbrand" value="" placeholder="EMAIL ID" class="textbox">
                    <!--<input type="text" name="date" id="date" value="" placeholder="CHOOSE A SUITABLE DATE" class="textbox textbox3">
                    <input type="text" name="time" id="time" value="" placeholder="AND TIME" class="textbox textbox4">
                    <input type="text" name="query" id="query" value="" placeholder="DEFAULT" class="textbox textbox5">-->
                    <textarea  class="textbox" id="querybrand" name="query" placeholder="QUERY" rows="5"> </textarea>
                    <input type="text" name="formname" id="formnamebrand" value="Send A Query" style="display:none;">
                    <button name="submit" class="try-now-submit1" type="button">SUBMIT</button>
                </div>
            </form>
        </div> <!--/try-now-con -->

    </div>
</div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
                <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
                <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
                <li><a href="<?php echo base_url();?>main/faq">Faq </a> </li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script type="text/javascript">
    var $container = jQuery('#masonry-grid');
    var $grid = $container.masonry({
        percentPosition: true,
        itemSelector: '.grid-item'
    });

    $(document).ready(function(e){
        $('#masonry-grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });
        var page = $("#load_more").data('val');
        var brand_count="<?php echo $brand_count[0]['no']; ?>";
        var brand_id="<?php print_r($brand); ?>";
        var per_page=4;
        //alert(page);

        var total_pages=Math.ceil(parseInt(brand_count)/parseInt(per_page));
        //alert(total_pages);


    });

    $(window).scroll(function(e){

        if ($(window).scrollTop() == $(document).height() - $(window).height()){
            //alert("hi");
            e.preventDefault();
            // $("#load_more").data('val');
            page = $("#load_more").data('val');
            var brand_count="<?php echo $brand_count[0]['no']; ?>";
            var brand_id="<?php print_r($brand); ?>";
            var per_page=4;
            // alert(page);
            var total_pages=Math.ceil(parseInt(brand_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<=total_pages){
                getbrand(page,brand_id);
            }
            else{
                $("#show").show();
            }



        }
    });

    var getbrand= function(page,brand_id){
        var data ={"page":page,
            "brand_id":brand_id};
        $("#loader").show();
        $('#load_more').data('val', ($('#load_more').data('val')+1));
        $.ajax({
            url:"<?php echo base_url() ?>myscroll/getbrand",
            type:'GET',
            data: data
        }).done(function(response){
            response = response.trim();
            var dom = $('<div/>').html(response).contents();
            $(dom).each(function(){
                if($(this).hasClass('wall-column')){
                    $grid.append($(this));
                    var el = this;
                    el.getElementsByTagName('img')[0].onload = function(){console.log(el);$grid.masonry( 'appended', $(el) )  };

                }
            })
            $("#loader").hide();
        });
    };


    var data ={"count":10,
        "fb_userid":'bmw',
        "twitter_userid":'bmw'
    };

    $.ajax({
        type: "GET",
        url: "http://www.rigalio.com/x/",

        //dataType: 'json',
        data: data,
        success: function (html) {
            // alert(html);
           // $("#image_popup_comments").html(html);
            alert(html);
            //$("#"+id).next().css( "background", "yellow" );
            //alert(html);
        }
    });  //ajax ends here

</script>



