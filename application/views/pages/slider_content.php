<div style="width: 100%; display: block; clear: both;" id="slider_load">
    <div style="width: 100%; display: block; clear: both;">
        <div class="main-slider-container hidden-xs screen-flip-slider">
            <div class=" main-slider">
                <div class="ui grid invisible">
                    <div class="five column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Puiforcat launches crystal glassware</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Dreams Venice has to offer</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The epitome of fine dining </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The new 7 Series is loaded with luxury</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">


                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Berluti: Fashioning Italian style </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>LVMH endorses sustainable luxury</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Handcrafted luxury from Bentley</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Soak in the sights in downtown London</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 2.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A rare collection of exquisite tableware</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A slice of heaven in exotica</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The rarest manuscripts up for auction at Christie’s</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The most expensive restaurant in the world</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A dessert crafted from truffles</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" column hidden-sm">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 3.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The Mediterranean beckons the luxury traveller</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 4.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The rise of a fashion czarina</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 5.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Experience Michelin Star restaurants in the city of love</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column hidden-sm">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>An I Phone app for curious voyagers</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>This rare Kodak is a collector’s dream</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Apple Watch is the newest kid on the horology kaleidoscope</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A boutique hotel nestled in a coffee plantation</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>


        <!--slider for mobile -->
        <div class="main-slider-container mobile-flip-slider hidden-lg hidden-md hidden-sm">
            <div class=" main-slider">
                <div class="ui grid invisible">
                    <div class="two column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">

                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.jpg"
                                                    class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.jpg"
                                                class='ui image'>


                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img
                                                src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg"
                                                class='ui image'>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"></div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>

        <!--slider for mobile ends -->
    </div>
</div>





<link href="<?php echo base_url(); ?>content/css/carousel.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/semantic.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/myflip.js"></script>
