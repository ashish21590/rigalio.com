<?php
//print_r($userdata); exit;
?>


<div class="settings-tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding settings-info"> 
    <?php if($userdata[0]['profile_picture'] == ''){ ?>
     <img src="<?php echo base_url(); ?>content/images/profile/profile_pcture.jpg" class="img-responsive"> 
    <?php } else{ ?>
     <span class="pf-img" style="background: url(<?php echo base_url(); ?><?php echo $userdata[0]['profile_picture'];?>);"> </span>
     
     <?php } ?>
     <span class="user-name"><?php echo $getuserdataall[0]['firstname'];?> <?php echo $getuserdataall[0]['lastname'];?></span> </div>
</div>  <!--/settings-tagline -->
<!--settings page con -->
<div class="settings-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding nomargin">
  <div class="container-fluid">
    <div class="row">
      <div class="settings-pg-con col-lg-6 col-md-6 col-sm-10 col-xs-12 nopadding">
        <div class="settings-tab-panel">
          <ul class="nav nav-pills">
            <li class="active"> <a href="#details" data-toggle="tab">details </a> </li>
            <li> <a href="#communication" data-toggle="tab"> communication</a> </li>
            <li> <a href="#privacy" data-toggle="tab">privacy </a> </li>
          </ul>
        </div> <!--/settings-tab-panel -->
        <div class="settings-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
         <div id="myTabContent2" class="tab-content">    
            <div class="details-tab-con tab-pane fade active in" id="details">
              <ul class="nopadding">
                <li> <label>Password</label><button class="edit">edit</button> 
                   <div class="password-info">
                     <ul class="nopadding">
                     <p id="usererror"></p>
                       <li><input type="password" id="currentpass" name="currentpass" value="" placeholder="CURRENT PASSWORD" onBlur="existpassword()" class="textboxd"></li>
                       <li class="passwrdshow"><input type="password" name="newpass" placeholder="NEW PASSWORD" id="newpass" value="" class="textboxd " /><span class="text">Show</span><input id="test2" type="checkbox" class="hiden-chck" />  </li>

                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                     <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                     <button type="button" class="save-btn" id="savepass123">save</button>
                     </div>
                   </div> <!--/Password-info -->
                </li>
                <li> <label>Email address</label> <button class="edit">edit </button> 
                  <div class="email-settings">
                    <h5>We already have your following email addresses </h5>
                    <ul class="nopadding">
                      <li><label><?php echo $getuserdataall[0]['email'];?> </label> <button class="primary">primary</button></li>
                      <?php
            foreach ($useremail as $emailid) 
            {
            ?>
                      <li><label><?php echo $emailid['useremail'];?></label><a href="<?php echo base_url();?>usersetting/makeprimary/<?php echo $emailid['useremailid']; ?>"> <button class="make-primary">make primary</button></a> <a href="<?php echo base_url();?>usersetting/deleteemail/<?php echo $emailid['useremailid']; ?>"><button class="remove-btn">Remove</button></a> </li>
                      <?php }?>
                    </ul>
                    <h4 class="add-email-click"> Add new email address</h4>
                    <div class="add-email">
                    <input type="text" name="addemail" value="" id="addemail" placeholder="NEW EMAIL ADDRESS" class="textboxd addemail">
                    <!--<button class="verify-btn">verify</button>
                    <p class="verify-msg" style="display: none;">Check your email. New email address is updated </p>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <button class="save-btn" id="saveemail">save</button></div>
                    </div>

                  </div> <!--/email-settings -->
                </li>
                <li> <label>date of birth</label> <button class="edit">edit </button> 
                   <div class="dob-info">
                     <ul class="nopadding">
                       <li><input type="text" name="currentdob" id="currentdob" value="<?php echo $getuserdataall[0]['dob'];?>" class="textboxd"></li>
                       <li><input type="text" name="newdob" value="" placeholder="ENTER NEW DATE" id="newdob" class="textboxd"></li>
                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button id="savedob" class="save-btn">save</button></div>
                   </div> <!--/dob-info -->
                </li>
                <li> <label>Phone number</label> <button class="edit">edit </button>
                  <div class="phone-info">
                  <?php
          if($getuserdataall[0]['phone'] == '')
          {
           ?>
                   <h5>Please Add Your Phone Number</h5>
                   <?php
          }else {
            ?>
                     <h5>We already have your following phone numbers </h5>
                    <ul class="nopadding">
                      <li><label>+91 <?php echo $getuserdataall[0]['phone'];?></label> <button class="primary">primary</button></li>
                      <?php
            foreach ($userphoneno as $phoneno) 
            {
            ?>
                      <li><label>+91 <?php echo $phoneno['phoneno'];?> </label><a href="<?php echo base_url();?>usersetting/makeprimaryphone/<?php echo $phoneno['phoneid']; ?>"> <button class="make-primary">make primary</button></a> <a href="<?php echo base_url();?>usersetting/deletephoneno/<?php echo $phoneno['phoneid']; ?>"><button class="remove-btn">Remove</button> </a></li>
                       <?php }?>
                    </ul>
                    <?php } ?>
                    <h4 class="add-ph-click"> Add new Phone no.</h4>
                    <div class="add-ph">
                      <input type="text" name="addphone" id="addphone" value="" placeholder="NEW PHONE NO." class="textboxd">
                     <!-- <button class="verify-btn">verify</button>-->
                      <!--<p class="verify-ph" style="display: none;">Check your phone. New phone no. is updated </p>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <button class="save-btn" id="savephone">save</button>
                    </div>
                  </div> <!--/phone-info --> 
                </li>
              </ul>
            </div> <!--/details-tab-con -->
            <div class="comm-tab-con tab-pane" id="communication">
              <ul>
                <li> <label>Frequency of information</label><button class="edit">edit </button> 
                  <div class="freq-info">
                    <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="info" value="Weekly" <?php echo ($getuserdataall[0]['reminder_type']=='Weekly')?"checked":"" ;?> ><label for="radio1">Weekly</label></li>
                    <li><input id="radio2" type="radio" name="info" value="Fortnightly" <?php echo ($getuserdataall[0]['reminder_type']=='Fortnightly')?"checked":"" ;?>><label for="radio2">Fortnightly</label></li>
                    <li><input id="radio3" type="radio" name="info" value="Monthly" <?php echo ($getuserdataall[0]['reminder_type']=='Monthly')?"checked":"" ;?> ><label for="radio1">Monthly</label></li>
                       
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="saveinfo">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/freq-info-inner -->
                </li>
                <li> <label>mode of communication </label> <button class="edit">edit </button> 
                   <div class="mode-comm">
                      <ul class="nopadding">
                    <li><input id="check8" type="checkbox" name="keepyou[]" value="Send me information via text messages"                        <?php 
            $exp_val=explode(',',$getuserdataall[0]['contact_type']);
            for($i=0;$i<count($exp_val);$i++){
            if ($exp_val[$i]=='Send me information via text messages') {echo "checked"; } }?>>
                    <label for="check8">Send me information via text messages </label></li>
                      <li><input id="check9" type="checkbox" name="keepyou[]" value="Call me to update" 
            <?php
            //$exp_val=explode(',',$userdata[0]['contact_type']);
            for($i=0;$i<count($exp_val);$i++){
            if ($exp_val[$i]=='Call me to update') { echo "checked";} }?>><label for="check9">Call me to update </label></li>
                      <li><input id="check10" type="checkbox" name="keepyou[]" value="E-mail me the information" <?php for($i=0;$i<count($exp_val);$i++){ if ($exp_val[$i]=='E-mail me the information'){ echo "checked"; }}?>><label for="check10">E-mail me the information </label></li>
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="savecommunication">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/mode-comm-inner -->
                </li>
              </ul>
            </div> <!--/communication-tab-con -->
            <div class="privacy-tab-con tab-pane" id="privacy">
              <ul>
               <!-- <li> <label>profile visibility</label> <button class="edit">edit </button> 
                  <div class="profile-visibile">
                     <h5>Who can see your profile on Rigalio </h5>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seeprofile" value="0" checked><label for="radio1">private</label></li>
                    <li><input id="radio2" type="radio" name="seeprofile" value="1"><label for="radio2">members</label></li>
                 </ul>
                   </div> 
                </li>-->
                <li> <label>crowns</label> <button class="edit">edit </button>
                  <div class="crown-info">
                     <h5>Who can see your crowns on Rigalio </h5>
                     <?php
           if(count($usercrown)==0){
            ?>
                      <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seecrown" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seecrown" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seecrown" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seecrown" value="0" <?php echo ($usercrown[0]['isActive']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seecrown" value="1" <?php echo ($usercrown[0]['isActive']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seecrown" value="2" <?php echo ($usercrown[0]['isActive']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                      <?php }?>
                   </div>
                </li>
                <li> <label>comments</label> <button class="edit">edit </button> 
                  <div class="comments-setting">
                     <h5>Who can see your comments on Rigalio </h5>
                     <?php
           if(count($usercomment)==0){
            ?>
                      <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seecomment" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seecomment" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seecomment" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                     <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seecomment" value="0" <?php echo ($usercomment[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seecomment" value="1" <?php echo ($usercomment[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seecomment" value="2" <?php echo ($usercomment[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                     <?php }?>
                   </div> <!-- /comments-setting-->
                </li>
            <!--    <li> <label>share</label> <button class="edit">edit </button>
                  <div class="share-setting">
                     <h5>Who can see your share on Rigalio </h5>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                   </div>
                </li>-->
                <li> <label>status</label> <button class="edit">edit </button>
                <div class="status-setting">
                     <h5>Who can see your status on Rigalio </h5>
                     <?php
           if(count($userpost)==0){
            ?>
                      <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seestatus" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seestatus" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seestatus" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seestatus" value="0" <?php echo ($userpost[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seestatus" value="1" <?php echo ($userpost[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seestatus" value="2" <?php echo ($userpost[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                      <?php }?>
                   </div> <!-- /status-setting-->  
                </li>
                <li> <label>photos</label><button class="edit">edit </button> 
                <div class="photos-setting">
                     <h5>Who can see your photos on Rigalio </h5>
                     <?php
           if(count($getuserpostimage)==0){
            ?>
                       <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seephotos" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seephotos" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seephotos" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seephotos" value="0" <?php echo ($getuserpostimage[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seephotos" value="1" <?php echo ($getuserpostimage[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seephotos" value="2" <?php echo ($getuserpostimage[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                     <?php }?>
                   </div> <!-- /photos-setting-->
                </li>
                <li> <label>people</label><button class="edit">edit </button> 
                  <div class="ppl-fol-setting">
                     <h5>Who can see your people following and followers on Rigalio </h5>
                      <?php
           if(count($getfollower)==0){
            ?>
                      <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seefollower" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seefollower" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seefollower" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seefollower" value="0" <?php echo ($getfollower[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seefollower" value="1" <?php echo ($getfollower[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seefollower" value="2" <?php echo ($getfollower[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php }?>
                   </div> <!-- /people follow-setting-->
                </li>
                <li><label>Brand</label> <button class="edit">edit </button>
                 <div class="brandfol-setting">
                     <h5>Who can see brand following and followers on Rigalio </h5>
                      <?php
					 if(count($getbrand)==0){
					  ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seebrand" value="0" class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seebrand" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seebrand" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php } else{ ?>
                  
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seebrand" value="0" <?php echo ($getbrand[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seebrand" value="1" <?php echo ($getbrand[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seebrand" value="2" <?php echo ($getbrand[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php }?>
                   </div> <!-- /brandfolowing-setting-->  
                </li>
              </ul>
            </div> <!--/privacy-tab-con -->
         </div>   
        </div> <!--/settings-blocks -->
          
      </div> <!--/settings-pg-con -->
    </div>
  </div>  
</div>
<!--settings-pg-con ends -->
  </body>
<script>
$.toggleShowPassword({
   field: '#newpass',
   control: '#test2'
});
</script>
<script>
// date picker script
$(function() {
 $( "#newdob" ).datepicker();
});

</script>

</html>