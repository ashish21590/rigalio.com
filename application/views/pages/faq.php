<!--faq-pg con -->
<div class="faq-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="faq-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h2>FAQ</h2> <hr>
           </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
                <div class="faq-start"><img src="<?php echo base_url(); ?>content/images/icons/faq-start-icon.png"></div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is the difference between a quartz and automatic watch? </span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>As the name suggests, automatic watches, or self-winding watches, are wound by the movements of the user's wrist and do not need a battery. these watches are equipped with a special mechanism that uses the movement of the wrist to store energy that powers the watch. a quartz watch, on the other hand, runs on battery, which has to be replaced at regular intervals. </p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <div class="load-more-faq"><a href="#">Load more</a> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           
        </div>
      </div> <!--/faq-pg-con inner ends -->
      

    </div>
  </div>  
</div><!--faq-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url();?>main/faq">Faq </a> </li>
       </ul>
    </div>
  </div>
</footer>
  </body>
<script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>content/js/bootstrap-hover-dropdown.js"></script>
<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>
<style>

</style>

  
</html>
