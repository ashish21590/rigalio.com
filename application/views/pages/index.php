<!-- Carousel-->
<div id="myslider123"></div>

<div class="main_content">
    <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
                        <h2 ng-model="heading"><?php print_r($heading); ?></h2>
                        <hr>
                    </div>
                </div> <!--/content tagline-->
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content">
                    <div class="grid" id="masonry-grid">
                        <?php
                        $sr = 0;
                        foreach ($products as $product) {
                        ?>

                        <div class="wall-column grid-item">
                            <div class="wall-item">
                                <div class="category <?php echo $product['category_color']; ?>">
                                    <div class="category_img">

                                        <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>"
                                             class="img-responsive">
                                        <!--<div class="hover-content-cat"> -->
                                        <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                                        <div class="hover-content-cat">


                                                <div class="tab">
                                                    <div class="tab-in">
                                                        <div class="cat-zoom-icon"
                                                             id="<?php echo $product['subCategoryId']; ?>"><span
                                                                class="fa fa-plus"
                                                                id="<?php echo $product['productId']; ?>"></span></div>
                                                        <div class="cat-plus-icon" style="display:none;"
                                                             id="<?php echo $product['subCategoryId']; ?>">
                                                            <span class="fa fa-minus"
                                                                  id="<?php echo $product['productId']; ?>"></div>
                                                        <div class="cat-minus-icon" style="display:none;"><span
                                                                class="fa fa-minus"></span></div>


                                                        <h4>
                                                            <a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>"><?php echo $product['brand_Name']; ?></a>
                                                        </h4>
                                                        <a href="javascript:void(0)" class="myan follow-brand-sectn"
                                                           id="<?php echo $product['brandId']; ?>">
                                                            <button class="follow-brand-btn follow">Follow brand
                                                            </button>
                                                            <!--<button class="follow-brand-btn follow-hover" style="display:none;">Follow brand</button> -->
                                                            <button class="follow-brand-btn following"
                                                                    style="display:none;"><span><i
                                                                        class="fa fa-check"></i>
                         </span>Following
                                                            </button>
                                                            <button class="follow-brand-btn unfollow"><span><i
                                                                        class="fa fa-times"> </i> </span>unfollow
                                                            </button>
                                                        </a>


                                                    </div>
                                        </div> <!--/tab structure -->
                                    </div> <!--/hover-content-cat -->
                                </div>
                            <!--/category_img-->
                                <div class="category_content">
                                    <div class="relative-struct">
                                        <h1 class="hea"><a
                                                href="<?php echo base_url(); ?>category/<?php echo str_replace(" ", "_", strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId']; ?>"><?php echo $product['category_Name']; ?></a>
                                            <div class="tooltip cat-follow-popup">
                                                <div class="tab">
                                                    <div class="tab-cell">
                                                        <div class="img-part"><span
                                                                class="category-iconic icomoon <?php echo $product['category_color']; ?> <?php echo $product['category_icon']; ?>"></span>
                                                        </div>
                                                        <div class="follow-con">
                                                            <img
                                                                src="<?php echo base_url(); ?>content/images/icons/follow-arrow.png"
                                                                class="arrow">
                                                            <h1 class=""><a
                                                                    href="<?php echo base_url(); ?>category/<?php echo str_replace(" ", "-", strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId']; ?>"><?php echo $product['category_Name']; ?></a>
                                                            </h1>
                                                            <div class="follow-status"
                                                                 id="<?php echo $product['categoryId']; ?>">
                                                                <a href="javascript:void(0)">
                                                                    <button class="follow-cat">Follow category</button>
                                                                    <button class="following-cat"
                                                                            style="display:none !important;"><span><i
                                                                                class="fa fa-check"></i></span>following
                                                                    </button>
                                                                </a>
                                                                <button class="unfollow-cat" style="display:none;">
                                                                    <span><i class="fa fa-times"> </i> </span>unfollow
                                                                </button>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </h1>
                                    </div> <!--/relative-struct -->

                                    <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                                    <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                                        <h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                                </div> <!--/category_content-->
                                <?php

                                $count_people = [];
                                $comment_people = [];
                                foreach ($this->getdata->crownuser_detail($product['productId']) as $count_no) {
                                    $count_people[] = $count_no;
                                }
                                foreach ($this->getdata->commentuser_detail($product['productId']) as $countcomment_no) {
                                    $comment_people[] = $countcomment_no;
                                }

                                $sum_total = count($count_people) + count($comment_people);
                                if ($sum_total != '0') {
                                    ?>

                                    <div class="hidden-status">
                                        <h6><?php
                                            //print_r($count_people);
                                            $m = [];
                                            foreach ($count_people as $me) {

                                                $m[] = $me['user_id'];
                                            }

                                            $m1 = [];
                                            foreach ($comment_people as $me) {

                                                $m1[] = $me['user_id'];
                                            }
                                            // echo count(array_unique(array_merge($m,$m1)));
                                            //print_r(array_merge($count_people,$comment_people));
                                            //$array1 = array_merge ($count_people,$comment_people);
                                            //$array2 = array_unique($array1);
                                            //$array3 = array_keys($array2);
                                            //print_r($array2);
                                            echo count(array_unique(array_merge($m, $m1)));; ?> People Reacted On
                                            This </h6>  <span class="profile-angle"><i
                                                class="fa fa-angle-down"></i></span></div>
                                <?php } ?>


                                <div class="category_options">
                                    <table>
                                        <tr>
                                            <td class="date-status <?php echo $product['category_color']; ?>">
                                                <p><?php $now = time();
                                                    $startDate = $product['created_on']; // or your date as well
                                                    $your_date = strtotime($startDate);
                                                    $datediff = $now - $your_date;
                                                    echo floor(($datediff / (60 * 60 * 24)) + 1); ?>days ago</p></td>
                                            <td class="crown-sectn <?php echo $product['category_color']; ?>"
                                                id="<?php echo $product['productId']; ?>"><span
                                                    id="crowncount"><?php foreach ($this->getdata->count_crown($product['productId']) as $count_no) {
                                                        echo $count_no['no'];
                                                    } ?> </span> <span class="icomoon icon-crown"> </span></td>
                                            <td class="comment-sectn <?php echo $product['category_color']; ?>">

                                               <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview#comment">

                                                <span
                                                    class="text"><?php foreach ($this->getdata->count_comment($product['productId']) as $count_no) {
                                                        echo $count_no['no'];
                                                    } ?></span></a> <span class="icomoon icon-chat"></span></td>
                                            <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>"
                                                id="fwd-id1"><span></span><span class="icomoon icon-sharing"></span>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="fwd-social-icons sec7" style="display:none;">
                                        <ul class="cat-follow-icons">
                                            <li>
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"
                                                   class="fb"></a>
                                            </li>
                                            <li>


                                                <a href="https://twitter.com/home?status=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"
                                                   class="twitter"></a>
                                            </li>
                                            <?php /* <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li> */ ?>
                                            <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                                        </ul>
                                        <div class="hidden-more-icons" style="display:none;">
                                            <ul class="follow-icons-more">
                                                <li>

                                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"
                                                       class="linkdin"></a>
                                                </li>
                                                <li>
                                                    <a href="https://plus.google.com/share?url=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"
                                                       class="gplus"></a>

                                                </li>
                                                <li>

                                                    <a href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview&description=Testing"
                                                       class="pini"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!--/inner forward icons social -->
                                    <div class="write-comment" style="display: none;"
                                         id="<?php echo $product['productId']; ?>">
                                        <?php
                                        $regif = $this->session->userdata('registrationid');
                                        if (!$regif || $userdata[0]['profile_picture'] == '') {
                                            ?>
                                            <span><img src="<?php echo base_url(); ?>content/images/icons/user.png"
                                                       class="img-responsive img-circle"> </span>
                                        <?php } else { ?>
                                            <span
                                                style="background:url(<?php echo base_url(); ?><?php echo $userdata[0]['profile_picture']; ?>);"
                                                class="pf-img"> </span>
                                        <?php } ?>
                                        <span class="commentbox"><a href="javascript:void(0)"><input type="text"
                                                                                                     class="home_comment"
                                                                                                     name="home_comment"
                                                                                                     value=""
                                                                                                     id="home_comment<?php echo $sr; ?>"></a> </span><a
                                            href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                                    </div> <!--/write-comment -->
                                </div> <!--/category_options-->

                            </div> <!--/category-->
                        </div> <!--/wall-item -->
                    </div> <!--/wall-column -->
                    <?php $sr++;
                    }
                    $this->session->set_userdata('sr', $sr);
                    //$ses_var = $this->session->userdata('sr);
                    ?>
                </div> <!--/grid ends -->
            </div>   <!--/whats_new_content -->

            <!--/column-->

            <!-- mywork place ends here-->
            <div id="total_product_count" style="display:none;"><?php print_r($all_products_count); ?></div>

            <div class="container loading-sectn" style="text-align: center">
                <button class="btn" id="load_more" style="background:none" data-val="2"><img style="display: none"
                                                                                             id="loader"
                                                                                             src="<?php echo str_replace('index.php', '', base_url()) ?>loader.gif"
                                                                                             class="img-responsive">
                </button>

            </div>
            <div id="show" style="display:none" class="nomore">No more products to showcase</div>
        </div> <!--/Whats_new_content-->
    </div> <!--/row-->
</div> <!--/container-fluid-->
</div> <!--/main_content--> <!--/main_content-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script>

</script>



<script type="text/javascript">
    // jQuery
    var fixHeight = function (images) {

        var w = 345;
        var h = 672;
        var W = window.innerWidth * images;
        var H = (h / w) * W;
        $('.main-slider').height(H)

    }
    function checkWidth() {


        if ($(window).width() > 1800) {

            $('.screen-flip-slider .ui.grid>.row').attr('five1 column row');
            fixHeight(.19);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }

        else if ($(window).width() <= 768 && $(window).width() > 568) {
            console.log('33');
            $('.screen-flip-slider .ui.grid>.row').attr('class', 'three1 column row gridabc');
            fixHeight(.33);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }
        else if ($(window).width() <= 568 && $(window).width() > 300) {
            console.log('50');
            //$('.main-slider').removeAttr("style") ;

            //$('.screen-flip-slider .ui.grid>.row').attr('class','two column row gridabc');
            $('.screen-flip-slider').hide();
            $('.mobile-flip-slider').show();
            $('.mobile-flip-slider .ui.grid>.row').attr('class', 'two1 column row gridabc');
        }

        else {
            $('.screen-flip-slider .ui.grid>.row').attr('class', 'five column row');
            console.log('20');
            fixHeight(.20);
        }

    }
    window.onresize = checkWidth;
    checkWidth();
    var page = $("#load_more").data('val');
    var total_product_count = "39";
    var per_page = 3;
    //alert(page);

    var total_pages = Math.ceil(parseInt(total_product_count) / parseInt(per_page));
    //alert(total_pages);
    if (page <= total_pages) {
        //alert('onload');
        //getcountry(page);
    }


    $(window).scroll(function (e) {

        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            //alert("hi");
            e.preventDefault();
            // $("#load_more").data('val');
            page = $("#load_more").data('val');
            var total_product_count = "39";
            var per_page = 4;
            // alert(page);
            var total_pages = Math.ceil(parseInt(total_product_count) / parseInt(per_page));
            //alert(total_pages);
            if (page <= total_pages) {
                getcountry(page);
            } else {
                $("#show").show();
            }
        }
    });
    var getcountry = function (page) {
        $("#loader").show();
        $('#load_more').data('val', ($('#load_more').data('val') + 1));
        $.ajax({
            url: "<?php echo base_url(); ?>myscroll/getCountry",
            type: 'GET',
            async: true,
            data: {page: page}
        }).done(function (response) {
            response = response.trim();
            // $("#masonry-grid").append(response);
            // $('#masonry-grid').masonry('reloadItems');
            var dom = $('<div/>').html(response).contents();
            $(dom).each(function () {
                if ($(this).hasClass('wall-column')) {
                    $grid.append($(this));
                    var el = this;
                    el.getElementsByTagName('img')[0].onload = function () {
                        console.log(el);
                        $grid.masonry('appended', $(el))
                    };
                }

            })
            $("#loader").hide();


            //scroll();
        });
        //$("#show").show();
    };


    // very simple to use!






        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>/main/myslider",

            //dataType: 'json',

            success: function (html) {

           $("#myslider123").html(html);

            }

        });  //ajax ends here



</script>

</body>
