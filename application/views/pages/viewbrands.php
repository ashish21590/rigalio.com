<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width" />
    

    <title>View people Followers Page</title>
    <script src="content/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="content/css/font-awesome.css">
    <link href="content/css/bootstrap.min.css" rel="stylesheet">
    <link href="content/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="content/css/header.css" />
    <link href="content/css/category.css" rel="stylesheet">
  
    <!-- Custom styles for this template -->
    <link href="content/css/carousel.css" rel="stylesheet">
    <link href="content/css/master.css" rel="stylesheet">
    <script src="content/js/bootstrap.min.js"></script>
    <script src="content/js/mycustom.js"></script>
    <script src="content/js/classie.js"></script>
    <script type="text/javascript" src="content/js/jssor.slider.debug.js"></script>
  </head>
  

  
<!-- NAVBAR
================================================== -->
  <body>
<nav class="navbar navbar-inverse navbar-fixed-top" id="header_nav">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-collapse-btn">
      <a href="javascript:void(0)" class="popup-btn"> <img src="content/images/icons/header-collapse-icon.png" class="img-responsive"> </a>
    </div>
    <div class="main-logo col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a href="javascript:void(0)"> <img src="content/images/main-logo.png" class="img-responsive"> </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 drop-search-wrap navbar-right">
      <a href="javascript:void(0)" class="search-icon"> <img src="content/images/icons/search-icon.png" class="img-responsive"></a>
    </div>
    <div class="search-hidden col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-search " style="display:none;">
            <form id="searchform" role="search" method="get" action="http://themes.birdwp.com/zefir-new/">
              <input type="text" name="s" id="s" class="search-field form-control" placeholder="I am Looking for">
            </form>
    </div>
</nav>
<div class="popup" id="popup">
    <div class="popup-inner">
       <a href="javascript:void(0)" class="close">CLOSE</a>
       <div class="main-menu">
          <ul class="main">
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Auto</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Fashion and Lifestyle</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Real Estate </a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Tarvel and Hotels</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Events</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
          </ul>
            
            <div class="main-social-sectn">
                <ul>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                </ul>
            </div> <!--/main-social-sectn" -->
        </div> <!-- /main-menu -->
    </div><!-- popup -inner-->
</div>  <!--/header popup -->     


<!--view people followers con -->
<div class="view-ppl-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="view-ppl-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="view-ppl-tag col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <button class="goback-btn pull-left browse-btn"><span class="icomoon icon-slider-left-arrow"></span>back</button><h3 class="
             mg-top">People </h3>
          </div> <!--/search-tagline -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding vault-part">
              <div class="vault-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="myTabContent3" class="tab-content">
                  <div class="ppl-vault-con tab-pane active in" id="ppl-vault">
                    <div class="ppl-inner-tab col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                      <ul class="nav nav-pills col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <li class="active"><a href="#ppl-followers" data-toggle="tab">followers</a></li>
                        <li ><a href="#ppl-following" data-toggle="tab">following</a></li>
                        <li ><a href="#pending" data-toggle="tab">pending</a></li>
                      </ul>
                    </div> <!--/ppl-inner-tab -->
                    <div class="ppl-folow-blocks nopadding col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div id="myTabContent4" class="tab-content">
                        <div class="ppl-followers-con tab-pane fade active in" id="ppl-followers">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding wall whats_new_content">
                            <div class="wall-column col-sm-6 col-xs-12">
                              <div class="wall-item">
                                <div class="category">
                                  <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                     <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                  </div>

                                </div> <!--/brand-result content -->

                              </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column col-sm-6 col-xs-12">
                              <div class="wall-item">
                                <div class="category">
                                  <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                     <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                  </div>

                                </div> <!--/brand-result content -->

                              </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column col-sm-6 col-xs-12">
                              <div class="wall-item">
                                <div class="category">
                                  <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                     <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                  </div>

                                </div> <!--/brand-result content -->

                              </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column col-sm-6 col-xs-12">
                              <div class="wall-item">
                                <div class="category">
                                  <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                     <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                  </div>

                                </div> <!--/brand-result content -->

                              </div> <!--/wall-item -->
                            </div> <!--/wall-column --> 
                          </div>   <!--/whats_new_content -->
                        </div> <!--/ppl-followers-con -->
                        <div class="ppl-following-con tab-pane" id="ppl-following">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wall nopadding whats_new_content">
                              <div class="wall-column col-sm-6 col-xs-12">
                                <div class="wall-item">
                                  <div class="category">
                                    <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                       <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                    </div>

                                  </div> <!--/brand-result content -->

                                </div> <!--/wall-item -->
                              </div> <!--/wall-column -->
                              <div class="wall-column col-sm-6 col-xs-12">
                                <div class="wall-item">
                                  <div class="category">
                                    <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                       <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                    </div>

                                  </div> <!--/brand-result content -->

                                </div> <!--/wall-item -->
                              </div> <!--/wall-column -->
                              <div class="wall-column col-sm-6 col-xs-12">
                                <div class="wall-item">
                                  <div class="category">
                                    <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                       <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                    </div>

                                  </div> <!--/brand-result content -->

                                </div> <!--/wall-item -->
                              </div> <!--/wall-column -->
                              <div class="wall-column col-sm-6 col-xs-12">
                                <div class="wall-item">
                                  <div class="category">
                                    <div class="profile-result-img"> <img src="content/images/search-pg/profile.png" class="img-responsive">
                                       <div class="brand-result-con"><span>Devendra Batra </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                    </div>

                                  </div> <!--/brand-result content -->

                                </div> <!--/wall-item -->
                              </div> <!--/wall-column -->
                             
                          </div>   <!--/whats_new_content -->
                        </div> <!--/ppl-following-con -->
                        <div class="pending-con tab-pane" id="pending">
                          <div class="followers-list">
                            <ul class="nopadding">
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn active">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn"  style="display: none;">ignore</button><button class="accept-btn active"  style="display: none;">accept</button> <button class="follow-back-btn active">follow back</button> </span> </li>
                            </ul>
                          </div> <!--/followers-list -->
                        </div> <!--/pending request -->
                      </div>
                    </div> <!--/ppl-folow-blocks -->
                    
                  </div>  <!--/ppl-vault-con -->
                  <div class="brand-vault-con tab-pane fade " id="brand-vault">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wall nopadding whats_new_content">
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                     </div>   <!--/whats_new_content --> 
                  </div> <!-- /brand-vault-con-->
                  <div class="cat-vault-con tab-pane" id="cat-vault">
                     bkjhkhk
                  </div> <!--/cat-vault-con -->
                </div>
              </div> <!--/vault-blocks -->
            </div> <!--/vault-part -->
      </div> <!--/view brands con -->
     

       
          

    </div>
  </div>  
</div>
<!--view brands con-pg-con ends -->

  </body>

<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>

  <script src="content/js/bootstrap.min.js"></script>
  <script src="content/js/bootstrap-hover-dropdown.js"></script>
</html>
