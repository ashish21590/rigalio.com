<!--aboutus-pg con -->
<div class="aboutus-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="aboutus-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h5>the essence of</h5>
             <h2> <span> <img src="<?php echo base_url(); ?>content/images/aboutus/big-motive-left.png" class="img-responsive"> </span> luxury<span><img src="<?php echo base_url(); ?>content/images/aboutus/big-motive-right.png" class="img-responsive"> </span> </h2> 
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 inner-con">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/first-divider.png" class="img-responsive"> 
             </div>
             <div class="inner-text">
               <p>It is not bound by the limits of imagination, but is the realm of the unimagined. It is the sense of accomplishing a goal, of experiencing the emotion of success, of undertaking an inspirational journey which is empowering, motivational and full of revelations. </p>
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/divider.png" class="img-responsive"> 
             </div>
             <div class="inner-text">
               <p>It is by no means a symbol of wealth, opulence or money. Real luxury is intrinsically invaluable, its attributes so irreplaceable that it is inherent to an individual’s personality like talent, opportunity, hard-work, or legacy.</p>
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/divider.png" class="img-responsive"> 
             </div>
             <div class="inner-text">
               <p>It embodies the values that form the foundation of an individual seeking inspiration from his life, his love and his dreams.  Luxury is an expression of the greatness that lies within man, it represents the physical manifestation of the qualities that define a human being.</p>
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/divider.png" class="img-responsive"> 
             </div>
             <div class="inner-text">
               <p>It is a world where the word best symbolizes the ordinary, beautiful denotes the average and expensive becomes a metaphor for the mediocre. Luxury is the deep taste of the moment, it is the mindfulness of ephemerality. </p>
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/divider.png" class="img-responsive"> 
             </div>
             <div class="inner-text">
               <p>It separates the novice from the proficient, the ignorant from the sophisticated, and the amateur from the connoisseur. Luxury is the ownership of a distinctive personal hallmark that has started to explore kingdoms, kingdoms that are now open.  </p>
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding divider-sectn"> 
                <img src="<?php echo base_url(); ?>content/images/aboutus/last-divider.png" class="img-responsive"> 
             </div>
           </div> <!--/inner-con -->
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 inner-con emblem-con">
             <div class="emblem-tag"><h5 class="col-lg-10 col-md-10 col-sm-12 col-xs-12">The insignia of Rigalio epitomizes the four great elements that are a symptomatic of success in contemporary society</h5>
                <div class="left-curl"><img src="<?php echo base_url(); ?>content/images/aboutus/big-motive-left.png"> </div>
                <div class="right-curl"><img src="<?php echo base_url(); ?>content/images/aboutus/big-motive-right.png"> </div>
             </div>
             <div class="emblem-section left-inner">
                <div class="emblem-img"><img src="<?php echo base_url(); ?>content/images/aboutus/sun.png" class="img-responsive"> </div>
                <h5>The eminence and supremacy of the Sun </h5> <hr>
                <p>Sun, the source of all life on the planet, symbolizes the intransient and omnipotent individual whose energies drive the surroundings. He is the axis whose standing in life and society makes him invaluable in terms of institutions and people that revolve around his being. Helios (Hêlios or Êelios), that is, the sun, or the god of the sun in Greek mythology has being described by Homer as giving light to both gods and men. Later poets have marvellously embellished this simple notion: they tell of a most magnificent palace of Helios in the east, containing a throne occupied by the god, and surrounded by personifications of the different divisions of time. </p>
                <p>Rigalio encapsulates this vision of the gods. It vividly portrays this transcendental quality in efficacious mortals and gratifies their requests to be an avenue which catalogues the best in definitive luxury the world over. The element which occupies the peak of the emblem signifies the importance that has been placed in the element. The Rigalio emblem embodies the belief that human beings who possess the rarefied qualities that define the brightest star in the solar system will be in possession of the same virtues that will make them a powerful, influential and responsible human being. </p>
             </div> <!--/emblem-section -->
             <div class="emblem-section right-inner">
                <div class="emblem-img"><img src="<?php echo base_url(); ?>content/images/aboutus/key.png" class="img-responsive"> </div>
                <h5>The virtue and opportunity of the key </h5> <hr>
                <p>Key, he admittance to every single occasion in life is the most understated, but in no trifle way the second element that defines Rigalio. The importance of something so banal lies in the intrinsic nature of the element and the surreal prospects it bestows on its patrons. In the Bible (Matthew 16:19) Jesus Christ says to Peter, “I will give you the keys of the kingdom of heaven…”The key in religious texts and mythology has been a source of great power, responsibility and opportunity. The Coat of Arms of the Vatican state showcases two keys- one in gold and the other one in silver depicting holiness and power with the gold key signifying that the power reaches to heaven and the silver key that it extends to all the faithful on earth, the interlacing indicating the linking between the two aspects of the power. It has also been engaged to defend some of the most valuable possessions and secrets that are of fundamental importance to mankind.</p>
                <p>The visualization and the meaning attached to the key in the Rigalio emblem picturizes the key an instrument of opportunity worthy to be used by an individual who is responsible. By virtue of the fact that the key is such a potent instrument Rigalio envisages the placement on the key on its emblem.  </p>
             </div> <!--/emblem-section -->
             <div class="emblem-section left-inner">
                <div class="emblem-img"><img src="<?php echo base_url(); ?>content/images/aboutus/spoon.png" class="img-responsive"> </div>
                <h5>The legacy and reverence of the spoon, </h5> <hr>
                <p>Spoon, the cradle of civilizational history proudly emphasizes the transition through the various eras of tradition, each depicting an associational relation to eminence and majesty. The ancient and great civilizations of the world whether it be the Greek, the Romans, the Egyptians or the Mughals have all left behind an inheritance so profound that its meanings resonates in the contemporary epoch. The historical empires have all contributed richly to the arts, sciences, and literature. They have also been at the helm of pioneering the most exquisite and lavish models of corporeal living that remains the benchmark for the present day elite. The golden civilizations and the citizens inhabiting the eras were known for their love of profligate elements and expected nothing short of the finest and the most stunning craftsmanship that service could offer. </p>
                <p>Rigalio exemplifies products and marques providing utmost quality in service, the finest sourced materials and unparalleled craftsmanship, it is expected that these brands would uphold traditional values and practices of etiquette. The nostalgia associated with history and tradition offered by Rigalio attempts to establish an emotional connect with heritage and custom.  </p>
             </div> <!--/emblem-section -->
             <div class="emblem-section right-inner">
                <div class="emblem-img"><img src="<?php echo base_url(); ?>content/images/aboutus/kheiron.png" class="img-responsive"> </div>
                <h5>The perspicacity and forte of Kheiron</h5> <hr>
                <p>Kheiron, he eldest and the most noble of all the Centaurs was an apostle of the wisdom of knowledge and a tutor to many a gods, including Achilles. In Greek mythology he has been revered as a god who was the noblest specimen of a combination of the human and the animal forms in the ancient works of art. Kheiron personifies the melange of the paramount qualities that are indispensable if one is to achieve success. It is only through an intricate blend of astuteness and exertion can one anticipate to reach the zenith. The immensity of emphasis placed on this virtues that delineate men can be gathered from the fact that Kherion sacrificed his immortality so that Prometheus may live. He was justly accorded his righteous place on Mount Olympus by Zeus and is reminisced as an idol.</p>
                <p>One of the key attributes of the Rigalio Insignia is the importance placed on the qualities that define humans. The path to success is checkered with decisions that are to be made at critical moments throughout the journey. The path which is accorded the utmost admiration is the one which involves honest labour and judicious intelligence. </p>
             </div> <!--/emblem-section -->
             <div class="emblem-section ending">
               <img src="<?php echo base_url(); ?>content/images/aboutus/ending.png" class="img-responsive">
             </div>
           </div><!--/inner-con -->
        </div>
      </div> <!--/privacy-pg-con -->
      

    </div><!--/row -->
  </div> <!--/container-fluid --> 
</div><!--privacy-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url();?>main/faq">Faq </a> </li>
       </ul>
    </div>
  </div>
</footer>
  </body>
<script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>content/js/bootstrap-hover-dropdown.js"></script>
<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>
<style>

</style>

  
</html>
