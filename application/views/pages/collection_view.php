<?php 

//print_r($collections);
?> <div class="collection-inner">
 <div class="container-fluid">
    <div class="row">
                    <div class="collection-tagline col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding"><a href="<?php echo base_url(); ?>collection"><button class="goback-btn pull-left browse-btn"> go back</button></a> </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><h3><?php echo $collections[0]['subCategory_Name'];?> </h3> </div>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">
                         <?php /*?><button class="edit-btn browse-btn">edit</button>
                         <a href="javascript:void(0)" class="lock"><button class="lock-btn browse-btn"><i class="fa fa-lock"></i><i class="fa fa-unlock-alt" style="display: none;"></i></button></a><?php */?>
                         <div class="privacy-status" style="display: none;"> <ul> <li><a href="javascript:void(0)"> Public </a> </li><li><a href="javascript:void(0)">Private</a> </li> </ul> </div>
                      </div>
                    </div> 
        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content">
       <div class="grid" id="masonry-grid">
	        <?php 
       //print_r($collections);
	   $sr = 0;
        foreach ($collections as $product) { 
        ?>
      <div class="wall-column grid-item">
          <div class="wall-item">
            <div class="category <?php echo $product['category_color']; ?>">
   
                                           <div class="category_img">
            <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive">
                    
                        <div class="hover-content-cat">
                        <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                         <div class="tab">
                           <div class="tab-in">
                         <div class="cat-zoom-icon" id="<?php echo $product['productId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                         <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                          <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"></div>
                         <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span> </div>
                          <h4><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","_",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>"><?php echo $product['brand_Name']; ?> </a></h4>
                         <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                         <button class="follow-brand-btn follow">Follow brand</button><!--<button class="follow-brand-btn follow-hover" style="display:none;">Follow brand</button> --> 
                         <button class="follow-brand-btn following" style="display:none;"><span><i class="fa fa-check"></i>
                         </span>Following</button>
                         <button class="follow-brand-btn unfollow"><span><i class="fa fa-times"> </i> </span>unfollow</button> </a>
  						</a>
						 </div>
                         </div> <!--/tab structure -->
                       </a> 
                      </div> <!--/hover-content-cat -->
                   </div></a> <!--/category_img--> 
                        <div class="category_content">
                     <div class="relative-struct">
                        <h1 class="hea"><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","_",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a>
                         <div class="tooltip cat-follow-popup">
                          <div class="tab">
                            <div class="tab-cell">
                              <div class="img-part">  <span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                             <div class="follow-con">
                                <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                <h1 class=""><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1> 
                               <div class="follow-status" id="<?php echo $product['categoryId']; ?>"> 
                                  <a href="javascript:void(0)"><button class="follow-cat">Follow category</button>
                                 <button class="following-cat" style="display:none !important;"><span><i class="fa fa-check"></i></span>following</button></a>
                                  <button class="unfollow-cat" style="display:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a>
                              </div>
                             </div>
                            </div>
                          </div>   
                             
                         </div>  </h1>
                     </div> <!--/relative-struct -->
                    
                    <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                   <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                   </div> <!--/category_content-->
                   
<div class="category_options">
                        <table>
                         <tr>
                           <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                  $your_date = strtotime($startDate);
                                 $datediff = $now - $your_date;
                                  echo floor(($datediff/(60*60*24))+1);?>days ago</p> </td>
<td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-crown"> </span></td>
<td class="comment-sectn <?php echo $product['category_color']; ?>"><span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?></span> <span class="icomoon icon-chat"></span> </td>
                           <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span></span><span class="icomoon icon-sharing"></span> </td>
                        </tr>
                        </table>
                        <div class="fwd-social-icons sec7" style="display:none;">
                           <ul class="cat-follow-icons">
                              <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="fb"></a>
                             </li>
                              <li>


                             <a href="https://twitter.com/home?status=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="twitter" ></a>
                              </li>
                              <?php /*?><li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */?>
                              <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                            </ul>
                            <div class="hidden-more-icons" style="display:none;">
                              <ul class="follow-icons-more">
                                <li>

<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="linkdin"></a>
                             </li>
                                <li>
<a href="https://plus.google.com/share?url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="gplus"></a>

                               </li>
                                <li>

<a href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&description=Testing" class="pini"></a>
                               </li>
                              </ul>
                            </div>
                       </div> <!--/inner forward icons social -->
                       <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                     <?php
              $regif = $this->session->userdata('registrationid');
            if(!$regif || $userdata[0]['profile_picture']=='') 
             {
             ?>
                                    <span><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                    <?php } else{ ?>
                                     <span><img src="<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>" class="img-responsive img-circle"> </span>
                                    <?php }  ?>
                                    <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                     </div> <!--/write-comment --> 
                   </div> <!--/category_options-->
                      
             </div> <!--/category-->
        </div> <!--/wall-item -->
      </div> <!--/wall-column -->
         <?php $sr++ ; 
     }
   $this->session->set_userdata('sr',$sr);
   //$ses_var = $this->session->userdata('sr);
    ?>
         </div> <!--/grid ends -->  
		</div>   <!--/whats_new_content --> 
    </div> <!--/row-->
  </div> <!--/container-fluid-->
           </div> <!--/collection-inner --> 
           