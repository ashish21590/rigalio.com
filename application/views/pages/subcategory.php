 <?php 
 //print_r($category); exit;
$subcategory_id=print_r($subcat);
//print_r($category); exit;
?>
 <!-- Carousel
    ================================================== -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding mg-top">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="main-slider-container subcategory-slider">  
    <div id="subcatCarousel" class="carousel main-slider slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#subcatCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#subcatCarousel" data-slide-to="1"></li>
        <li data-target="#subcatCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <?php 
            echo $slider_image[0]['slider_content'];
            ?>
        </div> <!--/slide1 -->
        <div class="item">
        <?php 
            echo $slider_image[1]['slider_content'];
            ?>
        </div> <!--/slide2 -->
        <div class="item">
           <?php 
            echo $slider_image[2]['slider_content'];
            ?>
        </div><!--/slide3 -->
      </div>

    </div><!--/carousel -->

  </div>  <!--/main-slider-container -->

</div>      
</div>
<!-- starts category page con  -->
<div class="category-pg-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="category-pg-inner col-lg-11 col-md-11 col-sm-12 col-xs-12">
  <div class="container-fluid">
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tagline-inner nopadding">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding breadcrumb">
             <span>You are here: </span>
             <ul> 
                <li><a href="<?php echo base_url(); ?>"> Home </a> </li>  <li><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($category[0]['category_Name']));?>/<?php echo $category[0]['categoryId'];?>"> <?php echo $category[0]['category_Name'];?> </a></li> <li><a href="<?php echo base_url();?>subcategory/<?php echo str_replace(" ","-",strtolower($category[0]['subCategory_Name']));?>/<?php echo $category[0]['subCategoryId'];?>"> <?php echo $category[0]['subCategory_Name'];?> </a></li>
             </ul>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category-pg-tag">
             <span class="category-iconic icomoon <?php echo $category[0]['mainsubcat_icon'];?> <?php echo $category[0]['category_color'];?>"> </span><h3> <?php  echo $category[0]['subCategory_Name'];?></h3>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category_pg">
            <a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($category[0]['category_Name']));?>/<?php echo $category[0]['categoryId'];?>"> <button class="sub-cat-btn <?php echo $category[0]['category_color']; ?>"> <span><i class="fa fa-angle-left"></i></span> Main Category</button></a>
             <div class="follow-status" id="<?php echo $category[0]['categoryId']; ?>"> 
                                  <a href="javascript:void(0)"><button class="follow-cat-btn normal follow-cat <?php echo $category[0]['category_color']; ?>">Follow category</button>
                                 <button class="follow-cat-btn following-cat" style="display:none !important;"><span><i class="fa fa-check"></i></span>following</button></a>
                                  <button class="follow-cat-btn unfollow-cat" style="display:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a>
                              </div>
             <!--<button class="follow-cat-btn unfollow"><span><i class="fa fa-times"> </i> </span>Unfollow</button> -->
          </div>
       </div>
       
    </div> <!--ends row -->
  </div> <!-- container-fluid ends-->
  </div> <!--category-pg-inner ends --> 
</div> <!--/ category page con  -->
<div class="main_content">
  <div class="container-fluid">
    <div class="row">
    
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
            <h2 ng-model="heading"><?php print_r($heading); ?></h2>
            <hr>
         </div> 
      </div> <!--/content tagline-->
      <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content" id="abc">
       <div class="grid" id="masonry-grid">
        <?php
		
if(!$products){
  echo "There is no product available in this category";
}
         $sr = 0;
         foreach ($products as $product) { 
                 ?>
  <div class="wall-column grid-item">
    <div class="wall-item">

         <div class="category <?php echo $product['category_color']; ?>">
                 <div class="category_img">
                      <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive">
                     <div class="hover-content-cat">
                     <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">

                          <div class="tab">
                          <div class="tab-in">
                         <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                               <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"> </div>
                               <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span> </div>
                          <h4><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","-",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>"><?php echo $product['brand_Name']; ?></a></h4>
                         <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>"><!--<button class="follow-btn follow">Follow brand</button>-->
                         <button class="follow-brand-btn follow">Follow brand</button>
                         <!--<button class="follow-btn follow-hover" style="display:none;">Follow brand</button></button>-->
                          <button class="follow-btn following" style="display:none;"><span><i class="fa fa-check"></i>
                         </span>Following</button>
                         <button class="follow-btn unfollow "><span><i class="fa fa-times"> </i> </span>unfollow</button> 
                         
                        </a>
						</div>  <!--/tab-in -->
                        </div> <!--/tab -->
                         </a>
                      </div> <!--/hover-content-cat -->
                   </div>
                    </a>
                   <div class="category_content">
                     <div class="relative-struct">
                       <h1 class="hea"><a href="<?php echo base_url(); ?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a> 
                       <div class="tooltip cat-follow-popup">
                          <div class="tab">
                            <div class="tab-cell">
                              <div class="img-part"> <span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                             <div class="follow-con">
                                <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                 <h1 class=""><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1> 
                                <div class="follow-status" id="<?php echo $product['categoryId']; ?>"> 
                                
                                  <a href="javascript:void(0)"><button class="follow-cat">Follow category</button><button class="following-cat" style="display:none !important;"><span><i class="fa fa-check"></i></span>following</button></a>
                                   <button class="unfollow-cat" style="display:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a>
                                  <!--<a href="javascript:void(0)"><button class="follow-hover" style="diplay:none;">Follow category</button></a>
                                  <a href="javascript:void(0)"><button class="following bg-green" style="diplay:none;"><span><i class="fa fa-check"></i></span>following</button></a>
                                  <a href="javascript:void(0)"><button class="unfollow border-green font-green" style="diplay:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a> -->
                                  
                                </div>
                             </div>
                            </div>
                          </div>                              
                       </div> 
                       </h1>
                     </div> <!--/relative-struct -->
                     
                    <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                     <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                   </div> <!--/category_content-->
                   
                    <?php 
                  
                  $count_people=[];
                  $comment_people = [];
                    foreach($this->getdata->crownuser_detail($product['productId']) as $count_no)
                    { 
                    $count_people[]=$count_no;  
                    }
                    foreach($this->getdata->commentuser_detail($product['productId']) as $countcomment_no)
                    { 
                    $comment_people[]=$countcomment_no;  
                    }
					 
                  $sum_total = count($count_people) + count($comment_people);
                 if ($sum_total != '0')
				{
                    ?> 
                     
                    <div class="hidden-status">
                  <h6><?php 
//print_r($count_people);
$m=[];
foreach($count_people as $me){

$m[]=$me['user_id'];
}

$m1=[];
foreach($comment_people as $me){

$m1[]=$me['user_id'];
}
// echo count(array_unique(array_merge($m,$m1)));
//print_r(array_merge($count_people,$comment_people));
//$array1 = array_merge ($count_people,$comment_people);
//$array2 = array_unique($array1);
//$array3 = array_keys($array2);
//print_r($array2);  
echo count(array_unique(array_merge($m,$m1)));;  ?> People Reacted On This </h6>  <span class="profile-angle"><i class="fa fa-angle-down"></i></span></div>
                  <?php  } ?>


                   <div class="category_options">
                        <table>
                         <tr>
                            <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now=time(); $startDate=$product['created_on'];
						   $yourDate= strtotime($startDate);
						   $datediff=$now-$yourDate;
						    echo floor(($datediff/(60*60*24))+1);?>days ago</p> </td>  
                           <td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-crown"> </span></td>
<td class="comment-sectn <?php echo $product['category_color']; ?>"><span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-chat"></span> </td>
                           <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span> </span> <span class="icomoon icon-sharing"> </span> </td>
                        </tr>
                        </table> 
                                        
                        <div class="fwd-social-icons sec7" style="display:none;">
                           <ul class="cat-follow-icons">
                              <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview" class="fb"></a>
                             </li>
                              <li>


                             <a href="https://twitter.com/home?status=<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview" class="twitter" ></a>
                              </li>
                              <?php /*<li><a class="insta" href="javascript:void(0)" target="_blank"></a></li> */ ?>
                              <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                            </ul>
                            <div class="hidden-more-icons" style="display:none;">
                              <ul class="follow-icons-more">
                                <li>

<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview" class="linkdin"></a>
                             </li>
                                <li>
<a href="https://plus.google.com/share?url=<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview" class="gplus"></a>

                               </li>
                                <li>

<a href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview&description=Testing" class="pini"></a>
                               </li>
                              </ul>
                            </div>
                       </div> <!--/inner forward icons social -->
                         <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                    <?php
					    $regif = $this->session->userdata('registrationid');
					if(!$regif || $userdata[0]['profile_picture']=='') 
					   {
					   ?>
                                    <span><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                    <?php } else{ ?>
                                     <span><img src="<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>" class="img-responsive img-circle"> </span>
                                    <?php }  ?>
                                    <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                     </div> <!--/write-comment -->    
                       
                   </div> <!--/category_options-->
                      
         </div> <!--/category-->
    </div> <!--/wall-item -->
  </div> <!--/wall-column -->
    <?php $sr++ ; 
     }
    $this->session->set_userdata('sr',$sr);
   //$ses_var = $this->session->userdata('sr);
    ?>
       </div>

       
  </div> <!--/Whats_new_content-->
 <!--/column-->

            <!-- mywork place ends here--> 
      <div id="subcat_count" style="display:none;"><?php echo $subcat_count[0]['no']; ?></div>
    <div class="container loading-sectn" style="text-align: center"><button class="btn" id="load_more" style="background:none" data-val = "1"><img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>loader.gif" class="img-responsive"> </button></div> 
   <div id="show" style="display:none" class="nomore">No more products to showcase</div>
   </div> <!--/Whats_new_content-->
    </div> <!--/row-->
  </div> <!--/container-fluid-->
</div> <!--/main_content-->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


  </body>


 
  <script type="text/javascript">
  var $container = jQuery('#masonry-grid');
  var $grid = $container.masonry({
     percentPosition: true,
    itemSelector: '.grid-item'
  });
 
$(document).ready(function(e){
 $('#masonry-grid').masonry({
  itemSelector: '.grid-item',
percentPosition: true
});
      var page = $("#load_more").data('val');
      var total_product_count="<?php echo $subcat_count[0]['no']; ?>";
      var subcategory_id="<?php print_r($subcat); ?>";
      var per_page=4;
            //alert(page);
            
            var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);

            if(page<=total_pages){
              //alert('onload');
             // getsubcat(page,subcategory_id);
            }
            
          });

 $(window).scroll(function(e){

  if ($(window).scrollTop() == $(document).height() - $(window).height()){
     
       // e.preventDefault();
          // $("#load_more").data('val');
          page = $("#load_more").data('val');
          var total_product_count="<?php echo $subcat_count[0]['no']; ?>";
          var subcategory_id="<?php print_r($subcat); ?>";
          var per_page=4;
           // alert(page);
           var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<=total_pages){
              getsubcat(page,subcategory_id);
            }
			else{
				$("#show").show();
				}
          }
        }); 
 
 var getsubcat= function(page,subcategory_id){
var data ={"page":page,
   "subcategory_id":subcategory_id};
   $("#loader").show();
   $('#load_more').data('val', ($('#load_more').data('val')+1));
   $.ajax({
    url:"<?php echo base_url() ?>myscroll/getsubcat",
    type:'GET',
    data: data
  }).done(function(response){
    response = response.trim();
    // $("#masonry-grid").append(response);          
    // $('#masonry-grid').masonry('reloadItems');
   var dom = $('<div/>').html(response).contents();
   $(dom).each(function(){
      if($(this).hasClass('wall-column')){
      $grid.append($(this));
      var el = this;
      el.getElementsByTagName('img')[0].onload = function(){console.log(el);$grid.masonry( 'appended', $(el) )  };
          }  

   })
    $("#loader").hide();
    });
  
};
 /*
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 6000);
    };
    */
    

    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>
  
