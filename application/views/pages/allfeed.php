<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Displaying A Custom Facebook Page Feed Using The Facebook PHP SDK</title>

    <!--<meta name="description" content="In this lab, we take a look at using the Facebook PHP SDK to connect to a Facebook app, retrieve the graph data from a Facebook page, and output it in a neat, nice looking format." />-->

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/base.css" />
    <link rel="stylesheet" href="css/style.css" />

</head>



<body>


<?php
/**
 * This is the link to my page graph
 * I've included it here so i can copy an paste for quick reference
 *
 * Copying and pasting this into the browser url bar gives you a full graph of the feed
 * which is very handy for browsing and seeing what exists in the array
 *
 * Change the values to suit your own needs, and when your script is final, remove this
 * comment block
 *
 * Typing this into the url will get you the super array (graph) to analyze
 * https://graph.facebook.com/YOUR_PAGE_ID/feed?access_token=APP_ACCESS_TOKEN
 */

// include the facebook sdk
require_once('resources/facebook-php-sdk-master/src/facebook.php');

// connect to app
$config = array();
$config['appId'] = '172781859754309';
$config['secret'] = '88707dc4c289bce742f8e9ad2f4bf7f8';
$config['fileUpload'] = false; // optional

// instantiate
$facebook = new Facebook($config);

// set page id
$pageid = $_GET['userid'];

// now we can access various parts of the graph, starting with the feed
$pagefeed = $facebook->api("/" . $pageid . "/feed?fields=full_picture,message,type,story,created_time");

//print_r($pagefeed);
?>


<div id="wrapper">


    <header>
        <div class="container clearfix">
            <div id="logo">
                <a href="http://rigalio.com/x/no_access/content/images/main-logo.png" /></a>
            </div>
            <div id="title">
                <h1>For Rigalio Facebook Feed </h1>
            </div>
        </div>
    </header>



    <div id="main">
        <div class="container">



            <h1>Facebook Feed</h1>



            <?php

            echo "<div class=\"fb-feed\">";

            // set counter to 0, because we only want to display 10 posts
            $i = 0;

            $fb=[];
            foreach($pagefeed['data'] as $post) {


                if ($post['type'] == 'status' || $post['type'] == 'link' || $post['type'] == 'photo') {


                    // open up an fb-update div
                    echo "<div class=\"fb-update\">";

                    // post the time


                    // check if post type is a status
                    if ($post['type'] == 'status') {
                        echo "<h2>Status updated: " . date("jS M, Y", (strtotime($post['created_time']))) . "</h2>";
                        if (empty($post['story']) === false) {
                            echo "<p>" . $post['story'] . "</p>";

                        } elseif (empty($post['message']) === false) {
                            echo "<p>" . $post['message'] . "</p>";
                        }
                    }

                    // check if post type is a link
                    if ($post['type'] == 'link') {
                        echo "<h2>Link posted on: " . date("jS M, Y", (strtotime($post['created_time']))) . "</h2>";
                        echo "<p>" . $post['name'] . "</p>";
                        echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">" . $post['link'] . "</a></p>";
                    }

                    // check if post type is a photo
                    if ($post['type'] == 'photo') {
                        echo "<h2>Photo posted on: " . date("jS M, Y", (strtotime($post['created_time']))) . "</h2>";
                        if (empty($post['story']) === false) {
                            echo "<p>" . $post['story'] . "</p>";
                        } elseif (empty($post['message']) === false) {
                            echo "<p>" . $post['message'] . "</p>";

                            $fb[$i]['message']=$post['message'];

                        }

                        echo "<image src=\"" . $post['full_picture'] . "\"/>";
                        $fb[$i]['full_picture']=$post['full_picture'];
                        $fb[$i]['source']="fb";
                        echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">View photo &rarr;</a></p>";
                    }


                    echo "</div>"; // close fb-update div

                    $i++; // add 1 to the counter if our condition for $post['type'] is met
                }


                //  break out of the loop if counter has reached 10
                if ($i == 10) {
                    break;
                }
            } // end the foreach statement

            echo "</div>";

            ?>


        </div>
    </div>


    <footer>
        <div class="container">
            <a href="http://www.callmenick.com/labs/styling-blockquotes-with-css-pseudo-classes">&larr; Back to the article</a>
        </div>
    </footer>



</div>


</body>
</html>
<?php
ob_flush();
?>

<?php
require 'tmhOAuth.php'; // Get it from: https://github.com/themattharris/tmhOAuth
// Use the data from http://dev.twitter.com/apps to fill out this info
// notice the slight name difference in the last two items)
$connection = new tmhOAuth(array(
    /*
        $oauth_access_token = "2250269989-fn0N4WKKUwun7zRG47eLHqTDAY9qw2jmCgbwgE2";
    $oauth_access_token_secret = "Vewq3cQfIwlLl5cBl1QjLO5wxKAAx877Yr6tkj3gTUrSj";
    $consumer_key = "S0RhoqbDbf4AnKkkkJK9XGCTH";
    $consumer_secret = "9yMYEMbehVzOhvj0iQrsxGWSy3ToUl67tiYajLnVBLZF4HfosJ";
    */

    'consumer_key' => 'S0RhoqbDbf4AnKkkkJK9XGCTH',
    'consumer_secret' => '9yMYEMbehVzOhvj0iQrsxGWSy3ToUl67tiYajLnVBLZF4HfosJ',
    'user_token' => '2250269989-fn0N4WKKUwun7zRG47eLHqTDAY9qw2jmCgbwgE2', //access token
    'user_secret' => 'Vewq3cQfIwlLl5cBl1QjLO5wxKAAx877Yr6tkj3gTUrSj' //access token secret
));
// set up parameters to pass
$parameters = array();
if ($_GET['count']) {
    $parameters['count'] = strip_tags($_GET['count']);
}
if ($_GET['userid']) {
    $parameters['screen_name'] = strip_tags($_GET['userid']);
}
$parameters['exclude_replies']="true";
if ($_GET['twitter_path']) { $twitter_path = $_GET['twitter_path']; }  else {
    $twitter_path = '1.1/statuses/user_timeline.json';
}
$http_code = $connection->request('GET', $connection->url($twitter_path), $parameters );
if ($http_code === 200) { // if everything's good
    $response = strip_tags($connection->response['response']);
    if ($_GET['callback']) { // if we ask for a jsonp callback function
        echo $_GET['callback'],'(', $response,');';
    } else {
        $response;
    }
} else {
    echo "Error ID: ",$http_code, "<br>\n";
    echo "Error: ",$connection->response['error'], "<br>\n";
}
// You may have to download and copy http://curl.haxx.se/ca/cacert.pem
$myarr=json_decode($response);
//print_r(get_object_vars($myarr)); exit;
$tw=[];
$t=0;
foreach ($myarr as $myval) {

    //print_r($myval); exit;

    $tw[$t]['message']=$myval->text;
    $tw[$t]['full_picture']=$myval->entities->media[0]->media_url_https;
    $tw[$t]['source']="twitter";
    echo $myval->text."<br>";

//print_r($myval->entities->media[0]-> media_url_https);
    echo "<img src=".$myval->entities->media[0]->media_url_https.">"."<br>";
    $t++;
}
?>


<?php
//returns a big old hunk of JSON from a non-private IG account page.
function scrape_insta($username) {
    $insta_source = file_get_contents("http://instagram.com/".$username);
//print_r(get_object_vars($insta_source));
    $shards = explode('window._sharedData = ', $insta_source);
    $insta_json = explode(';</script>', $shards[1]);
    $insta_array = json_decode($insta_json[0], TRUE);
    return $insta_array;
}
//Supply a username
$my_account = 'bmw';
//Do the deed
$results_array = scrape_insta($_GET['userid']);


$cnt=0;

$ins=[];
foreach($results_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] as $final)
{
    echo $cnt."ashish";
    //print_r($final);

    $ins[$cnt]['message']=$final['caption'];
    $ins[$cnt]['full_picture']=$final['display_src'];
    $ins[$cnt]['source']="instagram";
    echo $final['caption']."<br>";
    echo "<img src=".$final['display_src']."/>";


    //echo 'Latest Photo:<br/>';
    //echo '<a href=""><img src="'.$final['entry_data']['ProfilePage'][0]['user']['media']['nodes'][$cnt]['display_src'].'"></a></br>';
    //echo 'Likes: '.' - Comments: '.$final['entry_data']['ProfilePage'][0]['user']['media']['nodes'][$cnt]['comments']['count'].'<br/>';
    $cnt++;
}

//echo json_encode($results_array);
//An example of where to go from there
$latest_array = $results_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'][0];
//echo 'Latest Photo:<br/>';
//echo '<a href="http://instagram.com/p/'.$latest_array['code'].'"><img src="'.$latest_array['display_src'].'"></a></br>';
//echo 'Likes: '.$latest_array['likes']['count'].' - Comments: '.$latest_array['comments']['count'].'<br/>';
/* BAH! An Instagram site redesign in June 2015 broke quick retrieval of captions, locations and some other stuff.
//echo 'Taken at '.$latest_array['location']['name'].'<br/>';
//Heck, lets compare it to a useful API, just for kicks.
//echo '<img src="http://maps.googleapis.com/maps/api/staticmap?markers=color:red%7Clabel:X%7C'.$latest_array['location']['latitude'].','.$latest_array['location']['longitude'].'&zoom=13&size=300x150&sensor=false">';
*/

//print_r($results_array );
//echo json_encode($results_array);




//print_r(array_merge($fb,$ins,$tw));

print_r($tw);
ob_start();
?>


