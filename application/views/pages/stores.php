
<nav class="navbar navbar-inverse navbar-fixed-top" id="header_nav">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-collapse-btn">
        <a href="javascript:void(0)" class="popup-btn"> <img src="content/images/icons/header-collapse-icon.png" class="img-responsive"> </a>
    </div>
    <div class="main-logo col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a href="javascript:void(0)"> <img src="content/images/main-logo.png" class="img-responsive"> </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 drop-search-wrap navbar-right">
        <a href="javascript:void(0)" class="search-icon"> <img src="content/images/icons/search-icon.png" class="img-responsive"></a>
    </div>
    <div class="search-hidden col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-search " style="display:none;">
        <form id="searchform" role="search" method="get" action="http://themes.birdwp.com/zefir-new/">
            <input type="text" name="s" id="s" class="search-field form-control" placeholder="I am Looking for">
        </form>
    </div>
</nav>
<div class="popup" id="popup">
    <div class="popup-inner">
        <a href="javascript:void(0)" class="close">CLOSE</a>
        <div class="main-menu">
            <ul class="main">
                <li>
                    <div class="">
                        <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Auto</a>
                        <ul class="dropdown-menu submenu">
                            <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="">
                        <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Fashion and Lifestyle</a>
                        <ul class="dropdown-menu submenu">
                            <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="">
                        <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Real Estate </a>
                        <ul class="dropdown-menu submenu">
                            <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="">
                        <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Tarvel and Hotels</a>
                        <ul class="dropdown-menu submenu">
                            <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="">
                        <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Events</a>
                        <ul class="dropdown-menu submenu">
                            <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                            <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                        </ul>
                    </div>
                </li>
            </ul>

            <div class="main-social-sectn">
                <ul>
                    <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                    <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                    <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                    <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                </ul>
            </div> <!--/main-social-sectn" -->
        </div> <!-- /main-menu -->
    </div><!-- popup -inner-->
</div>  <!--/header popup -->



<div class="brand_pg_tagline">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 content_headline">
            <div class="brand-info">
                <div class="brand-img col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <img src="content/images/brands/louis-logo.png" class="img-responsive">
                </div> <!--/ brand-img -->
            </div> <!--brand-info -->
            <div class="product-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><label>Brand <span class="brand-name">Louis vuitton</span> </label> </li>
                    <li><label>Established<span>1864 </span> </label> </li>
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 brand-follow">
                    <button class="follow-cat-btn normal">follow brand</button>
                </div>
            </div> <!--/product-info -->
        </div>

    </div>
</div> <!--/brand_pg_tagline -->
<div class="basic-cat-info brand-pg bg-white col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="basic-cat-info-inner col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 follow-list">
            <table>
                <tr>
                    <td> <span>5 </span> <span><img src="content/images/icons/share-icon-grey.png"></span></td>
                    <td> <span>5 </span> <span><img src="content/images/icons/followers-icon.png"> </span></td>
                </tr>
            </table>
        </div>

    </div>  <!--/basic-cat-info-inner -->
</div>  <!--/basic-cat-info -->
<!--category-pg-con -->
<div class="category-pg-con brand-pg-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="brand-tab-panel col-lg-10 col-md-10 col-sm-11 col-xs-12">
                <ul  class="nav nav-pills">
                    <li class="active"><a href="#brand-legacy" data-toggle="tab">Legacy</a></li>
                    <li><a  href="#brand-product" data-toggle="tab">Products</a></li>
                    <li><a href="#brand-upcoming" data-toggle="tab">Upcoming</a></li>
                    <li><a href="#social-feed" data-toggle="tab">Social Feed</a></li>
                    <li><a href="#latest-news" data-toggle="tab">Latest News</a></li>
                    <li><a href="#store-loc" data-toggle="tab">Store Locator</a></li>
                </ul>
            </div> <!--/category-tab-panel -->

            <div id="myTabContent1" class="tab-content">
                <div class="legacy-con tab-pane fade active in" id="brand-legacy">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                        <h2>Legacy</h2>
                        <hr>
                    </div>
                </div> <!-- brand legacy tab content ends -->

                <div class="upcoming-con tab-pane fade" id="brand-upcoming">
                    this is upcoming part
                </div> <!-- brand products tab content ends -->

                <div class="latest-news-con tab-pane fade" id="latest-news">
                    this is latest news part
                </div><!--/latest-news-con tab content ends -->
                <div class="store-loc-con tab-pane fade" id="store-loc">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                        <h2>store locator</h2>
                        <hr>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 nopadding store-loc-inner">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 nopadding storelist">
                            <h3>Locate a store </h3> <div> <hr> </div><address class="line1">louis vuitton stores</address>
                            <div class="find-store col-lg-12 nopadding">
                                <input type="text" value="" placeholder="Enter City" name="" class="store-box"> <button class="store-btn"><span><i class="fa fa-search" aria-hidden="true"></i></span></button>
                            </div><hr>
                            <div class="store1">
                                <address class="line2">louis vuitton mumbai 1 taj palace & tower </address>
                                <address class="line3"> the taj palace & tower apollo bunder <br> <span class="code">4000 22000 mumbai india </span></address>
                                <span class="tel-no">+91 987654321 </span>
                            </div> <!-- /store1-->
                            <div class="store1">
                                <address class="line2">louis vuitton mumbai 1 taj palace & tower </address>
                                <address class="line3"> the taj palace & tower apollo bunder <br> <span class="code">4000 22000 mumbai india </span></address>
                                <span class="tel-no">+91 987654321 </span>
                            </div> <!-- /store1-->
                            <div class="store1">
                                <address class="line2">louis vuitton mumbai 1 taj palace & tower </address>
                                <address class="line3"> the taj palace & tower apollo bunder <br> <span class="code">4000 22000 mumbai india </span></address>
                                <span class="tel-no">+91 987654321 </span>
                            </div> <!-- /store1-->
                            <div class="store1">
                                <address class="line2">louis vuitton mumbai 1 taj palace & tower </address>
                                <address class="line3"> the taj palace & tower apollo bunder <br> <span class="code">4000 22000 mumbai india </span></address>
                                <span class="tel-no">+91 987654321 </span>
                            </div> <!-- /store1-->

                        </div> <!--/storelist -->
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 nopadding storemap">
                            <div id="map"></div>
                        </div> <!--/storemap -->

                    </div> <!--/store-loc-inner -->
                </div><!--/store-loc-con tab content ends -->
                <div class="social-feed-con tab-pane fade" id="social-feed">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                        <h2>social feed</h2>
                        <hr>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content">
                        <div class="grid" id="masonry-grid">
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-facebook" aria-hidden="true"></i></span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>
                                                    <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">

                                                <div class="few-comm">
                                                    <table>
                                                        <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                        <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span> </td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                        <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                                    </table>
                                                </div>
                                                <div class="see-all-com"> see all  </div>
                                                <span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>

                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/fashionbig.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-twitter" aria-hidden="true"></i> </span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">

                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-instagram" aria-hidden="true"></i></span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr class="">
                                                    <td class="date-status cat_travel"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/travel.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/fashion.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr class="">
                                                    <td class="date-status cat_travel"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn cat_travel"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <div class="category_img">
                                            <img src="<?php echo base_url(); ?>content/images/category/lifestyle.jpg" class="img-responsive">

                                        </div> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-google-plus"></i> </span></h3> </div>
                                            <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>
                                        </div> <!--/category_content-->
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status"><p>5 days ago </p>  </td>
                                                    <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>

                                                    <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                                                    <li><a href="javascript:void(0)" class="twitter"></a></li>
                                                    <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                                        <li><a class="pini" href="javascript:void(0)"> </a> </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">
                                                <span><img class="img-responsive img-circle" src="<?php echo base_url(); ?>content/images/timeline/profile-pic-small.png"> </span>
                                                <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>
                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                        </div>
                    </div>
                    <!--/whats_new_content -->
                </div><!--/social-feed-con tab content ends -->


            </div> <!--/tab-content -->
            <div class="product-share-sectn brand-pg col-lg-8 col-md-8 col-sm-11 col-xs-12">
                <h5>Share This </h5>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div class="product-inner-social col-lg-4 col-md-5 col-sm-12 col-xs-12 nopadding">
                        <ul class="follow-icons">
                            <li><a class="facebook-follow" href="" target="_blank"></a></li>
                            <li><a class="twitter-follow" href= target="_blank"></a></li>
                            <li><a class="instagram-follow" href="" target="_blank"></a></li>
                            <li><a class="linkedin-follow" href="" target="_blank"></a></li>
                            <li><a class="rss-follow" href="#"></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 nopadding btn-sectn"><a href=""><button class="goto-web-btn">Go to official website</button> </a><button class="query-btn" id="btn">Send a Query</button> </div>
                </div>
            </div> <!-- /product-share-sectn -->
            <div class="sendquery-form" style="display: none;">
                <form class="col-lg-6 col-md-7 col-sm-10 col-xs-12" >
                    <label>Send a query</label>
                    <input type="text" name="" value="" placeholder="NAME" class="textbox textbox2">
                    <input type="text" name="" value="" placeholder="CONTACT NO." class="textbox textbox2">
                    <input type="text" name="" value="" placeholder="EMAIL ID" class="textbox">
                    <input type="text" name="" value="" placeholder="CHOOSE A SUITABLE DATE" class="textbox textbox3">
                    <input type="text" name="" value="" placeholder="AND TIME" class="textbox textbox4">
                    <input type="text" name="" value="" placeholder="DEFAULT" class="textbox textbox5">
                    <textarea class="textbox"></textarea>
                    <button class="try-now-submit">SUBMIT</button>
                </form>
            </div> <!--/try-now-con -->


        </div>
    </div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                <li><a href="#">Rigalio Team </a> </li>
                <li><a href="#">Contact & Legal Notice </a> </li>
                <li><a href="#">Privacy </a> </li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


</body>
<script>
    function initMap() {
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv, {
            center: {lat: 44.540, lng: -78.546},
            zoom: 8
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>

</html>
