<!--contact us pg con -->
<div class="contact-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="contact-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h2>Contact us </h2> <hr>
             <h4>Welcome to the Customer Connect Service for Rigalio </h4>
             <hr class="grey">
           </div>
        </div>
      </div> <!--/contact-pg-con -->
      <div class="contact-pg-con bg-white col-lg-12 col-md-12 col-sm-12 col-xs-12  nopadding">
        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-11 assistance-con">
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="assistance-img"> <img src="<?php echo base_url(); ?>content/images/contact/contactus_faq.png" class="img-responsive"> </div>
                <h3> FAQ's</h3>
                <p>For instant answers to frequent enquiries about Rigalio.com and your online luxury experience, please browse our <b> FAQ's</b> section. </p>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="assistance-img"> <img src="<?php echo base_url(); ?>content/images/contact/contactus_service.png" class="img-responsive"> </div>
                <h3> SERVICE ASSISTANCE</h3>
                <p>Please note that we are only able to provide assistance for issues related to the online product display and are not able to answer questions concerning retailers or any other service offered by Rigalio. </p>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="assistance-img"> <img src="<?php echo base_url(); ?>content/images/contact/contactus_product.png" class="img-responsive"> </div>
                <h3>PRODUCT ASSISTANCE</h3>
                <P>For assistance related to watches please contact the nearest Service Centre.  </P>
             </div>
           
        </div>
      </div> <!--/contact-pg-con -->
      <div class="contact-form col-lg-12 col-md-12 col-sm-12 col-xs-12  nopadding">
         <div class="contact-us-con col-lg-10 col-md-10 col-sm-11 col-xs-11">
           <form class="col-lg-6 col-md-7 col-sm-10 col-xs-12" name="contactform" method="post">
             <h3>Drop us a mail</h3> <hr class="grey">
                <p id="msgctact"> </p>
                 <div id="myhide2" class="myhide2">
             <input type="text" name="name" id="namectact" value="" placeholder="NAME" class="textbox textbox2">
             <input type="text" name="contactno" id="contactnoctact" value="" placeholder="CONTACT NO." class="textbox textbox2">
             <input type="text" name="emailid" id="emailidctact" value="" placeholder="EMAIL ID" class="textbox">
             <textarea class="textbox" id="queryctact" name="query" placeholder="QUERY" rows="5"> </textarea>
             <input type="text" name="formname" id="formnamectact" value="Contact Us" style="display:none;">
             <button name="submit" class="contact-submit" type="button">SUBMIT</button>
              </div>
           </form>
         </div>
      </div> <!--/contact-form -->
      

    </div>
  </div>  
</div><!--privacy-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url();?>main/faq">Faq </a> </li>
       </ul>
    </div>
  </div>
</footer>
<script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>content/js/bootstrap-hover-dropdown.js"></script>
<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript">
  
function contactvalidation() {
	
	   var name =$("#name").val();
	   var contactno = $("#contactno").val();
	    var emailid =$("#emailid").val();
	  // var date = $("#date").val();
	   //var time = $("#time").val();
	   var query = $("#query").val();
	   var formname = $("#formname").val();
	    var pattern = /^\d{10}$/;
	  
	 // alert(name+emailid+contactno+date+time+query);
	   
 if(document.contactform.name.value == '' || document.contactform.name.value == 'NAME')

{
document.getElementById('msg1').innerHTML="Please Enter Name";
document.contactform.name.focus();
return false;
}

if(document.contactform.contactno.value == '' || document.contactform.contactno.value == 'CONTACT NO.')
{
document.getElementById('msg1').innerHTML="Please Enter Your Mobile No.";
document.contactform.contactno.focus();
return false;
}
else if(isNaN(document.contactform.contactno.value))
    {
                
    document.getElementById('msg').innerHTML="Please Enter Only Numeric Number";
    document.contactform.contactno.focus();
    return false;
    }
 else if (pattern.test(contactno)==false) 
                {
                document.getElementById('msg').innerHTML="Enter 10 digit Phone No.";
                return false;
                }

if(document.contactform.emailid.value == '' || document.contactform.emailid.value == 'EMAIL ID')

{
document.getElementById('msg1').innerHTML="Please Enter Email";
document.contactform.emailid.focus();
return false;
}else{
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var address = document.myform.emailid.value;
            if(reg.test(address) == false) {
 document.getElementById('msg1').innerHTML="Invalid Email Address";
 return false;
            }
        }

/*if(isNaN(document.myform.contactno.value))
    {
                
    document.getElementById('msg').innerHTML="Please Enter Only Numeric Number";
    document.myform.contactno.focus();
    return false;
    }
    else if(document.myform.contactno.value=='' || document.myform.contactno.value == 'CONTACT NO.' || isNaN(document.myform.contactno.value))
    {
    document.getElementById('msg').innerHTML="Enter Phone No.";
    return false;
    }
    else if (pattern.test(contactno)==false) 
                {
                document.getElementById('msg').innerHTML="Enter 10 digit Phone No.";
                return false;
                }
				*/

if(emailid || name || contactno ){
   document.getElementById('msg2').innerHTML= "Rigalio will get back to you with a feedback at the earliest !";
   document.getElementById('myhide').style.display="none";
   
  //  var data = "&name=" + name + "&emailid=" + emailid+ "&contactno=" + contactno + "&date=" + date + "&time=" + time + "&query=" + query ;
   
  // console(data);
    $.ajax({
     type: "POST",
      url: "http://rigalio.com/demo/main/contactussubmit?name=" + name + "&emailid=" + emailid+ "&contactno=" + contactno + "&query=" + query + "&formname=" + formname,
  // data:data,
   crossDomain:true,
     success: function(html){
      //alert(html);
   }
});   

return false;       
  }else{
    document.getElementById('msg1').innerHTML="Please Enter All fields";
    return false;
    }
    }

</script>

  </body>


</html>
