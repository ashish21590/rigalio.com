<?php
ob_start();
?>


<?php
/**
 * This is the link to my page graph
 * I've included it here so i can copy an paste for quick reference
 *
 * Copying and pasting this into the browser url bar gives you a full graph of the feed
 * which is very handy for browsing and seeing what exists in the array
 *
 * Change the values to suit your own needs, and when your script is final, remove this
 * comment block
 *
 * Typing this into the url will get you the super array (graph) to analyze
 * https://graph.facebook.com/YOUR_PAGE_ID/feed?access_token=APP_ACCESS_TOKEN
 */

// include the facebook sdk
require_once('resources/facebook-php-sdk-master/src/facebook.php');
// connect to app
$config = array();
$config['appId'] = '172781859754309';
$config['secret'] = '88707dc4c289bce742f8e9ad2f4bf7f8';
$config['fileUpload'] = false; // optional
// instantiate
$facebook = new Facebook($config);
// set page id
$pageid = $_GET['fb_userid'];
//echo $_GET['count'];
//echo "/" . $pageid . "/feed?//fields=full_picture,message,type,story,created_time&limit=".$_GET['count']."&https://graph.facebook.com/v2.6/22893372//268/feed?fields=full_picture,message,type,story,created_time&limit='".$_GET['count']."'";
// now we can access various parts of the graph, starting with the feed
$pagefeed = $facebook->api("/" . $pageid . "/feed?fields=full_picture,message,type,story,created_time,link&limit=".$_GET['count']."");
//print_r($pagefeed);



// set counter to 0, because we only want to display 10 posts
$i = 0;

$fb=[];
foreach($pagefeed['data'] as $post) {

    $fb[$i]['full_picture'] = $post['full_picture'];
    $fb[$i]['message'] = $post['message'];
    $fb[$i]['link']=$post['link'];
    $fb[$i]['source'] = "fb";
    $i++;
}



?>

<?php
require 'tmhOAuth.php'; // Get it from: https://github.com/themattharris/tmhOAuth
// Use the data from http://dev.twitter.com/apps to fill out this info
// notice the slight name difference in the last two items)
$connection = new tmhOAuth(array(
    /*
        $oauth_access_token = "2250269989-fn0N4WKKUwun7zRG47eLHqTDAY9qw2jmCgbwgE2";
    $oauth_access_token_secret = "Vewq3cQfIwlLl5cBl1QjLO5wxKAAx877Yr6tkj3gTUrSj";
    $consumer_key = "S0RhoqbDbf4AnKkkkJK9XGCTH";
    $consumer_secret = "9yMYEMbehVzOhvj0iQrsxGWSy3ToUl67tiYajLnVBLZF4HfosJ";
    */

    'consumer_key' => 'S0RhoqbDbf4AnKkkkJK9XGCTH',
    'consumer_secret' => '9yMYEMbehVzOhvj0iQrsxGWSy3ToUl67tiYajLnVBLZF4HfosJ',
    'user_token' => '2250269989-fn0N4WKKUwun7zRG47eLHqTDAY9qw2jmCgbwgE2', //access token
    'user_secret' => 'Vewq3cQfIwlLl5cBl1QjLO5wxKAAx877Yr6tkj3gTUrSj' //access token secret
));
// set up parameters to pass
$parameters = array();

$parameters['count'] = $_GET['count']+5;

if ($_GET['twitter_userid']) {
    $parameters['screen_name'] = strip_tags($_GET['twitter_userid']);
}
$parameters['exclude_replies']="true";
if ($_GET['twitter_path']) { $twitter_path = $_GET['twitter_path']; }  else {
    $twitter_path = '1.1/statuses/user_timeline.json';
}
$http_code = $connection->request('GET', $connection->url($twitter_path), $parameters );
if ($http_code === 200) { // if everything's good
    $response = strip_tags($connection->response['response']);
    /* if ($_GET['callback']) { // if we ask for a jsonp callback function
         echo $_GET['callback'],'(', $response,');';
     } else {
    */
    $response;
}
// You may have to download and copy http://curl.haxx.se/ca/cacert.pem
$myarr=json_decode($response);
//print_r(get_object_vars($myarr)); exit;
$tw=[];
$t=0;
foreach ($myarr as $myval) {

    //print_r($myval); exit;

    $tw[$t]['message']=$myval->text;
    $tw[$t]['full_picture']=$myval->entities->media[0]->media_url_https;
    $tw[$t]['source']="twitter";
    // echo $myval->text."<br>";

//print_r($myval->entities->media[0]-> media_url_https);
    //echo "<img src=".$myval->entities->media[0]->media_url_https.">"."<br>";
    $t++;
}

$myall=array_merge($fb,$tw);

foreach ($myall as $value) {
    ?>

    <div class="wall-column grid-item">
        <div class="wall-item">
            <div class="category">
                <div class="category_img">
                   <a href="<?php echo $value['link']; ?>"> <img src="<?php echo $value['full_picture'];?>" class="img-responsive">
                    </a>

                </div> <!--/category_img-->
                <div class="category_content">
                    <?php if($value['source']=="fb"){ ?>
                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-facebook" aria-hidden="true"></i></span></h3> </div>
                    <?php } else { ?>
                        <div class="feed-brand-name"> <h3>LVHM  <span class="social-fol"><i class="fa fa-twitter" aria-hidden="true"></i> </span></h3> </div>
                    <?php } ?>
                    <h2><a href="<?php echo $value['link']; ?>"><?php echo limit_text($value['message'], 10); ?></a> </h2>
                </div> <!--/category_content-->
                <div class="category_options">
                    <table>
                        <tbody>
                        <tr>
                            <td class="date-status"><p>5 days ago </p>  </td>
                            <td class="crown-sectn "><span class="text">5 </span><span class="icomoon icon-crown"> </span> </td>
                            <td class="fwd-icon-sectn"><span class="text">5 </span><span class="icomoon icon-sharing"></span> </td>
                        </tr>
                        </tbody>
                    </table>
                    <div style="display:none;" class="fwd-social-icons sec7">
                        <ul class="cat-follow-icons">
                            <li><a target="_blank" href="javascript:void(0)" class="fb"></a></li>
                            <li><a href="javascript:void(0)" class="twitter"></a></li>
                            <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li>
                            <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                        </ul>
                        <div style="display:none;" class="hidden-more-icons">
                            <ul class="follow-icons-more">
                                <li><a class="linkdin" href="javascript:void(0)"> </a> </li>
                                <li><a class="gplus" href="javascript:void(0)"> </a> </li>
                                <li><a class="pini" href="javascript:void(0)"> </a> </li>
                            </ul>
                        </div>
                    </div> <!--/inner forward icons social -->
                    <div style="display: none;" class="write-comment">

                        <div class="few-comm">
                            <table>
                                <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span> </td> <td class="post-com5">Is this better than Audi </td> </tr>
                                <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                            </table>
                        </div>
                        <div class="see-all-com"> see all  </div>
                        <span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                        <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>

                    </div> <!--/write-comment -->
                </div><!--/category_options-->
            </div> <!--/category-->
        </div> <!--/wall-item -->
    </div>

<?php }


function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

?>