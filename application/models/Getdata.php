<?php
class Getdata extends CI_Model {

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct()
    {
        $this->load->database();
    }

    /**
     * Get product by his is
     * @param int $product_id
     * @return array
     */
    public function getall()
    {

        $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` where `product`.`isActive`=1 limit 0 ,8";



        /*
               $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `brand`.`brandId`, `product`.`created_on`,`brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`categoryId`
        FROM `product`
        JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
        JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
        JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`

          limit $offset ,$limit";

        */
        $result = $this->db->query($sql)->result_array();

        return $result;
    }

    public function record_count() {
        return $this->db->count_all("product");
    }

    public function availcat()
    {

        $this->db->select('product.`productId`,product.`product_Name`, product.`productSmallDiscription`,
		category.category_color,product.`product_image`, category.`category_Name`, category.`categoryId`');
        $this->db->from('product');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->group_by('category.category_Name');
        $this->db->order_by('category.categoryId');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getcategory(){

        $this->db->select('*');
        $this->db->from('category');
        $query = $this->db->get();
        return $query->result_array();
    }

    function inviteduser_insert(){

        $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
        );
        return $this->db->insert('userinvit_login', $data);
        $insertinviteuser_id = $this->db->insert_id();
        return  $insertinviteuser_id;
    }
    function userbrand($userid)
    {
        $this->db->select('userluxry_intrest.`userid`,userluxry_intrest.`brandid`,brand.`brand_Name`,brand.`brand_image`,brand.`Establish`,brand.`brandDescription`');
        $this->db->from('userluxry_intrest');
        $this->db->join('brand','brand.brandId = userluxry_intrest.brandid');
        $this->db->where('userid',$userid);
        $query = $this->db->get();
        return $query->result_array();
    }

    //getting
    function category_wiseproduct($catid){

        //echo $catid;
        /*
                  $this->db->select('product.`productId`,product.`created_on`,product.`product_Name`, product.`productSmallDiscription`, category.category_color,category.`category_icon`,product.`product_image`, brand.brandId, brand.`brand_Name`,
                  sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,
                  category.`categoryId`');
                  $this->db->from('product');
                  $this->db->join('brand','brand.brandId = product.brandId_Fk');
                  $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
                  $this->db->join('category','category.categoryId = product.categoryId_Fk');
                  $this->db->where('category.categoryid',$catid);
        $this->db->limit(0,4);
                  $query = $this->db->get();
                  //print_r($query);
                  return $query->result_array();
        */


//echo $category_id;

        $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `category`.`categoryId` = $catid and `product`.`isActive`=1 limit 0,4 ";
//echo $sql;

        $result = $this->db->query($sql)->result_array();
        //print_r($result);
        return $result;


    }
    function category_product_count($catid){
        $this->db->select('count(product.productId) as no');
        $this->db->from('product');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->where('category.categoryid',$catid);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    function subcategory_product_count($subcategory_id){

        //echo $catid;
        $this->db->select('count(product.productId) as no');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->where('sub_category.subCategoryId',$subcategory_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();

    }
    function brand_product_count($brand_id){


        $this->db->select('count(product.productId) as no');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');

        $this->db->where('product.brandId_Fk',$brand_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();

    }
    function brand_count($searchdata){


        $this->db->select('count(brand.brandId) as no');
        $this->db->from('brand');
        $this->db->where("brand.`brand_Name` LIKE '%$searchdata%'");
        $query = $this->db->get();
        return $query->result_array();
    }
    function prod_count($searchdata){

        $this->db->select('count(product.productId) as no');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->where("product.product_Name LIKE '%$searchdata%'");
        $query = $this->db->get();
        return $query->result_array();
    }
    function people_count($searchdata){

        $this->db->select('count(user_registration.registrationid) as no');
        $this->db->from('user_registration');
        $this->db->like('firstname', $searchdata);
        $this->db->or_like('lastname', $searchdata);

        $query = $this->db->get();
        return $query->result_array();
    }

    function subcategory_wiseproduct($subcategory_id){

        //echo $catid;
        $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`
WHERE `sub_category`.`subCategoryId` = $subcategory_id limit 0 ,4";
        $result = $this->db->query($sql)->result_array();

        return $result;

        /* $this->db->select('product.`productId`,product.`created_on`,product.`product_Name`, product.`productSmallDiscription`, category.category_color,category.category_icon,product.`product_image`, brand.brandId, brand.`brand_Name`, sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`, category.`categoryId`');
         $this->db->from('product');
         $this->db->join('brand','brand.brandId = product.brandId_Fk');
         $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
         $this->db->join('category','category.categoryId = product.categoryId_Fk');
         $this->db->where('sub_category.subCategoryId',$subcategory_id);
         $query = $this->db->get();
         //print_r($query);
         return $query->result_array();*/

    }

    function category_incat($category_id){

        //echo $catid;
        $this->db->select("sub_category.`subCategoryId`, sub_category.`categoryId_Fk`, sub_category.	subCategory_Name, sub_category.`subCategory_Image`, sub_category.`title`, sub_category.`description`, sub_category.`keywords`, category.`category_Name`, category.`categoryId`, category.`category_image`,category.`category_icon`, category.`category_color`, category.`Description`, category.`title`, category.`Keywords`");
        $this->db->from('sub_category');
        $this->db->join('category','category.categoryId = sub_category.categoryId_Fk');
        $this->db->where('category.categoryId',$category_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    function subcategory($catid){

        //echo $catid;
        $this->db->select('*');

        $this->db->from('sub_category');
        $this->db->where('categoryId_Fk',$catid);
        $query = $this->db->get();
        return $query->result_array();

    }

    function insert_userregistration($values){


        $this->db->insert('user_registration', $values);
        return $this->db->insert_id();

    }
    function insert_fbuser($values){


        $this->db->insert('facebookinfo_user', $values);
        return $this->db->insert_id();

    }

    function checkuser($useremail){
        $this->db->select('*');
        $this->db->from('facebookinfo_user');
        $this->db->where('email',$useremail);
        $query = $this->db->get();
        return $query->result_array();

    }
    function inviteusercheck($username, $password){
        // $isActive = 0;
        $this->db->select('*');
        $this->db->from('userinvit_login');
        $this->db->where('username',$username);
        $this->db->where('password',$password);
        //$this->db->where('isActive',$isActive);
        $query = $this->db->get();
        //print_r($query);exit;
        return $query->result_array();
    }

    function brand($brand_id){
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brandid',$brand_id);
        $query = $this->db->get();
        return $query->result_array();

    }

    function management_view($brand_id){
        $this->db->select('*');
        $this->db->from('management_view');
        $this->db->where('brandid_fk',$brand_id);
        $query = $this->db->get();
        return $query->result_array();

    }

    function brand_wise($brand_id){


        /* $this->db->select('product.`productId`, product.`created_on`, product.`productSmallDiscription`, category.categoryid,product.`product_Name`,category.category_color,category.`category_icon`,product.`product_image`, brand.brandId, brand.`brand_Name`, brand.`brand_image`, sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`, category.`categoryId`');
                $this->db->from('product');
                $this->db->join('brand','brand.brandId = product.brandId_Fk');
                $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
                $this->db->join('category','category.categoryId = product.categoryId_Fk');

                 $this->db->where('product.brandId_Fk',$brand_id);
                $query = $this->db->get();
                //print_r($query);
                return $query->result_array(); */

        $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`,`brand`.`brand_image`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`
WHERE `product`.`brandId_Fk` = $brand_id limit 0,4";
//echo $sql;

        $result = $this->db->query($sql)->result_array();

        return $result;

    }

    function product($product_id){

        /*SELECT specification.`specificationId`, specification.`specification_Desc`, specification.`productId_Fk`
        , specification.`brandId_Fk`, specification_name.`specificationNameId`
        , specification_name.`specification_Name`, specification_type.`specifictionTypeId`
        , specification_type.`specifictionType_Name` FROM specification
         INNER JOIN specification_type ON specification.`specificationTypeId_Fk` = specification_type.`specifictionTypeId`
         INNER JOIN specification_name ON specification.specificationNameId_Fk = specification_name.specificationNameId WHERE specification.productId_Fk = 2 */

        $this->db->select('specification.`specificationId`, specification.`specification_Desc`, specification.`productId_Fk`
  , specification.`brandId_Fk`, specification_name.`specificationNameId`
  , specification_name.`specification_Name`, specification_type.`specifictionTypeId`
  , specification_type.`specifictionType_Name`');
        $this->db->from('specification');
        $this->db->join('specification_type','specification.specificationTypeId_Fk = specification_type.specifictionTypeId');
        // $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('specification_name','specification.specificationNameId_Fk = specification_name.specificationNameId');
        $this->db->group_by('specification`.`specificationTypeId_Fk');
        // $this->db->join('specification','specification.productId_Fk = product.productId');
        $this->db->where('specification.productId_Fk',$product_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    function get_category_res($productid,$featureid){


        $this->db->select('specification.`specificationId`, specification.`specification_Desc`, specification.`productId_Fk`
  , specification.`brandId_Fk`, specification_name.`specificationNameId`
  , specification_name.`specification_Name`, specification_type.`specifictionTypeId`
  , specification_type.`specifictionType_Name`');
        $this->db->from('specification');
        $this->db->join('specification_type','specification.specificationTypeId_Fk = specification_type.specifictionTypeId');
        // $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('specification_name','specification.specificationNameId_Fk = specification_name.specificationNameId');
        // $this->db->group_by('specification`.`specificationTypeId_Fk');
        // $this->db->join('specification','specification.productId_Fk = product.productId');
        $this->db->where('specification.productId_Fk',$productid);
        $this->db->where('specification.specificationTypeId_Fk',$featureid);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();

    }


    function single_product($product_id){

        /*SELECT specification.`specificationId`, specification.`specification_Desc`, specification.`productId_Fk`
        , specification.`brandId_Fk`, specification_name.`specificationNameId`
        , specification_name.`specification_Name`, specification_type.`specifictionTypeId`
        , specification_type.`specifictionType_Name` FROM specification
         INNER JOIN specification_type ON specification.`specificationTypeId_Fk` = specification_type.`specifictionTypeId`
         INNER JOIN specification_name ON specification.specificationNameId_Fk = specification_name.specificationNameId WHERE specification.productId_Fk = 2 */

        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('productId',$product_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }


    function searchproduct($searchdata){

        $this->db->select('product.`productId`, product.`created_on`, product.`product_Name`, ,product.`productSmallDiscription`, category.category_color,product.`product_image`, brand.brandId, brand.`brand_Name`, sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,category.`category_icon`, category.`categoryId`');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->where("product.product_Name LIKE '%$searchdata%'");
        $query = $this->db->get();
        return $query->result_array();
    }

    function searchbrand($searchdata){


        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where("brand.`brand_Name` LIKE '%$searchdata%'");
        $query = $this->db->get();
        return $query->result_array();
    }


    function searchpeople($searchdata){

        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->like('firstname', $searchdata);
        $this->db->or_like('lastname', $searchdata);
        // $this->db->where("user_registration.`firstname` LIKE '%$searchdata%'");
        //$this->db->where("user_registration.`lastname` LIKE '%$searchdata%'");
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_all_category(){
        $this->db->select('*');
        $this->db->from('category');
        $query=$this->db->get();
        return $query->result_array();

    }

    function get_all_subcategory($cat_id){
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('categoryId_Fk',$cat_id);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_avail_subcategory($cat_id){
        $this->db->select('product.`productId`,product.`product_Name`, product.`productSmallDiscription`,category.category_color,product.`product_image`, brand.brandId, brand.`brand_Name`,sub_category.`headersubcat_icon`,sub_category.subCategoryId,sub_category.mainsubcat_icon, sub_category.`subCategory_Name`, category.`category_Name`, category.`categoryId`');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->where('product.categoryId_Fk',$cat_id);
        $this->db->group_by('sub_category.subCategory_Name');
        $this->db->order_by('sub_category.subCategoryId');
        $query=$this->db->get();
        return $query->result_array();
    }

    function product_catgory($productid){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('productId',$productid);
        $query=$this->db->get();
        return $query->result_array();
    }

    function product_highlight($productid){
        $this->db->select('*');
        $this->db->from('product_highlight');
        $this->db->where('productId_Fk',$productid);
        $query=$this->db->get();
        return $query->result_array();

    }

    function product_image($productid){
        $this->db->select('*');
        $this->db->from('images');
        $this->db->where('productId_Fk',$productid);
        $query=$this->db->get();
        return $query->result_array();

    }

    function get_category_name($category_id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('categoryId',$category_id);
        $query=$this->db->get();
        return $query->result_array();

    }

    function get_brand_name($brand_id){
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brandId',$brand_id);
        $query=$this->db->get();
        return $query->result_array();

    }

    function get_subcategory_name($subcat_id){
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('subCategoryId',$subcat_id);
        $query=$this->db->get();
        return $query->result_array();

    }

    function seo($field,$fiels_value){

        $this->db->select('*');
        $this->db->from('seo');
        $this->db->where($field,$fiels_value);
        $query=$this->db->get();
        return $query->result_array();
    }

    function slider($field,$fiels_value){

        $this->db->select('*');
        $this->db->from('slider');
        $this->db->where($field,$fiels_value);
        $query=$this->db->get();
        return $query->result_array();
    }


    public function insertquery(){

        $data = array(
            'name' => $this->input->post('name'),
            'mobile' => $this->input->post('contactno'),
            'emailid' => $this->input->post('emailid'),
            'query' => $this->input->post('query'),
            'formname' => $this->input->post('formname'),

        );
        return $this->db->insert('query', $data);

    }

    public function inserttrynow()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'mobile' => $this->input->post('contactno'),
            'emailid' => $this->input->post('emailid'),
            'date' => $this->input->post('date'),
            'time' => $this->input->post('time'),
            'query' => $this->input->post('query'),
            'formname' => $this->input->post('formname'),

        );
        return $this->db->insert('query', $data);

    }

    public function contactus()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'mobile' => $this->input->post('contactno'),
            'emailid' => $this->input->post('emailid'),
            'query' => $this->input->post('query'),
            'formname' => $this->input->post('formname'),

        );
        return $this->db->insert('query', $data);

    }

    public function userregistration_signup($userinviteid, $fullFilePath)
    {
        $data = array(
            'firstname' => $this->input->post('fname'),
            'lastname' => $this->input->post('lname'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'auth_provider' => $this->input->post('auth'),
            'userinviteid' => $userinviteid,
            'profile_picture' => $fullFilePath
        );
        $this->db->insert('user_registration', $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function userregistration_signupwoimage($userinviteid)
    {
        $data = array(
            'firstname' => $this->input->post('fname'),
            'lastname' => $this->input->post('lname'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'auth_provider' => $this->input->post('auth'),
            'userinviteid' => $userinviteid,
        );
        $this->db->insert('user_registration', $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function uploadpic($fullFilePath, $userinsertid)
    {
        $data = array(
            'userid' => $userinsertid,
            'profilepic_path' =>  $fullFilePath,
        );
        return  $this->db->insert('userprofile_pic', $data);
    }

    public function userluxry_intrest($userintrest, $userinsertid)
    {
        $userintrest1 = explode(',',$userintrest);
        // print_r ($userintrest1);
        for($i=0;$i<count($userintrest1);$i++){
            $data = array(
                'userid' => $userinsertid,
                'cat_name' => $userintrest1[$i],
                'smartfollow' => $this->input->post('allcat')
            );
            $this->db->insert('userluxry_intrest', $data);

        }
    }

    public function userfollowcategory($userintrest, $userinsertid)
    {
        $userintrest1 = explode(',',$userintrest);
        // print_r ($userintrest1);
        for($i=0;$i<count($userintrest1);$i++){
            $data = array(
                'user_id' => $userinsertid,
                'category_id' => $userintrest1[$i]
            );
            $this->db->insert('following', $data);

        }
    }


    public function user_notification($notification, $userinsertid)
    {
        $data = array(
            'userid' => $userinsertid,
            'contact_type' =>  $notification,
            'reminder_type' => $this->input->post('getinfo'),
        );
        return  $this->db->insert('notification_type', $data);
    }

    public function user_detail($userinsertid)
    {
        $data = array(
            'userid' => $userinsertid,
            'subtitle' => $this->input->post('subtitlefield'),
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'dob' => $this->input->post('dob'),
            'email' => $this->input->post('email'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'occupation' => $this->input->post('Occupation'),
        );
        return  $this->db->insert('user_detail', $data);
    }

    public function user_login($userinsertid)
    {
        $data = array(
            'userid' => $userinsertid,
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'email' => $this->input->post('email'),
        );
        return  $this->db->insert('user_login', $data);
    }


    function category_onsubcat($subcategory_id){

        //echo $catid;
        $this->db->select("sub_category.`subCategoryId`, sub_category.`categoryId_Fk`, sub_category.	subCategory_Name, sub_category.`subCategory_Image`,sub_category.`mainsubcat_icon`, sub_category.`title`, sub_category.`description`, sub_category.`keywords`, category.`category_Name`, category.`categoryId`, category.`category_image`, category.`category_color`, category.`Description`, category.`title`, category.`Keywords`");
        $this->db->from('sub_category');
        $this->db->join('category','category.categoryId = sub_category.categoryId_Fk');
        $this->db->where('sub_category.subCategoryId',$subcategory_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }


    public function count_crown($productid){
        $this->db->select('count(product_id) as no');
        $this->db->from('user_crown');
        $this->db->where('product_id',$productid);
        $query=$this->db->get();
        return $query->result_array();
    }

    public function postcount_crown($post_id){
        $this->db->select('count(post_id) as no');
        $this->db->from('user_crown');
        $this->db->where('post_id',$post_id);
        $query=$this->db->get();
        return $query->result_array();
    }

    public function count_follower($userid){
        $this->db->select('count(follower_id) as no');
        $this->db->from('following');
        $this->db->where('follower_id',$userid);

        $this->db->where('follow',"1");
        $query=$this->db->get();
        return $query->result_array();

    }



    function user_details($userregistrationid){

        $this->db->select('`user_registration`.`registrationid`,`user_registration`.`status`,`user_registration`.`userinviteid`,`user_registration`.`social_id`,`user_registration`.`linkedinid`,`user_registration`.`email`,`user_registration`.`auth_provider`,`user_registration`.`locale`,`user_registration`.`gender`,`user_registration`.`firstname`,`user_registration`.`lastname`,`user_registration`.`username`,`user_registration`.`profile_picture`,`user_detail`.`occupation`,`user_detail`.`subtitle`');
        $this->db->from('user_registration');
        $this->db->join('user_detail','user_detail.userid = user_registration.registrationid');
        $this->db->where('registrationid',$userregistrationid);
        $query=$this->db->get();
        return $query->result_array();



    }
    function get_followed_brand($userregistrationid,$field){

        $arr=[];
        $this->db->select($field);
        $this->db->from('following');
        $this->db->where('user_id',$userregistrationid);
        // $this->db->where($field!=0);
        $query=$this->db->get();
        $result= $query->result_array();
        $sr=0;
        foreach ($result as $values) {

            $this->db->select('productId');
            $this->db->from('product');
            $this->db->where('brandid_fk',$values['brand_id']);
            $query=$this->db->get();
            $qr= $query->result_array();
            foreach ($qr as $value) {
                $arr[$sr]=$value['productId'];
                $sr++;
            }

        }
        return $arr;

    }

    function get_followed_category($userregistrationid,$field){

        $arr=[];
        $this->db->select($field);
        $this->db->from('following');
        $this->db->where('user_id',$userregistrationid);
        // $this->db->where($field!=0);
        $query=$this->db->get();
        $result= $query->result_array();
        foreach ($result as $values) {
            $this->db->select('productId');
            $this->db->from('product');
            $this->db->where('categoryId_Fk',$values['category_id']);
            $query=$this->db->get();
            $sr=0;
            $qr= $query->result_array();
            foreach ($qr as $value) {
                $arr[$sr]=$value['productId'];
                $sr++;
            }

        }
        return $arr;
    }

    function get_all_products($data){
        $arr=[];
        foreach ($data as $values) {
            $this->db->select('product.`productId`,product.`product_Name`, product.`productSmallDiscription`,product.`product_image`,product. `product_Title`, product.`productlongDiscription`,product.`created_on` as date_time,category.category_color,brand.brandId, brand.`brand_Name`,
		 								  sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,category.`category_icon`, category.`categoryId`, product.`Price`, product.`model_no`, `gender`, product.`rank`, product.`isActive`, `new`, product.`updated_on`');
            $this->db->from('product');
            $this->db->join('brand','brand.brandId = product.brandId_Fk');
            $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
            $this->db->join('category','category.categoryId = product.categoryId_Fk');
            $this->db->where('productId',$values);
            $query=$this->db->get();
            if($query->num_rows() > 0){
                $arr[]= $query->result_array();
            }
        }
        return $arr;

    }

    // for the scroll limit

    function getall_scroll($page){

        $this->db->select('product.`productId`,product.`product_Name`,category.category_color,product.`product_image`, brand.brandId, brand.`brand_Name`, sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`, category.`categoryId`');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');

        $this->db->limit(3,$page);


        $query = $this->db->get();
        return $query->result_array();

    }

    // for getting user information

    function user_info($userid){
        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->where('registrationid',$userid);
        $query = $this->db->get();
        return $query->result_array();

    }
    function get_user_post($post_id){

        $this->db->select('*');
        $this->db->from('user_post');
        $this->db->where('id',$post_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //models for the timeline manuplation



    function get_comment_all($user_id){


        $this->db->select('product.`productId`,product.`product_Name`,product.`product_image`,product.`productSmallDiscription`,category.category_color,brand.brandId, brand.`brand_Name`,sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,category.`category_icon`,
		 category.`categoryId`,comment.date_time as date_time,comment.comment,comment.source as source,product.productId as post_id');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->join('comment','product.productId=comment.product_id');

        $this->db->where('comment.user_id',$user_id);
        $this->db->group_by("product.productId");
        $query = $this->db->get();
        //print_r($query);
        return $comment_sec=$query->result_array();
        /*$this->db->select('product_id as comment_product_id,comment,date_time as comment_date');
        $this->db->from('comment');
        $this->db->where('user_id','1');
        $query = $this->db->get();
       $comment_sec= $query->result_array();
       */
    }





    function get_posts_all($user_id){

        if($user_id==$this->session->userdata('registrationid')) {
            $this->db->select('post_data as user_post_data,image as post_image,date_time as date_time,source,id as post_id');
            $this->db->from('user_post');
            $this->db->where('user_id', $user_id);
            //$this->db->where('visibility', 1);
            $query = $this->db->get();
            return $query->result_array();

        }else {
            $this->db->select('post_data as user_post_data,image as post_image,date_time as date_time,source,id as post_id');
            $this->db->from('user_post');
            $this->db->where('user_id', $user_id);
            $this->db->where('visibility', 1);
            $query = $this->db->get();
            return $query->result_array();

        }

    }



    function get_crowns_all($user_id){
        $this->db->select('product.`productId`,product.`product_Name`,product.`product_image`,product.`productSmallDiscription`,category.category_color,brand.brandId, brand.`brand_Name`,
		 sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,category.`category_icon`, 
		 category.`categoryId`,user_crown.date_time as date_time,user_crown.source as source,,user_crown.id as post_id');
        $this->db->from('product');
        $this->db->join('brand','brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category','category.categoryId = product.categoryId_Fk');
        $this->db->join('user_crown','product.productId=user_crown.product_id');
        $this->db->where('user_crown.user_id',$user_id);
        $this->db->where('user_crown.crown','1');
        $this->db->group_by("product.productId");
        $query = $this->db->get();
        //print_r($query);
        return  $crown_sec=$query->result_array();




        /*                $this->db->select('product_id as crown_product_id,date_time as comment_date');
                        $this->db->from('user_crown');
                        $this->db->where('user_id','1');
                        $query = $this->db->get();
                        return $query->result_array();


                        */

    }

    function get_following_all($user_id){

    }

    //models for the timeline manuplation ends here


    function friendship_status($user_id,$friend_id){

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id',$user_id);
        $this->db->where('follower_id',$friend_id);
        $query = $this->db->get();
        return  $query->result_array();

    }

    function follower_id($friend_name){
        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->where('username',$friend_name);
        $query = $this->db->get();
        return $query->result_array();

    }

    //for the welcome post

    function user_first_post($userid){


        $user_details = array('post_data' => 'Welcome to rigalio!','user_id' =>$userid);
        $this->db->insert('user_post',$user_details);
        $user_insert_id=$this->db->insert_id();
        return $user_insert_id;

    }

    function count_comment($productid){
        $this->db->select('count(comment) as no');
        $this->db->from('comment');
        $this->db->where('product_id',$productid);
        $query=$this->db->get();
        return $query->result_array();

    }

    function checkuserinviteid($inviteuserid){
        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->where('userinviteid',$inviteuserid);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateuserinvite($userinviteid)
    {
        $data = array('isActive'=>1);
        $this->db->where('invitelogin_id',$userinviteid);
        $this->db->update('userinvit_login',$data);
        //return $result;
        //print_r($data);
    }

    public function aftersignupregid($userinviteid)
    {
        $this->db->select('registrationid');
        $this->db->from('user_registration');
        $this->db->where('userinviteid',$userinviteid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function loginaftersignup($registrationid, $username, $password)
    {
        //echo $registrationid; echo $username; echo $password; exit;
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where('userid', $registrationid);
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get();
        return $query->result_array();
    }

    function isactivenotfind($username, $password)
    {
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where('username',$username);
        $this->db->where('password',$password);
        $this->db->or_where('email',$username);
        $query = $this->db->get();
        return $query->result_array();
    }

    function alreadyuserexist($username)
    {
        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->where('username',$username);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getuserdata($userregistrationid)
    {
        $this->db->select('user_registration.registrationid, user_registration.userinviteid, user_registration.firstname, user_registration.lastname, user_registration.profile_picture, user_registration.email, user_registration.username');
        $this->db->from('user_registration');
        //$this->db->join('userprofile_pic','user_registration.registrationid = userprofile_pic.userid');
        $this->db->where('user_registration.registrationid', $userregistrationid);
        $query = $this->db->get();
        return $query->result_array();
    }



    public function get_friends_id($userregistrationid){

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id',$userregistrationid);
        $this->db->where('follower_id!=','0');
        $this->db->where('follow=','1');
        $query = $this->db->get();
        return $query->result_array();

    }


    function get_comment_friends_all($user_id){
        $arr=[];
        foreach ($user_id as $values) {
            $this->db->select('product.`productId`,product.`created_on`,product.`product_Name`,product.`productSmallDiscription`,product.`product_image`,brand.brandId, brand.`brand_Name`,
		 									 sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`,category.`category_color`,category.`category_icon`, 
		 									 category.`categoryId`,comment.date_time as date_time,comment.comment,comment.source as source,product.productId as post_id,user_registration.`profile_picture` as profile_pic,user_registration.`firstname` as username,user_registration.registrationid as userid');
            $this->db->from('product');
            $this->db->join('brand','brand.brandId = product.brandId_Fk');
            $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
            $this->db->join('category','category.categoryId = product.categoryId_Fk');
            $this->db->join('comment','product.productId=comment.product_id');
            $this->db->join('user_registration','user_registration.registrationid=comment.user_id');

            $this->db->where('comment.user_id',$values['follower_id']);
            $this->db->group_by("product.productId");
            $query = $this->db->get();
            //print_r($query);
            $arr[]=$query->result_array();
        }
        return $arr;
        /*$this->db->select('product_id as comment_product_id,comment,date_time as comment_date');
       $this->db->from('comment');
       $this->db->where('user_id','1');
       $query = $this->db->get();
      $comment_sec= $query->result_array();
      */


    }


    function get_crowns_friends_all($user_id){


        $arr=[];
        foreach ($user_id as $values) {

            $this->db->select('product.`productId`,product.`product_Name`,product.`product_image`,product.`productSmallDiscription`,brand.brandId, brand.`brand_Name`,
		 									 sub_category.subCategoryId, sub_category.`subCategory_Name`,category.`category_color`,category.`category_Name`,category.`category_icon`, 
		 									 category.`categoryId`,user_crown.date_time as date_time,user_crown.source as source,,user_crown.id as post_id,user_registration.`profile_picture` as profile_pic,user_registration.`firstname` as username,user_registration.registrationid as userid');
            $this->db->from('product');
            $this->db->join('brand','brand.brandId = product.brandId_Fk');
            $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');

            $this->db->join('category','category.categoryId = product.categoryId_Fk');
            $this->db->join('user_crown','product.productId=user_crown.product_id');
            $this->db->join('user_registration','user_registration.registrationid=user_crown.user_id');
            $this->db->where('user_crown.user_id',$values['follower_id']);
            $this->db->where('user_crown.crown','1');
            $this->db->group_by("product.productId");
            $query = $this->db->get();
            //print_r($query);
            $arr[]=$query->result_array();

        }

        return $arr;



    }

    function get_posts__friends_all($user_id){


        $arr=[];
        foreach ($user_id as $values) {

            $this->db->select('post_data as user_post_data,image as post_image,date_time as date_time,source,id as post_id,user_registration.`profile_picture` as profile_pic,user_registration.`firstname` as username,user_registration.registrationid as userid');
            $this->db->from('user_post');
            $this->db->where('user_id',$values['follower_id']);
            $this->db->where('user_post.visibility',1);
            $this->db->join('user_registration','user_registration.registrationid=user_post.user_id');
            $query = $this->db->get();
            $arr[]=$query->result_array();

        }

        return $arr;
    }
    public function get_all_notification($friends_id){
        $arr=[];
        foreach ($friends_id as $friends) {
            # code...

            if($friends['follow']==0){

                $this->db->select('*');
                $this->db->from('user_registration');
                //$this->db->where('follow','0');
                $this->db->where('registrationid',$friends['user_id']);


                $query=$this->db->get();
                $arr[]= $query->result_array();
            }
        }

        return $arr;


    }

    public function timeline_feed($values){

        // print_r($values);

        $this->db->insert('timeline', $values);
        return $this->db->insert_id();


    }

    public function timeline_of($userregistrationid){


        $this->db->select('*');
        $this->db->from('timeline');
        $this->db->where('me_id',$userregistrationid);
        // $this->db->where('user_id!=','0');
        $query = $this->db->get();
        return $query->result_array();

    }
    function get_gallery_image($user_id){

        $this->db->select('*');
        $this->db->from('user_post');
        $this->db->join('user_registration','user_registration.registrationid=user_post.user_id');
        $this->db->where('user_post.user_id',$user_id);
        $this->db->where('user_post.image!=""');
        $this->db->order_by('user_post.id','desc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_user_gallery($user_id){

        $this->db->select('*');
        $this->db->from('user_post');
        $this->db->join('user_registration','user_registration.registrationid=user_post.user_id');
        $this->db->where('user_post.user_id',$user_id);
        $this->db->where('user_post.image!=""');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_collection($userregistrationid){


        $this->db->select('count(saveforlater.product_id) as no,sub_category.subCategoryId, sub_category.`subCategory_Name`,`sub_category`.`subCategory_Image`,saveforlater.status');
        $this->db->from('saveforlater');
        $this->db->join('sub_category','saveforlater.subcat_id = sub_category.subCategoryId');
        $this->db->where('saveforlater.user_id',$userregistrationid);
        $this->db->group_by('sub_category.subCategoryId');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_users(){
        $this->db->select('registrationid');
        $this->db->from('user_registration');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_followers_id($userregistrationid){

        $this->db->select('*');
        $this->db->from('following');
        // $this->db->where('user_id',$userregistrationid);
        $this->db->where('follower_id',$userregistrationid);
        $this->db->where('follow','1');
        $query = $this->db->get();
        return $query->result_array();

    }


    public function get_pending_id($userregistrationid){

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('follower_id',$userregistrationid);
        $this->db->where('user_id!=','0');
        $this->db->where('follow=','0');
        $query = $this->db->get();
        return $query->result_array();

    }

    public function get_all_followers($friends_id){
        $arr=[];
        foreach ($friends_id as $friends) {
            $this->db->select('`user_registration`.`registrationid`,`user_registration`.`status`,`user_registration`.`userinviteid`,`user_registration`.`social_id`,`user_registration`.`linkedinid`,`user_registration`.`email`,`user_registration`.`auth_provider`,`user_registration`.`locale`,`user_registration`.`gender`,`user_registration`.`firstname`,`user_registration`.`lastname`,`user_registration`.`username`,`user_registration`.`profile_picture`,`user_detail`.`occupation`');
            $this->db->from('user_registration');
            $this->db->join('user_detail','user_detail.userid = user_registration.registrationid');
            $this->db->where('registrationid',$friends['user_id']);

            $query=$this->db->get();
            $arr[]= $query->result_array();
        }

        return $arr;

    }


    public function get_all_friends($friends_id){
        $arr=[];
        foreach ($friends_id as $friends) {
            # code...
            $this->db->select('`user_registration`.`registrationid`,`user_registration`.`status`,`user_registration`.`userinviteid`,`user_registration`.`social_id`,`user_registration`.`linkedinid`,`user_registration`.`email`,`user_registration`.`auth_provider`,`user_registration`.`locale`,`user_registration`.`gender`,`user_registration`.`firstname`,`user_registration`.`lastname`,`user_registration`.`username`,`user_registration`.`profile_picture`,`user_detail`.`occupation`');
            $this->db->from('user_registration');
            $this->db->join('user_detail','user_detail.userid = user_registration.registrationid');
            $this->db->where('registrationid',$friends['follower_id']);

            $query=$this->db->get();
            $arr[]= $query->result_array();
        }

        return $arr;

    }


    public function count_brandfollower($brandid){
        $this->db->select('count(id) as no');
        $this->db->from('following');
        $this->db->where('brand_id',$brandid);
        $query=$this->db->get();
        return $query->result_array();
    }

    function insert_fbdetail($values){
        $this->db->insert('facebookinfo_user', $values);
        return $this->db->insert_id();
    }


    public function my_notification($user_id){

        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where('user_id',$userregistrationid);
        $query = $this->db->get();
        return $query->result_array();

    }

    public function my_posts_notifcations($user_id){

        $this->db->select('*');
        $this->db->from('user_post');
        $this->db->where('user_id',$user_id);
        $query=$this->db->get();
        return $query->result_array();



    }


    public function collect_notification(){


        $sql="SELECT user_post.`user_id` as owner_id,notifications.`post_type`,notifications.`user_id` as action_user_id, notifications.`date_time`,user_post.`post_data` as date_time FROM `user_post` join notifications on user_post.`id`=notifications.`post_id`";
        $result = $this->db->query($sql)->result_array();

        return $result;
    }


    public function get_comments($post_id) {

        $this->db->select('*');
        $this->db->from('comment');
        $this->db->join('user_registration','user_registration.registrationid=comment.user_id');
        $this->db->where('comment.post_id',$post_id);
        $query = $this->db->get();
        return $query->result_array();

    }

    public function allcountry(){

        $this->db->select('*');
        $this->db->from('countries');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function finest(){
        $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` where `product`.`finest`=1";

        $result = $this->db->query($sql)->result_array();

        return $result;

    }

    public function updateprofilepic($userregistrationid, $fullFilePath)
    {
        $data = array('profile_picture'=> $fullFilePath);
        $this->db->where('registrationid',$userregistrationid);
        $this->db->update('user_registration',$data);
        //return $result;
        //print_r($data);
    }

    public function requestndinvite()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'emailid' => $this->input->post('email'),
            'linkedinlink' => $this->input->post('userinkedin'),
            'phoneno' => $this->input->post('phoneno'),
            'company' => $this->input->post('company'),
            'designation' => $this->input->post('designation'),
        );
        return  $this->db->insert('requestndinvite', $data);
    }

    public function following_brands_sidebar($userregistrationid,$field){

        if(!$userregistrationid){
            redirect(base_url());
        }
        $this->db->select($field);
        $this->db->from('following');
        $this->db->where('user_id',$userregistrationid);
        //$this->db->order_by('id')

        // $this->db->where($field!=0);
        $query=$this->db->get();
        $result= $query->result_array();

        //print_r($result);
        $arr=[];
        foreach ($result as $value) {
            $arr[]=$value['brand_id'];
        }
//print_r($arr); exit;
        if(count($arr)>0){
            $this->db->select('*');
            $this->db->from('brand');
            $this->db->where_in('brandId', $arr);
            $query=$this->db->get();
            return $query->result_array();

        }

    }
    function count_commentonpost($post_id){
        $this->db->select('count(comment) as no');
        $this->db->from('comment');
        $this->db->where('post_id',$post_id);

        $query=$this->db->get();
        return $query->result_array();

    }

    public function crownuser_detail($productid){
        $this->db->select('user_crown.`user_id`,user_crown.`source`, user_crown.`product_id`, user_registration.`registrationid`,     user_registration.`firstname`, user_registration.`lastname`');
        $this->db->from('user_crown');
        $this->db->join('user_registration','user_registration.registrationid = user_crown.user_id');
        $this->db->where('user_crown.product_id',$productid);
        $this->db->group_by('user_crown.user_id');
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    public function commentuser_detail($productid){
        $this->db->select('comment.`user_id`,comment.`source`, comment.`product_id`, user_registration.`registrationid`,     user_registration.`firstname`, user_registration.`lastname`');
        $this->db->from('comment');
        $this->db->join('user_registration','user_registration.registrationid = comment.user_id');
        $this->db->where('comment.product_id',$productid);
        $this->db->group_by('comment.user_id');
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    public function crownuser_detailpost($postid){
        $this->db->select('user_crown.`user_id`,user_crown.`source`, user_crown.`post_id`, user_registration.`registrationid`,     user_registration.`firstname`, user_registration.`lastname`');
        $this->db->from('user_crown');
        $this->db->join('user_registration','user_registration.registrationid = user_crown.user_id');
        $this->db->where('user_crown.post_id',$postid);
        $this->db->group_by('user_crown.user_id');
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    public function commentuser_detailpost($postid){
        $this->db->select('comment.`user_id`,comment.`source`, comment.`post_id`, user_registration.`registrationid`,     user_registration.`firstname`, user_registration.`lastname`');
        $this->db->from('comment');
        $this->db->join('user_registration','user_registration.registrationid = comment.user_id');
        $this->db->where('comment.post_id',$postid);
        $this->db->group_by('comment.user_id');
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }

    public function post_detail($postid){
        $this->db->select('*');
        $this->db->from('user_post');
        $this->db->where('id',$postid);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }



    public function product_by_subcategory($subcat_id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('subcategoryId_Fk',$subcat_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }


    public function delete_post($user_id,$post_id){
        $this->db->where('id', $post_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('user_post');
    }

    public function myhide_product($user_id){

        $this->db->select('product_id');
        $this->db->from('hide_me');
        $this->db->where('user_id',$user_id);
        $query=$this->db->get();
        return $query->result_array();
    }

    function hide_me($user_id,$product_id){

        $this->db->select('*');
        $this->db->from('hide_me');
        $this->db->where('user_id',$user_id);
        $this->db->where('product_id',$product_id);
        $query=$this->db->get();
        $result= $query->result_array();
        if ($query->num_rows() < 1){
            $user_details = array('user_id' => $user_id,'product_id' =>$product_id);
            $this->db->insert('hide_me',$user_details);
            $user_insert_id=$this->db->insert_id();
            return $user_insert_id;

        }

        return "already";

    }
    public function myhidden_post($userregistrationid){

        $this->db->select('product_id');
        $this->db->from('hide_me');
        $this->db->where('user_id',$userregistrationid);

        $query=$this->db->get();

        return $result= $query->result_array();

    }

    function hide_mepost($user_id,$post_id){

        $this->db->select('*');
        $this->db->from('hide_me');
        $this->db->where('user_id',$user_id);
        $this->db->where('post_id',$post_id);
        $query=$this->db->get();
        $result= $query->result_array();
        if ($query->num_rows() < 1){
            $user_details = array('user_id' => $user_id,'post_id' =>$post_id);
            $this->db->insert('hide_me',$user_details);
            $user_insert_id=$this->db->insert_id();
            return $user_insert_id;

        }

        return "already";

    }


    public function myhidden_userpost($userregistrationid){

        $this->db->select('post_id');
        $this->db->from('hide_me');
        $this->db->where('user_id',$userregistrationid);

        $query=$this->db->get();

        return $result= $query->result_array();

    }


    public function count_crown_news($news_id){
        $this->db->select('count(news_id) as no');
        $this->db->from('user_crown');
        $this->db->where('news_id',$news_id);
        $query=$this->db->get();
        return $query->result_array();
    }


}