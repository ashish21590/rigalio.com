<?php
class Settingmodel extends CI_Model {
	
	public function __construct()
	{
	   $this->load->database();
	}
	
	public function getuserdataall($userregistrationid)
	{
	  $this->db->select('user_registration.registrationid, user_registration.userinviteid, user_registration.firstname, user_registration.lastname, user_registration.profile_picture, user_registration.email, userluxry_intrest.userid, userluxry_intrest.cat_name, notification_type.userid, notification_type.contact_type, notification_type.reminder_type, user_login.userid, user_login.username, user_login.password, user_detail.dob, user_detail.userid, user_detail.phone');
	  $this->db->from('user_registration');
	  $this->db->join('userluxry_intrest','user_registration.registrationid = userluxry_intrest.userid');
	  $this->db->join('notification_type','user_registration.registrationid = notification_type.userid');
	  $this->db->join('user_login','user_registration.registrationid = user_login.userid');
	  $this->db->join('user_detail','user_registration.registrationid = user_detail.userid');
	  //$this->db->join('userprofile_pic','user_registration.registrationid = userprofile_pic.userid');
	  //$this->db->join('user_crown','user_registration.registrationid = user_crown.user_id');
	  //$this->db->join('comment','user_registration.registrationid = comment.user_id');
	  $this->db->where('user_registration.registrationid', $userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	public function getuserdata($userregistrationid)
  {
	  $this->db->select('user_registration.registrationid, user_registration.userinviteid, user_registration.firstname, user_registration.lastname, user_registration.profile_picture, user_registration.email, user_registration.username');
	  $this->db->from('user_registration');
	  //$this->db->join('userprofile_pic','user_registration.registrationid = userprofile_pic.userid');
	  $this->db->where('user_registration.registrationid', $userregistrationid);
	  $query = $this->db->get();
	  
	  return $query->result_array();
  }
	
	function getuserpic($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('userprofile_pic');
	  $this->db->where('userid',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function getusercrown($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('user_crown');
	  $this->db->where('user_id',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function getusercomment($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('comment');
	  $this->db->where('user_id',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	public function alluseremail($userregistrationid)
	{
	  $isActive = 0;
	  $this->db->select('*');
	  $this->db->from('useremails');
	  $this->db->where('userid', $userregistrationid);
	  $this->db->where('isActive',$isActive);
	  $query = $this->db->get();
	  return $query->result_array(); 
    }
	
	function passexist($currentpass, $userregistrationid)
    {
	  $this->db->select('*');
	  $this->db->from('user_login');
	  $this->db->where('password',$currentpass);
	  $this->db->where('userid',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function passupdate($newpass, $userregistrationid)
    {
	  $data = array(
			'password' => $newpass,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_login', $data);
    }
	
	function dobupdate($newdob, $userregistrationid)
    {
	  $data = array(
			'dob' => $newdob,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_detail', $data);
    }
	
	function phonenoexist($userregistrationid)
    {
	  $this->db->select('*');
	  $this->db->from('user_detail');
	  $this->db->where('userid',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function addphoneno($addphone, $userregistrationid)
    {
	  $data = array(
			'phone' => $addphone,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_detail', $data);
    }
	
	function addmorephoneno($addphone, $userregistrationid)
    {
	  $data = array(
			'userid' => $userregistrationid,
			'phoneno' => $addphone,
			 );
	 return  $this->db->insert('userphoneno', $data);	
    }
	
	public function alluserphoneno($userregistrationid)
	{
	  $isActive = 0;
	  $this->db->select('*');
	  $this->db->from('userphoneno');
	  $this->db->where('userid', $userregistrationid);
	  $this->db->where('isActive',$isActive);
	  $query = $this->db->get();
	  return $query->result_array(); 
    }
	
	function addmoreemail($addemail, $userregistrationid)
    {
	  $data = array(
			'userid' => $userregistrationid,
			'useremail' => $addemail,
			 );
	 return  $this->db->insert('useremails', $data);	
    }
	
	public function deleteemailid($id)
	{
		$this->db->where('useremailid',$id);	
		$this->db->delete('useremails');
	}
	
	function getprimaryemail($userregistrationid)
    {
	  $this->db->select('*');
	  $this->db->from('user_registration');
	  $this->db->where('registrationid',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function wantstomake($id)
    {
	  $this->db->select('*');
	  $this->db->from('useremails');
	  $this->db->where('useremailid',$id);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function updatenonprimary($id, $primaryemailid)
    {
	  $data = array(
			'useremail' => $primaryemailid,
			 );
	  $this->db->where('useremailid',$id);			
      $result = $this->db->update('useremails', $data);
    }
	
   function updateprimary($userregistrationid, $wantstomakepe)
    {
	  $data = array(
			'email' => $wantstomakepe,
			 );
	  $this->db->where('registrationid',$userregistrationid);			
      $result = $this->db->update('user_registration', $data);
    }
	
	function updateprimaryuserlogin($userregistrationid, $wantstomakepe)
    {
	  $data = array(
			'email' => $wantstomakepe,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_login', $data);
    }
	
	function updateprimaryuserdetail($userregistrationid, $wantstomakepe)
    {
	  $data = array(
			'email' => $wantstomakepe,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_detail', $data);
    }
	
	public function deletephoneno($id)
	{
	
		$this->db->where('phoneid',$id);	
		$this->db->delete('userphoneno');
	}
	
	function getprimaryphoneno($userregistrationid)
    {
	  $this->db->select('*');
	  $this->db->from('user_detail');
	  $this->db->where('userid',$userregistrationid);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function wantstomakeph($id)
    {
	  $this->db->select('*');
	  $this->db->from('userphoneno');
	  $this->db->where('phoneid',$id);
	  $query = $this->db->get();
	  return $query->result_array();
    }
	
	function updatenonprimaryph($id, $primaryphoneno)
    {
	  $data = array(
			'phoneno' => $primaryphoneno,
			 );
	  $this->db->where('phoneid',$id);			
      $result = $this->db->update('userphoneno', $data);
    }
	
	function updateprimaryph($userregistrationid, $wantstomakephpk)
    {
	  $data = array(
			'phone' => $wantstomakephpk,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('user_detail', $data);
    }
	
	function updateinfo($userregistrationid, $updatedinfo)
    {
	  $data = array(
			'reminder_type' => $updatedinfo,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('notification_type', $data);
    }
	
	function updatecommunication($userregistrationid, $keepyou)
    {
	  $data = array(
			'contact_type' => $keepyou,
			 );
	  $this->db->where('userid',$userregistrationid);			
      $result = $this->db->update('notification_type', $data);
    }
	
	function updatecrownsetting($userregistrationid, $seecrown)
    {
	  $data = array(
			'isActive' => $seecrown,
			 );
	  $this->db->where('user_id',$userregistrationid);			
      $result = $this->db->update('user_crown', $data);
    }
	
	function updatecommentsetting($userregistrationid, $seecomment)
    {
	  $data = array(
			'is_active' => $seecomment,
			 );
	  $this->db->where('user_id',$userregistrationid);			
      $result = $this->db->update('comment', $data);
    }
	
	function updatestatussetting($userregistrationid, $seestatus)
    {
		$imagenull = '';
	    $data = array(
			'is_active' => $seestatus,
			 );
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('image', $imagenull);			
      $result = $this->db->update('user_post', $data);
    }
	
	function getuserpost($userregistrationid)
	{
	  $imagenull = '';
	  $this->db->select('*');
	  $this->db->from('user_post');
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('image',$imagenull);
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function postimagenotnull($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('user_post');
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('image !=', ''); 
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function updatephotossetting($userregistrationid, $seephotos)
    {
	    $data = array(
			'is_active' => $seephotos,
			 );
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('image !=', '');			
      $result = $this->db->update('user_post', $data);
    }
	
	function getuserpostimage($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('user_post');
	  $this->db->where('user_id',$userregistrationid);
	   $this->db->where('image !=', '');	
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function updatefollowersetting($userregistrationid, $seefollower)
    {
	    $data = array(
			'is_active' => $seefollower,
			 );
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('follower_id !=', '0');			
      $result = $this->db->update('following', $data);
    }
	
	function getfollower($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('following');
	  $this->db->where('user_id',$userregistrationid);
	   $this->db->where('follower_id !=', '0');	
	  $query = $this->db->get();
	  return $query->result_array();
	}

        function getbrand($userregistrationid)
	{
	  $this->db->select('*');
	  $this->db->from('following');
	  $this->db->where('user_id',$userregistrationid);
	   $this->db->where('brand_id !=', '0');	
	  $query = $this->db->get();
	  return $query->result_array();
	}
	
	function updatebrandsetting($userregistrationid, $seebrand)
    {
	    $data = array(
			'is_active' => $seebrand,
			 );
	  $this->db->where('user_id',$userregistrationid);
	  $this->db->where('brand_id !=', '0');			
      $result = $this->db->update('following', $data);
    }


	public function checkemailid($addemail, $userregistrationid)
	{
	//echo 'hi'; exit;
		$this->db->select('*');
		$this->db->from('user_registration');
		$this->db->where('email',$addemail);
		$this->db->where('registrationid', $userregistrationid);
		$query = $this->db->get();
		// print_r($query); exit;
	  	return $query->result_array();
	}
	public function checkemailidreg($addemail, $userregistrationid)
	{
	//echo 'hi'; exit;
		$this->db->select('*');
		$this->db->from('useremails');
		$this->db->where('useremail',$addemail);
		$this->db->where('userid', $userregistrationid);
		$query = $this->db->get();
		// print_r($query); exit;
	  	return $query->result_array();
	}
	
	
}
?>