<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Scrollmodel extends CI_Model{
 
    public function getCountry($page){
        $offset =4*$page;
        $limit = 4;


$sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` where `product`.`isActive`=1 limit $offset ,$limit";



/*
       $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `brand`.`brandId`, `product`.`created_on`,`brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`

  limit $offset ,$limit";

*/
        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    public function getcat_product($page,$category_id){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `category`.`categoryId` = $category_id and `product`.`isActive`=1 limit $offset ,$limit";
//echo $sql;

        $result = $this->db->query($sql)->result_array();
     //print_r()
        return $result;
    
    
    }
	public function getsubcat_product($page,$subcategory_id){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `sub_category`.`subCategoryId` = $subcategory_id and `product`.`isActive`=1 limit $offset ,$limit";
//echo $sql;

        $result = $this->db->query($sql)->result_array();
     
        return $result;
    
    
    }
	public function getbrand_product($page,$brand_id){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`,`brand`.`brand_image`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `product`.`brandId_Fk` = $brand_id and  `product`.`isActive`=1 limit $offset ,$limit";
//echo $sql;

        $result = $this->db->query($sql)->result_array();
     
        return $result;
    
    
    }
    public function searchbrand_product($page,$searchdata,$check){
    if($check=='limit'){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql = "SELECT *
FROM `brand`
WHERE `brand`.`brand_Name` LIKE '%$searchdata%' limit $offset ,$limit";
//echo $sql;

        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    else{
    
    $sql = "SELECT *
	FROM `brand`
	WHERE `brand`.`brand_Name` LIKE '%$searchdata%' limit 5,200";
	//echo $sql;

        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    
    }
    
    public function search_product($page,$searchdata,$check){
     if($check=='limit'){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `product`.`product_Name` LIKE '%$searchdata%' limit $offset ,$limit";


        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    else
    {
    $sql = "SELECT `product`.`productId`, `product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk` 
WHERE `product`.`product_Name` LIKE '%$searchdata%' and  `product`.`isActive`=1 limit 5,200";


        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    
    }
	 public function search_people($page,$searchdata,$check){
	 if($check=='limit'){
    $offset =4*$page;
        $limit = 4;
//echo $category_id;

$sql =  "SELECT `user_registration`.`registrationid`,`user_registration`.`firstname`,`user_registration`.`lastname`,`user_registration`.`username`,`user_registration`.`profile_picture`,`user_detail`.`occupation`
FROM `user_registration`
JOIN `user_detail` ON `user_detail`.`userid` = `user_registration`.`registrationid`
WHERE `user_registration`.`firstname` LIKE '%$searchdata%' OR `user_registration`.`lastname` LIKE '%$searchdata%' limit $offset ,$limit";


        $result = $this->db->query($sql)->result_array();
     
        return $result;
    }
    else{
    $sql =  "SELECT `user_registration`.`registrationid`,`user_registration`.`firstname`,`user_registration`.`lastname`,`user_registration`.`username`,`user_registration`.`profile_picture`,`user_detail`.`occupation`
FROM `user_registration`
JOIN `user_detail` ON `user_detail`.`userid` = `user_registration`.`registrationid`
WHERE `user_registration`.`firstname` LIKE '%$searchdata%' OR `user_registration`.`lastname` LIKE '%$searchdata%' limit $offset ,$limit";


        $result = $this->db->query($sql)->result_array();
     
        return $result;
    
    }
    }
   
}