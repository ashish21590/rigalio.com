<?php

class Upload extends CI_Controller
{

    public function index()
    {
        $this->load->view('upload.php');

    }


    public function upload_file()

    {

        $this->load->helper('func');

        $config['upload_path'] = './content/';
        $config['allowed_types'] = 'gif|jpg|png|doc|txt|JPG|PNG';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            //$status = 'error';
            // echo $msg = $this->upload->display_errors('', '');

            $data = $this->upload->data();
            $this->load->model('Files_Model');
            $this->load->model('Getdata');
            $this->load->model('Ajaxrequest');
            $userid = $this->input->post('user_id');
            $post_data = $this->input->post('post');
            $visibility = $this->input->post('visibility');
            if(!$visibility){
                $visibility=1;
            }
           // $file_id = $this->Files_Model->insert_file($data['file_name'], $post_data,$visibility);
            //$image_path=$this->Files_Model->get_image($file_id);
            $dat['post_id'] = $this->Ajaxrequest->user_post_insert_wimg($post_data, $userid,$visibility);
            //echo $data['post_id'];
            $dat['user_post'] = $this->Getdata->get_user_post($dat['post_id']);
            $dat['user_details'] = $this->Getdata->user_info($userid);


            //$dat['user_details']=$this->Getdata->user_info($file_id);
            //print_r($data);
            if ($dat['post_id'] > 0) {
                //echo "1";
                $this->load->view('pages/user_post_status.php', $dat);
            } else {
                echo "0";

            }

        } else {

//echo "Hello";
            $data = $this->upload->data();
            $this->load->model('Files_Model');
            $this->load->model('Getdata');
            $this->load->model('Ajaxrequest');
            $userid = $this->input->post('user_id');
            $post_data = $this->input->post('post');
         echo    $visibility = $this->input->post('visibility');

            $file_id = $this->Files_Model->insert_file($data['file_name'], $post_data,$visibility);
            $image_path = $this->Files_Model->get_image($file_id);

//echo $data['file_name'];
            $dat['post_id'] = $this->Ajaxrequest->user_post_insert($post_data, $userid, $data['file_name'],$visibility);
            //echo $data['post_id'];
            $dat['user_post'] = $this->Getdata->get_user_post($dat['post_id']);
            $dat['user_details'] = $this->Getdata->user_info($userid);
            // print_r($dat);
            //print_r($image_path);
            if ($file_id) {
                $status = "success";
                $msg = "File successfully uploaded";
                $image = $image_path[0]['filename'];
                //$dat['user_details']=$this->Getdata->user_info($file_id);
                //print_r($data);
                if ($dat['post_id'] > 0) {
                    //echo "1";
                    $this->load->view('pages/user_post_status.php', $dat);
                } else {
                    echo "0";

                }
            } else {
                unlink($data['full_path']);
                $status = "error";
                $msg = "Something went wrong when saving the file, please try again.";
            }
        }
        // @unlink($_FILES[$file_element_name]);
    }
    // echo json_encode(array('status' => $status, 'msg' => $msg,'image'=> $image));
}
