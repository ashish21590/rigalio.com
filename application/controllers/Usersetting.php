<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/facebook/facebook.php';
class Usersetting extends CI_Controller {

			 public function __construct(){
				    parent::__construct();
				    $this->load->helper('url_helper');
                    $this->load->helper('form');
                    $this->load->library('form_validation');
					$this->load->model('settingmodel');
				    $this->load->library('session');  //Load the Session
					//$this->config->load('facebook');
					//$this->output->enable_profiler(True );
					//$this->output->cache(5); //Load the facebook.php file which is located in config directory
			    }
				
		     public function setting(){
			  
			       $this->load->model('getdata');
				   $this->load->model('settingmodel');
			  	   $data['seo']=$this->getdata->seo('page','usersetting');
				   $data['products']=$this->getdata->getall();
			  	   $data['all_cat']=$this->getdata->get_all_category();
				   $data['avali_cat']=$this->getdata->availcat();
				   $userregistrationid = $this->session->userdata('registrationid');
				  // echo $userregistrationid;
				  // $userregistrationid = $this->session->userdata('registrationid');
				  if($userregistrationid != '')
				  {
				   $data['userdata'] = $this->settingmodel->getuserdata($userregistrationid);
				   $data['getuserdataall'] = $this->settingmodel->getuserdataall($userregistrationid);
				   $data['userpic'] = $this->settingmodel->getuserpic($userregistrationid);
				    $data['usercrown'] = $this->settingmodel->getusercrown($userregistrationid);
				   $data['usercomment'] = $this->settingmodel->getusercomment($userregistrationid);
				   // print_r($data['usercomment']);  exit;
				   $data['userpost'] = $this->settingmodel->getuserpost($userregistrationid);
				   $data['getuserpostimage'] = $this->settingmodel->getuserpostimage($userregistrationid);
				   $data['getfollower'] = $this->settingmodel->getfollower($userregistrationid);
                    $data['getbrand'] = $this->settingmodel->getbrand($userregistrationid);
				   //print_r($data['getfollower']);  exit;
				   //print_r($getuserpostimage['is_active']); exit;
				   $data['useremail'] = $this->settingmodel->alluseremail($userregistrationid);
				   $data['userphoneno'] = $this->settingmodel->alluserphoneno($userregistrationid); 
				 // print_r($data['useremail']); exit;
                    $this->load->view('pages/head.php',$data);
			 	   $this->load->view('pages/usersetting.php' ,$data);
			 	   $this->load->view('pages/footer.php');
				  }
				  else{
redirect(base_url());
					  }
  		  }	
		  
		  public function userpassexist(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $currentpass = $this->input->post('currentpass');
			 $password = $this->settingmodel->passexist($currentpass, $userregistrationid);
			 if(count($password) == 0)
			 {
			 echo '0';
			 }
			 else
			 { 
			 echo '1';
			 }
  		 }
		 
		public function passwordchange(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $newpass = $this->input->post('newpass');
			 $newpassword = $this->settingmodel->passupdate($newpass, $userregistrationid);
//redirect('main/logout');

echo "logout";
  		 }
		 
		 public function dobchange(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $newdob = $this->input->post('newdob');
			 $newpassword = $this->settingmodel->dobupdate($newdob, $userregistrationid);
  		 }
		 
		 public function addphoneno(){
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $phoneexist = $this->settingmodel->phonenoexist($userregistrationid);
			 $addphone = $this->input->post('addphone');
			if($phoneexist[0]['phone']=='')
			{
			 $addphoneno = $this->settingmodel->addphoneno($addphone, $userregistrationid);
			// echo '0';
			}
			else{
			$addmorephoneno = $this->settingmodel->addmorephoneno($addphone, $userregistrationid);
			//echo '1';	
			}
		 }
		 
		 public function addemail(){
			$this->load->model('settingmodel');
			$userregistrationid = $this->session->userdata('registrationid');
			$addemail = $this->input->post('addemail');
			$checkemail=$this->settingmodel->checkemailid($addemail, $userregistrationid);
			$checkemail1=$this->settingmodel->checkemailidreg($addemail, $userregistrationid);
			//print_r($checkemail); print_r($checkemail1); exit;
			if(count($checkemail)==0 && count($checkemail1)==0){
			$addmoreemail = $this->settingmodel->addmoreemail($addemail, $userregistrationid);
				echo "0";
			}
			else{
			echo "1";
			
			}
			//echo '1';	
		 }
		 
		public function deleteemail()
		{
		$id = $this->uri->segment(3);
		$this->load->model('settingmodel');
		$this->settingmodel->deleteemailid($id);
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function makeprimary()
		{
		 $this->load->model('settingmodel');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $primaryemailiddata = $this->settingmodel->getprimaryemail($userregistrationid);
		 $primaryemailid = $primaryemailiddata[0]['email']; 
		 $id = $this->uri->segment(3);
		 $wantstomake = $this->settingmodel->wantstomake($id);
		 $wantstomakepe = $wantstomake[0]['useremail']; 
		 $updatenonprimary = $this->settingmodel->updatenonprimary($id, $primaryemailid);
		 $updateprimary = $this->settingmodel->updateprimary($userregistrationid, $wantstomakepe);
		 $updateprimaryuserlogin = $this->settingmodel->updateprimaryuserlogin($userregistrationid, $wantstomakepe);
		 $updateprimaryuserdetail = $this->settingmodel->updateprimaryuserdetail($userregistrationid, $wantstomakepe);
		 redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function deletephoneno()
		{
		$id = $this->uri->segment(3);
		$this->load->model('settingmodel');
		$this->settingmodel->deletephoneno($id);
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function makeprimaryphone()
		{
		 $this->load->model('settingmodel');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $primaryphonenodata = $this->settingmodel->getprimaryphoneno($userregistrationid);
		 $primaryphoneno = $primaryphonenodata[0]['phone']; 
		 //echo $primaryphoneno;
		 $id = $this->uri->segment(3);
		 $wantstomakeph = $this->settingmodel->wantstomakeph($id);
		 $wantstomakephpk = $wantstomakeph[0]['phoneno']; 
		 //echo $wantstomakephpk; exit;
		 $updatenonprimaryph = $this->settingmodel->updatenonprimaryph($id, $primaryphoneno);
		 $updateprimaryph = $this->settingmodel->updateprimaryph($userregistrationid, $wantstomakephpk);
		 redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function updateinfo()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$updatedinfo = $this->input->post('updatedinfo');
		$updateinfodata = $this->settingmodel->updateinfo($userregistrationid, $updatedinfo);
		//echo $updatedinfo; 
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function updatecommunication()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$keepyou = $this->input->get('keepyou');
		$updatecommunication = $this->settingmodel->updatecommunication($userregistrationid, $keepyou);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseecrown()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seecrown = $this->input->post('seecrown');
		$updatecommunication = $this->settingmodel->updatecrownsetting($userregistrationid, $seecrown);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseecomment()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seecomment = $this->input->post('seecomment');
		$updatecomment = $this->settingmodel->updatecommentsetting($userregistrationid, $seecomment);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseestatus()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seestatus = $this->input->post('seestatus');
		$updatestatus = $this->settingmodel->updatestatussetting($userregistrationid, $seestatus);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseephotos()
		{
		$this->output->enable_profiler(True );
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seephotos = $this->input->post('seephotos');
		//$imagenotnulldata = $this->settingmodel->postimagenotnull($userregistrationid);
		//print_r($me); exit;
		//if(count($imagenotnulldata)>0)
		//{
		//echo count($me); exit;
		$updatephotos = $this->settingmodel->updatephotossetting($userregistrationid, $seephotos);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseefollower()
		{
		$this->output->enable_profiler(True );
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seefollower = $this->input->post('seefollower');
		$updatefollower = $this->settingmodel->updatefollowersetting($userregistrationid, $seefollower);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}

                public function canseebrand()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seebrand = $this->input->post('seebrand');
		$updatebrand = $this->settingmodel->updatebrandsetting($userregistrationid, $seebrand);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}

              public function logout(){
			$base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
			$this->session->sess_destroy();  //session destroy
			redirect($base_url);  //redirect to the home page
	      }
		
		
		 
		 
}
?>