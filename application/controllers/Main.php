<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/facebook/facebook.php';

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');  //Load the Session
        $this->config->load('facebook');
        $this->load->library('pagination');
        //$this->output->enable_profiler(True );
        //$this->output->cache(5); //Load the facebook.php file which is located in config directory
    }

    public function index()
    {
        $this->session->unset_userdata('inviteuserid');
        $userregistrationid = $this->session->userdata('registrationid');

        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();
        $data['all_products_count'] = $this->getdata->record_count();

        //print_r($data['avali_cat']); exit;
        $data['slider_image'] = $this->getdata->slider('page', 'main');
        $data['seo'] = $this->getdata->seo('page', 'home');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //print_r($data);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['heading'] = "See whats new here !";
        $this->load->view('pages/head.php', $data);//passing category in the head section
        $this->load->view('pages/index.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function category()
    {
        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['category'] = $this->getdata->getcategory();
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['heading'] = "See whats new here !";
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/category.php');
        $this->load->view('pages/footer.php');
    }

    public function singlecat($category_Name, $category_id)
    {

        //$category_id = $this->uri->segment(3, 0);
        $mycat = $category_id;
        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('categoryid_fk', $category_id);
        $data['slider_image'] = $this->getdata->slider('page', 'category');
        $data['products'] = $this->getdata->category_wiseproduct($category_id);
        $data['cat_count'] = $this->getdata->category_product_count($category_id);
        $data['category'] = $this->getdata->category_incat($category_id);
        if (empty($data['category'])) {
            redirect(base_url());
        }
        $errors = array_filter($data['products']);
        //$caterrors = array_filter($data['category']);
        if (!empty($errors)) {

            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['avali_cat'] = $this->getdata->availcat();
            $data['sub_category'] = $this->getdata->subcategory($category_id);
            $data['avail_sub_category'] = $this->getdata->get_avail_subcategory($category_id);
            $userregistrationid = $this->session->userdata('registrationid');
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $data['cat'] = $mycat;
            //print_r($cat);
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/category', $data);
            $this->load->view('pages/footer.php');
        } else {
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['avali_cat'] = $this->getdata->availcat();
            $userregistrationid = $this->session->userdata('registrationid');
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $data['sub_category'] = $this->getdata->subcategory($category_id);
            $data['heading'] = "There is no product available in this category !";


            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/category', $data);
            $this->load->view('pages/footer.php');
        }

    }

    public function subcategory($subCategory_Name, $subcategory_id)
    {

        //$subcategory_id = $this->uri->segment(3, 0);
        $mysubcat = $subcategory_id;
        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('subcategoryId_Fk', $subcategory_id);
        $data['slider_image'] = $this->getdata->slider('page', 'subcategory');
        $data['products'] = $this->getdata->subcategory_wiseproduct($subcategory_id);
        $data['category'] = $this->getdata->category_onsubcat($subcategory_id);
        $data['subcat_count'] = $this->getdata->subcategory_product_count($subcategory_id);
        $data['subcat'] = $mysubcat;
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        if (empty($data['category'])) {
            redirect(base_url());
        }
        $errors = array_filter($data['products']);
        if (!empty($errors)) {
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //$data['sub_category']=$this->getdata->subcategory($category_id);
            $data['avali_cat'] = $this->getdata->availcat();
            $data['heading'] = "CHECK OUT WHAT'S NEW AND NEWS !";
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/subcategory', $data);
            $this->load->view('pages/footer.php');
        } else {
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //$data['sub_category']=$this->getdata->subcategory($category_id);
            $data['avali_cat'] = $this->getdata->availcat();
            $userregistrationid = $this->session->userdata('registrationid');
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $data['heading'] = "There is no product available in this category !";
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/subcategory', $data);
            $this->load->view('pages/footer.php');
        }

    }

    public function brand($brandname, $brand_id)
    {

        //$brand_id = $this->uri->segment(3, 0);
        $mybrand = $brand_id;
        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('brandid_fk', $brand_id);
        print_r($data);
        $data['products'] = $this->getdata->brand_wise($brand_id);
        $data['brand_details'] = $this->getdata->brand($brand_id);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['avali_cat'] = $this->getdata->availcat();
        $data['brand_count'] = $this->getdata->brand_product_count($brand_id);
        $data['brand'] = $mybrand;
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['management_details'] = $this->getdata->management_view($brand_id);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/brand.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function product($product_Name, $productid, $tab)
    {


        $config = array();
        $config["base_url"] = base_url() . "product/$product_Name";
        $config["total_rows"] = $this->getdata->record_count() + 1;
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;
        //$choice = $config["total_rows"] / $config["per_page"];
        //$config["num_links"] = round($choice);
        $config['display_pages'] = FALSE;
        $config['next_link'] = '<div class="next-cat">Next <i class="fa fa-angle-right"></i></div> ';

        $config['prev_link'] = '<div class="prev-cat"><i class="fa fa-angle-left"></i>Previous </div> ';
        $config['first_link'] = '';
        $config['last_link'] = '';

        $this->pagination->initialize($config);

        if (empty($productid)) {
            $productid = 1;
        }

        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('productid_fk', $productid);        //	for the seo purpose//
        $data['products'] = $this->getdata->product($productid);
        $data['prod_highlight'] = $this->getdata->product_highlight($productid);
        $data['prod_image'] = $this->getdata->product_image($productid);
        $data['prod_cat'] = $this->getdata->product_catgory($productid);
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['all_products_count'] = $this->getdata->record_count();
        if ($data['prod_cat']) {

            $cat_id = $data['prod_cat'][0]['categoryId_Fk'];
            $brand_id = $data['prod_cat'][0]['brandId_Fk'];
            $subcat_id = $data['prod_cat'][0]['subcategoryId_Fk'];
            $data['cat_name'] = $this->getdata->get_category_name($cat_id);
            $data['brand_name'] = $this->getdata->get_brand_name($brand_id);
            $data['subcat_name'] = $this->getdata->get_subcategory_name($subcat_id);
            $this->load->model('ajaxrequest');
            $data['all_comments'] = $this->ajaxrequest->all_comments($productid);
            $data['allproduct'] = $this->getdata->getall();
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section

            $data['links'] = $this->pagination->create_links();
            $data['avali_cat'] = $this->getdata->availcat();
            $userregistrationid = $this->session->userdata('registrationid');
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/product.php', $data);
            $this->load->view('pages/footer.php');
        } else {

            redirect(base_url());
        }

    }

    //public function search($serach_text){
//
//					$data['result']=$serachtext;
//					$data['heading']="Your search result !";
//					$this->load->model('getdata');
//					$data['seo']=$this->getdata->seo('page','search');
//					$data['all_cat']=$this->getdata->get_all_category();//for getting all the category in the head section
//					$data['search_result']=$this->getdata->search($serachtext);
//					$this->load->view('pages/head.php',$data);
//					$this->load->view('pages/search.php',$data);
//					$this->load->view('pages/footer.php');
//
//			}

    public function search()
    {

        //$data['result']=$serachtext;
        $searchdata = $this->input->post('serach_text');
        //print_r($data); exit;
        if ($searchdata != '') {
            $data['heading'] = "Your search result !";
            $this->load->model('getdata');
            $data['seo'] = $this->getdata->seo('page', 'search');
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['avali_cat'] = $this->getdata->availcat();
            $userregistrationid = $this->session->userdata('registrationid');
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            //print_r($data);
            $data['brand'] = $searchdata;
            $data['product'] = $searchdata;
            $data['people'] = $searchdata;
            $data['search_productresult'] = $this->getdata->searchproduct($searchdata);
            $data['prod_count'] = $this->getdata->prod_count($searchdata);
            $data['search_brandresult'] = $this->getdata->searchbrand($searchdata);
            $data['brand_count'] = $this->getdata->brand_count($searchdata);
            $data['people_count'] = $this->getdata->people_count($searchdata);
            $data['search_people'] = $this->getdata->searchpeople($searchdata);
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/search.php', $data);
            $this->load->view('pages/footer.php');
        } else {
            redirect(base_url());


        }
    }


    function user_profile()
    {
//$this->output->cache(5);
        $this->load->model('getdata');
        $data['finest'] = $this->getdata->finest();
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        if (!$userregistrationid) {
            redirect(base_url());
        }
        $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
        if ($userregistrationid) {
            $data['seo'] = $this->getdata->seo('page', 'profile');
            //$data['mytimeline']=$this->getdata->timeline_of($userregistrationid);
            $data['user_detail'] = $this->getdata->user_details($userregistrationid);
            $user_brands = $this->getdata->get_followed_brand($userregistrationid, 'brand_id');
            $user_category = $this->getdata->get_followed_category($userregistrationid, 'category_id');
            //print_r($user_brands);
            // --------For the user friends and relastionship status ---------/////
            $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);
            // print_r($user_following_friends_id);
            $all_comment = $this->getdata->get_comment_friends_all($user_following_friends_id); //getting comments of the friends following and getting all the data related to the friends
            //print_r($$all_comment); exit;
            $all_crowns = $this->getdata->get_crowns_friends_all($user_following_friends_id);//getting user crowned products and the post and related data
            $all_posts = $this->getdata->get_posts__friends_all($user_following_friends_id); // getting all the post which the user friends have posted and wish to share them on public.

            $myall_posts = [];

            for ($i = 0; $i < count($all_posts); $i++) {

                for ($j = 0; $j < count($all_posts[$i]); $j++) {


                    $myall_posts[] = $all_posts[$i][$j];
                }

            }

            $myall_comments = [];

            for ($i = 0; $i < count($all_comment); $i++) {

                for ($j = 0; $j < count($all_comment[$i]); $j++) {


                    $myall_comments[] = $all_comment[$i][$j];
                }

            }

            $myall_crowns = [];
            $mykeys = [];

            for ($i = 0; $i < count($all_crowns); $i++) {

                for ($j = 0; $j < count($all_crowns[$i]); $j++) {

                    if (!in_array($all_crowns[$i][$j]['productId'], $mykeys)) {
                        $mykeys[] = $all_crowns[$i][$j]['productId'];

                        $myall_crowns[] = $all_crowns[$i][$j];


                    }


                }

            }


            //print_r($myall_crowns); exit;
            $myarr = array_merge($user_brands, $user_category);
            $user_data = $this->getdata->get_all_products($myarr);
            //print_r($user_data);
            //print_r($all_posts);

            //print_r($myarr); exit;

            $arr = [];
//$a=0;
            foreach ($user_data as $value) {
//print_r($value);
                $arr[] = $value[0];
                //$a++;
            }
            //print_r($arr); exit;
            //print_r($all_crowns);
            //print_r($all_crowns);
            $result_all_home = array_merge($myall_crowns, $myall_posts, $myall_comments, $arr);
            $myclean_posts = [];
            $mykeys = [];
            $mypost_keys = [];
            $myclean_posts_post = [];
            foreach ($result_all_home as $myclean) {

                if (array_key_exists('post_id', $myclean)) {

                    if (!in_array($myclean['post_id'], $mykeys)) {
                        $mykeys[] = $myclean['post_id'];

                        $myclean_posts[] = $myclean;


                    }


                } else {


                    if (!in_array($myclean['productId'], $mypost_keys)) {
                        $mypost_keys[] = $myclean['productId'];

                        $myclean_posts_post[] = $myclean;


                    }

                }
            }


//print_r($myclean_posts_post); exit;


            $result_all_home = array_merge($myclean_posts, $myclean_posts_post);


            //print_r($all_posts);
            function compare_lastname($a, $b)
            {
                return strnatcmp($b['date_time'], $a['date_time']);
            }

            // sort alphabetically by name
            usort($result_all_home, 'compare_lastname');

            //print_r($myarr1); exit;

            $data['user_data1'] = $result_all_home;
            //print_r($result_all_home); exit;


            $myarr = array_merge($user_brands, $user_category);
            //print_r($myarr);
            $myarr1 = [];
            $sr = 0;
            //array  result for the user follow category and brand
            if (count($myarr) > 0) {
                //for showing homepage data
                //$data['user_data']=$this->getdata->get_all_products($myarr);
            } else {
                //$data['user_data']="";
            }
            //ends here array  result for the user follow category and brand
            //$data['user_category']=$this->getdata->user_details($userregistrationid,'category_id');
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //for the timeline part
            //$this->load->model('getdata');

            //for getting all comments of the user
            $user_comments = $this->getdata->get_comment_all($userregistrationid);
            //$user_posts=$this->getdata->get_posts_all('1');
            $user_crowns = $this->getdata->get_crowns_all($userregistrationid);

            $user_posts = $this->getdata->get_posts_all($userregistrationid);
            //$data['user_following']=$this->getdata->get_following_all($user_id);
            $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
            $me;
            foreach ($data as $value) {
                $me = $value;
            }
            //print_r($me);
            function sksort(&$array, $subkey = "id", $sort_ascending = false)
            {

                if (count($array))
                    $temp_array[key($array)] = array_shift($array);

                foreach ($array as $key => $val) {
                    $offset = 0;
                    $found = false;
                    foreach ($temp_array as $tmp_key => $tmp_val) {
                        if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                            $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                                array($key => $val),
                                array_slice($temp_array, $offset)
                            );
                            $found = true;
                        }
                        $offset++;
                    }
                    if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
                }

                if ($sort_ascending) $array = array_reverse($temp_array);

                else $array = $temp_array;
            }

            if (count($me) > 0) {
                sksort($me, "date_time");
                $data['user_val'] = $me;

                //print_r($data);
            }

            //timeline part ends here
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            //$this->load->view('pages/profile.php',$data);
            //if(count($me)>0&&count($myarr)>0){
            if (count($me) > 0 && count($myarr) > 0) {
                //echo "going here ";
                $this->load->view('pages/profile1.php', $data);
            } else {
                $this->load->view('pages/profile1.php', $data);
            }

        } else {

            echo "Login to visit your profile";
            //$this->load->view('pages/footer.php');
        }
    }

    public function scroll()
    {
        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall_scroll("0");
        $data['seo'] = $this->getdata->seo('page', 'home');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //print_r($data);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['heading'] = "See whats new here !";
        $this->load->view('pages/head.php', $data);//passing category in the head section
        $this->load->view('pages/index_scroll.php', $data);
        //$this->load->view('pages/footer.php');

    }

    public function new_scroll()

    {

        echo $page = $this->uri->segment(3, 0);
        $this->load->model('getdata');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        if ($page != '') {
            $data['products'] = $this->getdata->getall_scroll($page);
        } else {
            $data['products'] = $this->getdata->getall_scroll("0");
        }
        $data['seo'] = $this->getdata->seo('page', 'home');
        //print_r($data);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['heading'] = "See whats new here !";
        //$this->load->view('pages/head.php',$data);//passing category in the head section
        $this->load->view('pages/sc.php', $data);
        //$this->load->view('pages/footer.php');
    }


    public function userexist()
    {

        $this->load->model('getdata');
        $username = $this->input->post('username');
        $alreadyusername = $this->getdata->alreadyuserexist($username);
        if (count($alreadyusername) == 0) {
            echo '0';
        } else {
            echo '1';
        }

    }

    public function inviteuserlogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->load->model('getdata');
        $invitecheck = $this->getdata->inviteusercheck($username, $password);
        if (count($invitecheck) == 0) {
            //echo '2'; exit;
            $logindatanew = $this->getdata->isactivenotfind($username, $password);
            //print_r($logindatanew);exit;
            if (count($logindatanew) > 0) {
                $userregid = $logindatanew[0]['userid'];
                $this->session->set_userdata('registrationid', $userregid);
                echo '1';
                //echo 'user exist';
            } else {
                echo '2';
                //echo 'user not exist';
            }
        } else {
            $isActive = $invitecheck[0]['isActive'];
            if ($isActive == 1) {
                $inviteuserid = $invitecheck[0]['invitelogin_id'];
                $idregistration = $this->getdata->aftersignupregid($inviteuserid);
                $registrationid = $idregistration[0]['registrationid'];
                //print_r($registrationid); exit;
                $logindata = $this->getdata->loginaftersignup($registrationid, $username, $password);
                //print_r($logindata); exit;
                if (count($logindata) > 0) {
                    $this->session->set_userdata('registrationid', $registrationid);
                    echo '3';
                    //echo 'username password match!!';
                } else {
                    echo '4';
                    //echo 'wrong username and password';
                }
            } else {
                //print_r($invitecheck);
                $inviteuserid = $invitecheck[0]['invitelogin_id'];
                $this->session->set_userdata('inviteuserid', $inviteuserid);
                echo '5';
                //echo 'please sign up !!!';
            }

        }
        //$data = count($invitecheck);
        //$this->load->view('pages/loginpage.php',$data);
        //print_r($data);exit;
        //echo $data['usercount']; exit;
        //if(count($invitecheck)>0){
//				echo  '1';
//				$inviteuserid =$invitecheck[0]['invitelogin_id'];
//				//echo $inviteuserid;
//				$this->session->set_userdata('inviteuserid',$inviteuserid);
//				 }
//				  else{
//					  $msg_already = "user already sign in";
//				    echo  '0';
//					  }
    }

    public function logout()
    {
        $base_url = $this->config->item('base_url'); //Read the baseurl from the config.php file
        $this->session->sess_destroy();  //session destroy
        redirect($base_url);  //redirect to the home page
    }

    public function query()
    {

        $saveddata = $this->getdata->insertquery();
        redirect('main/brand');
    }

    public function trynow()
    {

        $saveddata = $this->getdata->inserttrynow();
        redirect('main/product');
    }

    public function privacy()
    {

        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $data['seo'] = $this->getdata->seo('page', 'Privacy ');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/privacy.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function contactus()
    {

        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $data['seo'] = $this->getdata->seo('page', 'Contactus');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/contactus.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function contactussubmit()
    {

        $saveddata = $this->getdata->contactus();
        redirect('main/contactus');

    }

    public function signup()
    {

        $this->load->model('getdata');
        //echo $userinviteuserid; exit;
        //echo $userinviteuserid; exit;
        //$data=$this->getdata->checkuser($user_profile['email']);
        //$data['insertinviteuser_id'] = $this->getdata->inviteduser_insert();
        // echo $insertinviteuser_id; exit;
        $this->session->unset_userdata('registrationid');
        $data['seo'] = $this->getdata->seo('page', 'signup');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $data['country'] = $this->getdata->allcountry();
        $inviteuserid = $this->session->userdata('inviteuserid');
        if ($inviteuserid) {
            $this->load->view('pages/head.php', $data);
            $this->load->view('pages/signup.php', $data);
            $this->load->view('pages/footer.php');
        } else {
            redirect('main/');
        }
        //$data['inviteuserid']=$this->session->userdata('inviteuserid');
        //echo $inviteuserid ;  exit;
        //echo count($data); exit;
        //print_r($data['inviteuserdetail']);	  exit;

    }

    public function signupdata()
    {

        $this->load->model('getdata');
        $data['base_path'] = base_url();
        $config['upload_path'] = "uploads/profilepic/";
        $config['allowed_types'] = "gif|jpg|png|doc|txt|JPG|PNG";
        $config['max_size'] = "1024 * 8";
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $fullFilePath = null;
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $filedetails = $this->upload->data();
            $fullFilePath = "uploads/profilepic/" . $filedetails['file_name'];
            // $data['seo']=$this->getdata->seo('page','signup ');
            // $data['products']=$this->getdata->getall();
            // $data['all_cat']=$this->getdata->get_all_category();
            $userinviteid = $this->session->userdata('inviteuserid');
            $userintrest = $this->input->post('category');
            //echo($userintrest); exit;
            echo $notification = $this->input->post('keepyou');
            echo $userinsertid = $this->getdata->userregistration_signup($userinviteid, $fullFilePath);
            //echo $userinsertid;exit;
            echo $uploadpic = $this->getdata->uploadpic($fullFilePath, $userinsertid);
            echo $userintrestdata = $this->getdata->userluxry_intrest($userintrest, $userinsertid);
            echo $userintrestdata = $this->getdata->userfollowcategory($userintrest, $userinsertid);
            echo $usernotifydata = $this->getdata->user_notification($notification, $userinsertid);
            echo $userdetail = $this->getdata->user_detail($userinsertid);
            echo $userlogin = $this->getdata->user_login($userinsertid);
            echo $firstpost = $this->getdata->user_first_post($userinsertid);
            echo $this->getdata->updateuserinvite($userinviteid);
            echo $this->session->set_userdata('registrationid', $userinsertid);
            //redirect('main/userprofile');
        }
    }


    public function signupwithoutimage()
    {

        $this->load->model('getdata');
        // $data['seo']=$this->getdata->seo('page','signup ');
        // $data['products']=$this->getdata->getall();
        // $data['all_cat']=$this->getdata->get_all_category();
        $userinviteid = $this->session->userdata('inviteuserid');
        $userintrest = $this->input->post('category');
        //echo($userintrest); exit;
        echo $notification = $this->input->post('keepyou');
        echo $userinsertid = $this->getdata->userregistration_signupwoimage($userinviteid);
        //echo iiuuuuu$userinsertid;exit;
        //echo $uploadpic = $this->getdata->uploadpic($fullFilePath, $userinsertid);
        echo $userintrestdata = $this->getdata->userluxry_intrest($userintrest, $userinsertid);
        echo $userintrestdata = $this->getdata->userfollowcategory($userintrest, $userinsertid);
        echo $usernotifydata = $this->getdata->user_notification($notification, $userinsertid);
        echo $userdetail = $this->getdata->user_detail($userinsertid);
        echo $userlogin = $this->getdata->user_login($userinsertid);
        echo $firstpost = $this->getdata->user_first_post($userinsertid);
        echo $this->getdata->updateuserinvite($userinviteid);
        echo $this->session->set_userdata('registrationid', $userinsertid);
        //redirect('main/userprofile');

    }


    public function faq()
    {

        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('page', 'faq');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/faq.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function aboutus()
    {

        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('page', 'aboutus');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/aboutus.php', $data);
        $this->load->view('pages/footer.php');
    }

    public function userprofile()
    {

        $this->load->model('getdata');
        $data['seo'] = $this->getdata->seo('page', 'userprofile');
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        //echo $userinviteid;
        // $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //print_r($data['userdata']);
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/userprofile', $data);
        $this->load->view('pages/footer.php');
    }

    public function friend_list($friend_id)
    {
        //$userregistrationid="1";
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);

        if ($userregistrationid) {
            $this->load->model('getdata');
            $friend_id_follower = $this->getdata->follower_id($friend_id);
            $friend_id = $this->getdata->follower_id($friend_id);

            $data['friend_id'] = $friend_id;


            //checking if user are already friend
            $check = $this->getdata->friendship_status($userregistrationid, $friend_id[0]['registrationid']);

            $data['check'] = $check;
            $data['gallery_img'] = $this->getdata->get_gallery_image($friend_id[0]['registrationid']);


            $data['user_following_brands_sidebar'] = $this->getdata->following_brands_sidebar($friend_id[0]['registrationid'], 'brand_id');

            $data['user_detail'] = $this->getdata->user_details($friend_id[0]['registrationid']);
            $data['seo'] = $this->getdata->seo('page', 'profile');

            $user_comments = $this->getdata->get_comment_all($friend_id[0]['registrationid']);
            $user_posts = $this->getdata->get_posts_all($friend_id[0]['registrationid']);
            //$user_posts=$this->getdata->get_posts_all('1');
            $user_crowns = $this->getdata->get_crowns_all($friend_id[0]['registrationid']);
            //$data['user_following']=$this->getdata->get_following_all($user_id);
            $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
            //print_r($data);
            $me;
            foreach ($data as $value) {
                $me = $value;
            }
            //print_r($me); exit;
            function sksort(&$array, $subkey = "id", $sort_ascending = false)
            {

                if (count($array))
                    $temp_array[key($array)] = array_shift($array);

                foreach ($array as $key => $val) {
                    $offset = 0;
                    $found = false;
                    foreach ($temp_array as $tmp_key => $tmp_val) {
                        if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                            $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                                array($key => $val),
                                array_slice($temp_array, $offset)
                            );
                            $found = true;
                        }
                        $offset++;
                    }
                    if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
                }

                if ($sort_ascending) $array = array_reverse($temp_array);

                else $array = $temp_array;
            }

            sksort($me, "date_time");
            $data['products'] = $me;


            //print_r($me);
            //print_r($data);
            $data['heading'] = "See whats new here !";

            if (!$check) {

                $this->load->view('pages/head.php', $data);//passing category in the head section
                //$this->load->view('pages/profile.php',$data);
                $this->load->view('pages/lockedfriend.php', $data);

            }


//print_r($friend_id_follower); exit;
            $data['friend_id_follower'] = $friend_id_follower;
            $this->load->view('pages/head.php', $data);//passing category in the head section
            //$this->load->view('pages/profile.php',$data);
            $this->load->view('pages/friend.php', $data);
            //echo "user is friend";


            //echo "user logged in";


        } else {

            echo "Please login to visit user profiles !";
        }


    }

    public function user_timeline()
    {
        $this->load->model('getdata');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //for getting all comments of the user
        $user_comments = $this->getdata->get_comment_all('1');
        $user_posts = $this->getdata->get_posts_all('1');
        //$user_posts=$this->getdata->get_posts_all('1');
        $user_crowns = $this->getdata->get_crowns_all('1');
        //$data['user_following']=$this->getdata->get_following_all($user_id);
        $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
        $data['user_following_brands_sidebar'] = $this->getdata->following_brands_sidebar($userregistrationid, 'brand_id');
        //print_r($user_val); exit;
        $me;
        foreach ($data as $value) {
            $me = $value;
        }
        //print_r($me); exit;
        function sksort(&$array, $subkey = "id", $sort_ascending = false)
        {

            if (count($array))
                $temp_array[key($array)] = array_shift($array);

            foreach ($array as $key => $val) {
                $offset = 0;
                $found = false;
                foreach ($temp_array as $tmp_key => $tmp_val) {
                    if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                        $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                            array($key => $val),
                            array_slice($temp_array, $offset)
                        );
                        $found = true;
                    }
                    $offset++;
                }
                if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
            }

            if ($sort_ascending) $array = array_reverse($temp_array);

            else $array = $temp_array;
        }


        sksort($me, "date_time");

        print_r($me);


    }


    function gallery()
    {

        $userregistrationid = $this->session->userdata('registrationid');
        $data['seo'] = $this->getdata->seo('page', 'gallery');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $this->load->view('pages/head.php', $data);
        $data['gallery_images'] = $this->getdata->get_user_gallery($userregistrationid);
        $this->load->view('pages/gallery.php', $data);


    }

    function test()
    {

        $userregistrationid = $this->session->userdata('registrationid');
        $data['seo'] = $this->getdata->seo('page', 'gallery');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['products'] = $this->getdata->getall();
        $data['all_cat'] = $this->getdata->get_all_category();
        $data['avali_cat'] = $this->getdata->availcat();
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/abcde.php', $data);

    }

    public function viewbrand()
    {
        $userregistrationid = $this->session->userdata('registrationid');
        if (!empty($userregistrationid)) {
            $this->load->model('getdata');
            $data['seo'] = $this->getdata->seo('page', 'viewbrand');
            $data['userbrand'] = $this->getdata->userbrand($userregistrationid);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //$data['heading']="See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            $this->load->view('pages/view-brands.php', $data);
        } else {
            redirect(base_url());
        }
    }

    public function myfollowers()
    {

        $this->load->model('getdata');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);//for getting the following friends id
        $user_followers = $this->getdata->get_followers_id($userregistrationid);
        //print_r($user_followers);
        $user_pending_id = $this->getdata->get_pending_id($userregistrationid);

        //print_r($user_pending_id); exit;

        $data['followers'] = $this->getdata->get_all_followers($user_followers); //person
        $data['followings'] = $this->getdata->get_all_friends($user_following_friends_id);
        $data['notification'] = $this->getdata->get_all_notification($user_pending_id);
        //print_r($data);
        $data['seo'] = $this->getdata->seo('page', 'followers');
        $this->load->view('pages/head.php', $data);
        $this->load->view('pages/followers.php', $data);


    }






    public function mynotification()
    {

        $userregistrationid = $this->session->userdata('registrationid');
        //$userregistrationid=5;
        //$data['user_info']=$this->getdata->my_posts_notifcations($userregistrationid);
        $user_notification = $this->getdata->collect_notification();
        $new_notifications = [];
        foreach ($user_notification as $value) {
            if ($value['owner_id'] == $userregistrationid) {
                //echo "Yes notification exist";
                $new_notifications[] = $value;
            }
        }
        $data['notify'] = $new_notifications;

        $this->load->view('notifications.php', $data);


    }

//for getting comments on the post

    public function get_comments()
    {
        $post_id = $this->input->post('post_id');
        $this->load->model('getdata');
        $comments = $this->getdata->get_comments($post_id);
        $data['comments'] = $this->getdata->get_comments($post_id);

         $count_rows = count($comments);


        if ($count_rows > 0) {
            //print_r($comments);
            $this->load->view('pages/load_comment.php', $data); exit;
        } else {
            echo "No comments on this post !"; exit;
        }

    }

    function finestatrigalio()
    {

        //$user_id= $this->input->post('user_id');
        //$subcat_id=$this->input->post('subcat_id');
        $this->load->model('getdata');


        $userregistrationid = $this->session->userdata('registrationid');

        if (!$userregistrationid) {
            redirect(base_url());
        }
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);


        //$data['products']=$this->getdata->getall();
        //$data['slider_image']=$this->getdata->slider('page', 'main');
        $data['seo'] = $this->getdata->seo('page', 'home');
        $data['all_products_count'] = $this->getdata->record_count();
        //print_r($data);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['heading'] = "See whats new here !";
        $this->load->view('pages/head.php', $data);//passing category in the head section
        $this->load->model('ajaxrequest');
        $data['collections'] = $this->getdata->finest();
        $this->load->view('pages/finestatrigalio.php', $data);

    }

    public function updateprofilepic()
    {

        $this->load->model('getdata');
        $data['base_path'] = base_url();
        $config['upload_path']   =   "uploads/profilepic/";
        $config['allowed_types'] = "gif|jpg|png|doc|txt|JPG|PNG|JPEG";
        $config['max_size'] = "20489";
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);
        $fullFilePath = null;
        if (!$this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }
        else
        {
            $filedetails = $this->upload->data();
            $fullFilePath = "uploads/profilepic/".$filedetails['file_name'];
            $userregistrationid = $this->session->userdata('registrationid');
            $this->getdata->updateprofilepic($userregistrationid, $fullFilePath);

        }
    }

    public function requestndinvite()
    {
        $this->load->model('getdata');
        $requestndinvite = $this->getdata->requestndinvite();
    }


    public function myprofile_scroll()
    {
        //$this->output->cache(15);
        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        //$userregistrationid=$this->session->userdata('registrationid');
        $userregistrationid = 1;
        $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);
        $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
        //print_r($collection_productid);
        if ($userregistrationid) {
            $data['seo'] = $this->getdata->seo('page', 'profile');
            //$data['mytimeline']=$this->getdata->timeline_of($userregistrationid);
            $data['user_detail'] = $this->getdata->user_details($userregistrationid);
            $user_brands = $this->getdata->get_followed_brand($userregistrationid, 'brand_id');
            $user_category = $this->getdata->get_followed_category($userregistrationid, 'category_id');
            //print_r($user_brands);
            // --------For the user friends and relastionship status ---------/////
            $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);
            // print_r($user_following_friends_id); exit;
            $all_comment = $this->getdata->get_comment_friends_all($user_following_friends_id); //getting comments of the friends following and getting all the data related to the friends
            //print_r($$all_comment); exit;
            $all_crowns = $this->getdata->get_crowns_friends_all($user_following_friends_id);//getting user crowned products and the post and related data
            //print_r($all_crowns); exit;
            $all_posts = $this->getdata->get_posts__friends_all($user_following_friends_id); // getting all the post which the user friends have posted and wish to share them on public.

            //print_r($all_posts);

            $myall_posts = [];

            for ($i = 0; $i < count($all_posts); $i++) {

                for ($j = 0; $j < count($all_posts[$i]); $j++) {


                    $myall_posts[] = $all_posts[$i][$j];
                }

            }

            $myall_comments = [];

            for ($i = 0; $i < count($all_comment); $i++) {

                for ($j = 0; $j < count($all_comment[$i]); $j++) {

                    $myall_comments[] = $all_comment[$i][$j];
                }

            }

            $myall_crowns = [];

            for ($i = 0; $i < count($all_crowns); $i++) {

                for ($j = 0; $j < count($all_crowns[$i]); $j++) {

                    $myall_crowns[] = $all_crowns[$i][$j];
                }

            }
//print_r($myall_crowns); exit;

            //print_r($all_posts);
            $myarr = array_merge($user_brands, $user_category);
            $user_data = $this->getdata->get_all_products($myarr);
            //print_r($user_data);
            //print_r($all_posts);

            //for changing the dimension of array

            $arr = [];
            foreach ($user_data as $value) {
                //	print_r($value);
                $arr[] = $value[0];
            }
            //print_r($arr); exit;
            //print_r($all_crowns);
            //print_r($all_crowns);
            $result_all_home = array_merge($myall_crowns, $myall_posts, $myall_comments, $arr);
            //print_r(($result_all_home)); exit;


            $myclean_posts = [];
            $mykeys = [];
            $mypost_keys = [];
            $myclean_posts_post = [];

            foreach ($result_all_home as $myclean) {

                if (array_key_exists('post_id', $myclean)) {

                    if (!in_array($myclean['post_id'], $mykeys)) {
                        $mykeys[] = $myclean['post_id'];

                        $myclean_posts[] = $myclean;


                    }


                } else {
                    if (!in_array($myclean['productId'], $mypost_keys)) {
                        $mypost_keys[] = $myclean['productId'];

                        $myclean_posts_post[] = $myclean;


                    }


                }
            }

            //print_r(array_merge($myclean_posts,$myclean_posts_post)); exit;

            //	print_r(array_map("myfunction",$result_all_home));

            //	function myfunction(){


            //	}
            //print_r($all_posts);
            //for sorting datewise
            function compare_lastname($a, $b)
            {
                return strnatcmp($b['date_time'], $a['date_time']);
            }

            // sort alphabetically by name
            usort($result_all_home, 'compare_lastname');

            //print_r($result_all_home);

            //$result = array_unique($result_all_home);
//print_r($result); exit;

            $data['user_data'] = $result_all_home;


            $myarr = array_merge($user_brands, $user_category);
            //print_r($myarr);
            $myarr1 = [];
            $sr = 0;
            //array  result for the user follow category and brand
            if (count($myarr) > 0) {
                //for showing homepage data
                //$data['user_data']=$this->getdata->get_all_products($myarr);
            } else {
                //$data['user_data']="";
            }
            //ends here array  result for the user follow category and brand
            //$data['user_category']=$this->getdata->user_details($userregistrationid,'category_id');
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //for the timeline part
            //$this->load->model('getdata');

            //for getting all comments of the user
            $user_comments = $this->getdata->get_comment_all($userregistrationid);
            //$user_posts=$this->getdata->get_posts_all('1');
            $user_crowns = $this->getdata->get_crowns_all($userregistrationid);

            $user_posts = $this->getdata->get_posts_all($userregistrationid);
            //$data['user_following']=$this->getdata->get_following_all($user_id);
            $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
            $me;
            foreach ($data as $value) {
                $me = $value;
            }
            //print_r($me);
            function sksort(&$array, $subkey = "id", $sort_ascending = false)
            {

                if (count($array))
                    $temp_array[key($array)] = array_shift($array);

                foreach ($array as $key => $val) {
                    $offset = 0;
                    $found = false;
                    foreach ($temp_array as $tmp_key => $tmp_val) {
                        if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                            $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                                array($key => $val),
                                array_slice($temp_array, $offset)
                            );
                            $found = true;
                        }
                        $offset++;
                    }
                    if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
                }

                if ($sort_ascending) $array = array_reverse($temp_array);

                else $array = $temp_array;
            }

            if (count($me) > 0) {
                sksort($me, "date_time");
                $data['user_val'] = $me;

                //print_r($data);
            }

            //timeline part ends here
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            //$this->load->view('pages/profile.php',$data);
            //if(count($me)>0&&count($myarr)>0){
            if (count($me) > 0 && count($myarr) > 0) {
                //echo "going here ";
                $this->load->view('pages/scroll_profile.php', $data);
            } else {
                //$this->load->view('pages/new_user.php',$data);
                $this->load->view('pages/scroll_profile.php', $data);
            }

        } else {

            echo "Login to visit your profile";
            //$this->load->view('pages/footer.php');
        }
    }

    public function scrolling_profile()
    {
        if (!$this->session->userdata('mydata')) {
            $this->load->model('getdata');
            $data['products'] = $this->getdata->getall();
            $userregistrationid = $this->session->userdata('registrationid');
            //$userregistrationid=1;
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);
            $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
            //print_r($collection_productid);
            if ($userregistrationid) {

                $hidden_list = $this->getdata->myhidden_post($userregistrationid);
                $data['seo'] = $this->getdata->seo('page', 'profile');
                //$data['mytimeline']=$this->getdata->timeline_of($userregistrationid);
                $data['user_detail'] = $this->getdata->user_details($userregistrationid);
                $user_brands = $this->getdata->get_followed_brand($userregistrationid, 'brand_id');
                $user_category = $this->getdata->get_followed_category($userregistrationid, 'category_id');
                //print_r($user_brands);
                // --------For the user friends and relastionship status ---------/////
                $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);
                // print_r($user_following_friends_id); exit;
                $all_comment = $this->getdata->get_comment_friends_all($user_following_friends_id); //getting comments of the friends following and getting all the data related to the friends
                //print_r($$all_comment); exit;
                $all_crowns = $this->getdata->get_crowns_friends_all($user_following_friends_id);//getting user crowned products and the post and related data
                //print_r($all_crowns); exit;
                $all_posts = $this->getdata->get_posts__friends_all($user_following_friends_id); // getting all the post which the user friends have posted and wish to share them on public.

                //print_r($all_posts);

                $myall_posts = [];

                for ($i = 0; $i < count($all_posts); $i++) {

                    for ($j = 0; $j < count($all_posts[$i]); $j++) {


                        $myall_posts[] = $all_posts[$i][$j];
                    }

                }

                $myall_comments = [];

                for ($i = 0; $i < count($all_comment); $i++) {

                    for ($j = 0; $j < count($all_comment[$i]); $j++) {

                        $myall_comments[] = $all_comment[$i][$j];
                    }

                }

                $myall_crowns = [];

                for ($i = 0; $i < count($all_crowns); $i++) {

                    for ($j = 0; $j < count($all_crowns[$i]); $j++) {

                        $myall_crowns[] = $all_crowns[$i][$j];
                    }

                }
//print_r($myall_crowns); exit;

                //print_r($all_posts);
                $myarr = array_merge($user_brands, $user_category);
                $user_data = $this->getdata->get_all_products($myarr);
                //print_r($user_data);
                //print_r($all_posts);

                //for changing the dimension of array

                $arr = [];
                foreach ($user_data as $value) {
                    //	print_r($value);
                    $arr[] = $value[0];
                }
                //print_r($arr); exit;
                //print_r($all_crowns);
                //print_r($all_crowns);
                $result_all_home = array_merge($myall_crowns, $myall_posts, $myall_comments, $arr);
                //print_r(($result_all_home)); exit;


                $myclean_posts = [];
                $mykeys = [];
                $mypost_keys = [];
                $myclean_posts_post = [];

                foreach ($result_all_home as $myclean) {

                    if (array_key_exists('post_id', $myclean)) {

                        if (!in_array($myclean['post_id'], $mykeys)) {
                            $mykeys[] = $myclean['post_id'];

                            $myclean_posts[] = $myclean;


                        }


                    } else {
                        if (!in_array($myclean['productId'], $mypost_keys)) {
                            $mypost_keys[] = $myclean['productId'];

                            $myclean_posts_post[] = $myclean;


                        }


                    }
                }
                function compare_lastname($a, $b)
                {
                    return strnatcmp($b['date_time'], $a['date_time']);
                }

                // sort alphabetically by name
                usort($result_all_home, 'compare_lastname');

                //print_r($result_all_home);

                $myprod = [];
                $mypost = [];
                $myaprod_home_array = [];

                foreach ($hidden_list as $ht) {

                    $myh_list[] = $ht['product_id'];
                }

                foreach ($result_all_home as $value) {

                    //print_r($value);
                    if (array_key_exists('productId', $value)) {
                        if (!in_array($value['productId'], $myprod)) {
                            if (!in_array($value['productId'], $myh_list)) {
                                $myaprod_home_array[] = $value;
                                $myprod[] = $value['productId'];
                            }
                        }

                    } else if (array_key_exists('source', $value)) {
                        if (isset($value['productId'])) {
                            if (in_array($value['productId'], $myprod)) {
                                if (!in_array($value['productId'], $myh_list)) {
                                    $myaprod_home_array[] = $value;
                                    $myprod[] = $value['productId'];
                                }
                            }
                        } else {

                            $myaprod_home_array[] = $value;
                        }

                    } else {
                        $myaprod_home_array[] = $value;


                    }


                }
            } else {
                $this->session->set_userdata('mydata', $myaprod_home_array);
            }

//print_r($myprod);
            //$result = array_unique($result_all_home);
//print_r($result); exit;
            $data['user_data'] = $myaprod_home_array;
            //print_r($result_all_home);

            function paganation($display_array, $page)
            {
                $show_per_page = 20;

                $page = $page < 1 ? 1 : $page;

                // start position in the $display_array
                // +1 is to account for total values.
                $start = ($page - 1) * ($show_per_page + 1);
                //echo $start; exit;
                $offset = $show_per_page + 1;

                return $outArray = array_slice($display_array, $start, $offset);

                // print_r($outArray);
            }


            $data['products'] = paganation($myaprod_home_array, $_POST['page']);

            //print_r($data);


            $this->load->view('pages/myprofile_partials.php', $data);

        }
    }

    function userhome()
    {
//$this->output->cache(5);
        $this->load->model('getdata');
        $data['finest'] = $this->getdata->finest();
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        ///$hidden_list=$this->getedat->myhidden_post();
        if (!$userregistrationid) {
            redirect(base_url());
        }
        $hidden_list = $this->getdata->myhidden_post($userregistrationid);
        $hidden_list_post = $this->getdata->myhidden_userpost($userregistrationid);
//print_r($hidden_list); exit;

        $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);

        $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
        if ($userregistrationid) {
            $data['seo'] = $this->getdata->seo('page', 'profile');
            //$data['mytimeline']=$this->getdata->timeline_of($userregistrationid);
            $data['user_detail'] = $this->getdata->user_details($userregistrationid);
            $user_brands = $this->getdata->get_followed_brand($userregistrationid, 'brand_id');
            $user_category = $this->getdata->get_followed_category($userregistrationid, 'category_id');
            //print_r($user_brands);
            // --------For the user friends and relastionship status ---------/////
            $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);
            // print_r($user_following_friends_id);
            $all_comment = $this->getdata->get_comment_friends_all($user_following_friends_id); //getting comments of the friends following and getting all the data related to the friends
            //print_r($$all_comment); exit;
            $all_crowns = $this->getdata->get_crowns_friends_all($user_following_friends_id);//getting user crowned products and the post and related data
            $all_posts = $this->getdata->get_posts__friends_all($user_following_friends_id); // getting all the post which the user friends have posted and wish to share them on public.

            $myall_posts = [];

            for ($i = 0; $i < count($all_posts); $i++) {

                for ($j = 0; $j < count($all_posts[$i]); $j++) {


                    $myall_posts[] = $all_posts[$i][$j];
                }

            }

            $myall_comments = [];

            for ($i = 0; $i < count($all_comment); $i++) {

                for ($j = 0; $j < count($all_comment[$i]); $j++) {


                    $myall_comments[] = $all_comment[$i][$j];
                }

            }

            $myall_crowns = [];
            $mykeys = [];

            for ($i = 0; $i < count($all_crowns); $i++) {

                for ($j = 0; $j < count($all_crowns[$i]); $j++) {

                    if (!in_array($all_crowns[$i][$j]['productId'], $mykeys)) {
                        $mykeys[] = $all_crowns[$i][$j]['productId'];

                        $myall_crowns[] = $all_crowns[$i][$j];


                    }


                }

            }


            //print_r($myall_crowns); exit;
            $myarr = array_merge($user_brands, $user_category);
            $user_data = $this->getdata->get_all_products($myarr);
            //print_r($user_data);
            //print_r($all_posts);

            //print_r($myarr); exit;

            $arr = [];
//$a=0;
            foreach ($user_data as $value) {
//print_r($value);
                $arr[] = $value[0];
                //$a++;
            }
            //print_r($arr); exit;
            //print_r($all_crowns);
            //print_r($all_crowns);
            $result_all_home = array_merge($myall_crowns, $myall_posts, $myall_comments, $arr);
            $myclean_posts = [];
            $mykeys = [];
            $mypost_keys = [];
            $myclean_posts_post = [];
            foreach ($result_all_home as $myclean) {

                if (array_key_exists('post_id', $myclean)) {

                    if (!in_array($myclean['post_id'], $mykeys)) {
                        $mykeys[] = $myclean['post_id'];

                        $myclean_posts[] = $myclean;


                    }


                } else {


                    if (!in_array($myclean['productId'], $mypost_keys)) {
                        $mypost_keys[] = $myclean['productId'];

                        $myclean_posts_post[] = $myclean;


                    }

                }
            }


//print_r($myclean_posts_post); exit;


            $result_all_home = array_merge($myclean_posts, $myclean_posts_post);


            //print_r($all_posts);
            function compare_lastname($a, $b)
            {
                return strnatcmp($b['date_time'], $a['date_time']);
            }

            // sort alphabetically by name
            usort($result_all_home, 'compare_lastname');

            //print_r($myarr1); exit;

            $myprod = [];
            $mypost = [];
            $myaprod_home_array = [];

            $myh_list = [];

            $myhlist_post = [];


            foreach ($hidden_list as $ht) {

                $myh_list[] = $ht['product_id'];
            }


            foreach ($hidden_list_post as $ht_post) {

                $myhlist_post[] = $ht_post['post_id'];
            }

            foreach ($result_all_home as $value) {

                //print_r($value);
                if (array_key_exists('productId', $value)) {
                    if (!in_array($value['productId'], $myprod)) {
//print_r($myprod);
                        if (!in_array($value['productId'], $myh_list)) {
                            $myaprod_home_array[] = $value;
                            $myprod[] = $value['productId'];
                        }
                    }

                } else if (array_key_exists('source', $value)) {
                    if (isset($value['productId'])) {
                        if (in_array($value['productId'], $myprod)) {
                            if (!in_array($value['productId'], $myh_list)) {
                                $myaprod_home_array[] = $value;
                                $myprod[] = $value['productId'];
                            }
                        }
                    } else {


                        if (!in_array($value['post_id'], $myhlist_post)) {


                            $myaprod_home_array[] = $value;
                        }

                    }
                } else {
                    if (!in_array($value['post_id'], $myhlist_post)) {


                        $myaprod_home_array[] = $value;
                    }


                }


            }


            $data['user_data'] = $myaprod_home_array;
            //print_r($result_all_home); exit;


            //$myarr=array_merge($user_brands,$user_category);
            //print_r($myarr);
            $myarr1 = [];
            $sr = 0;
            //array  result for the user follow category and brand
            if (count($myarr) > 0) {
                //for showing homepage data
                //$data['user_data']=$this->getdata->get_all_products($myarr);
            } else {
                //$data['user_data']="";
            }
            //ends here array  result for the user follow category and brand
            //$data['user_category']=$this->getdata->user_details($userregistrationid,'category_id');
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //for the timeline part
            //$this->load->model('getdata');

            //for getting all comments of the user
            $user_comments = $this->getdata->get_comment_all($userregistrationid);
            //$user_posts=$this->getdata->get_posts_all('1');
            $user_crowns = $this->getdata->get_crowns_all($userregistrationid);

            $user_posts = $this->getdata->get_posts_all($userregistrationid);
            //$data['user_following']=$this->getdata->get_following_all($user_id);
            $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
            $me;
            foreach ($data as $value) {
                $me = $value;
            }
            //print_r($me);
            function sksort(&$array, $subkey = "id", $sort_ascending = false)
            {

                if (count($array))
                    $temp_array[key($array)] = array_shift($array);

                foreach ($array as $key => $val) {
                    $offset = 0;
                    $found = false;
                    foreach ($temp_array as $tmp_key => $tmp_val) {
                        if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                            $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                                array($key => $val),
                                array_slice($temp_array, $offset)
                            );
                            $found = true;
                        }
                        $offset++;
                    }
                    if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
                }

                if ($sort_ascending) $array = array_reverse($temp_array);

                else $array = $temp_array;
            }

            if (count($me) > 0) {
                sksort($me, "date_time");
                $data['user_val'] = $me;

                //print_r($data);
            }

            //timeline part ends here
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            //$this->load->view('pages/profile.php',$data);
            //if(count($me)>0&&count($myarr)>0){
            if (count($me) > 0 && count($myarr) > 0) {
                //echo "going here ";
                $this->load->view('pages/userhome.php', $data);
            } else {
                $this->load->view('pages/userhome.php', $data);
            }

        } else {

            echo "Login to visit your profile";
            //$this->load->view('pages/footer.php');
        }
    }

    function usertimeline()
    {
//$this->output->cache(5);
        $this->load->model('getdata');
        $data['finest'] = $this->getdata->finest();
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();

        $userregistrationid = $this->session->userdata('registrationid');

          $data['pending_request']=$this->getdata->get_pending_id($userregistrationid);


        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['user_following_brands_sidebar'] = $this->getdata->following_brands_sidebar($userregistrationid, 'brand_id');
        if (!$userregistrationid) {
            redirect(base_url());
        }
        $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
        if ($userregistrationid) {
            $data['seo'] = $this->getdata->seo('page', 'profile');
            //$data['mytimeline']=$this->getdata->timeline_of($userregistrationid);
            $data['user_detail'] = $this->getdata->user_details($userregistrationid);
            $user_brands = $this->getdata->get_followed_brand($userregistrationid, 'brand_id');
            $user_category = $this->getdata->get_followed_category($userregistrationid, 'category_id');
            //print_r($user_brands);
            // --------For the user friends and relastionship status ---------/////
            $user_following_friends_id = $this->getdata->get_friends_id($userregistrationid);
            // print_r($user_following_friends_id);
            $all_comment = $this->getdata->get_comment_friends_all($user_following_friends_id); //getting comments of the friends following and getting all the data related to the friends
            //print_r($$all_comment); exit;
            $all_crowns = $this->getdata->get_crowns_friends_all($user_following_friends_id);//getting user crowned products and the post and related data
            $all_posts = $this->getdata->get_posts__friends_all($user_following_friends_id); // getting all the post which the user friends have posted and wish to share them on public.

            $myall_posts = [];

            for ($i = 0; $i < count($all_posts); $i++) {

                for ($j = 0; $j < count($all_posts[$i]); $j++) {


                    $myall_posts[] = $all_posts[$i][$j];
                }

            }

            $myall_comments = [];

            for ($i = 0; $i < count($all_comment); $i++) {

                for ($j = 0; $j < count($all_comment[$i]); $j++) {


                    $myall_comments[] = $all_comment[$i][$j];
                }

            }

            $myall_crowns = [];
            $mykeys = [];

            for ($i = 0; $i < count($all_crowns); $i++) {

                for ($j = 0; $j < count($all_crowns[$i]); $j++) {

                    if (!in_array($all_crowns[$i][$j]['productId'], $mykeys)) {
                        $mykeys[] = $all_crowns[$i][$j]['productId'];

                        $myall_crowns[] = $all_crowns[$i][$j];


                    }


                }

            }


            //print_r($myall_crowns); exit;
            $myarr = array_merge($user_brands, $user_category);
            $user_data = $this->getdata->get_all_products($myarr);
            //print_r($user_data);
            //print_r($all_posts);

            //print_r($myarr); exit;

            $arr = [];
//$a=0;
            foreach ($user_data as $value) {
//print_r($value);
                $arr[] = $value[0];
                //$a++;
            }
            //print_r($arr); exit;
            //print_r($all_crowns);
            //print_r($all_crowns);
            $result_all_home = array_merge($myall_crowns, $myall_posts, $myall_comments, $arr);
            $myclean_posts = [];
            $mykeys = [];
            $mypost_keys = [];
            $myclean_posts_post = [];
            foreach ($result_all_home as $myclean) {

                if (array_key_exists('post_id', $myclean)) {

                    if (!in_array($myclean['post_id'], $mykeys)) {
                        $mykeys[] = $myclean['post_id'];

                        $myclean_posts[] = $myclean;


                    }


                } else {


                    if (!in_array($myclean['productId'], $mypost_keys)) {
                        $mypost_keys[] = $myclean['productId'];

                        $myclean_posts_post[] = $myclean;


                    }

                }
            }


//print_r($myclean_posts_post); exit;


            $result_all_home = array_merge($myclean_posts, $myclean_posts_post);


            //print_r($all_posts);
            function compare_lastname($a, $b)
            {
                return strnatcmp($b['date_time'], $a['date_time']);
            }

            // sort alphabetically by name
            usort($result_all_home, 'compare_lastname');

            //print_r($myarr1); exit;

            $data['user_data'] = $result_all_home;
            //print_r($result_all_home); exit;


            $myarr = array_merge($user_brands, $user_category);
            //print_r($myarr);
            $myarr1 = [];
            $sr = 0;
            //array  result for the user follow category and brand
            if (count($myarr) > 0) {
                //for showing homepage data
                //$data['user_data']=$this->getdata->get_all_products($myarr);
            } else {
                //$data['user_data']="";
            }
            //ends here array  result for the user follow category and brand
            //$data['user_category']=$this->getdata->user_details($userregistrationid,'category_id');
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            //for the timeline part
            //$this->load->model('getdata');

            //for getting all comments of the user
            $user_comments = $this->getdata->get_comment_all($userregistrationid);
            //$user_posts=$this->getdata->get_posts_all('1');
            $user_crowns = $this->getdata->get_crowns_all($userregistrationid);

            $user_posts = $this->getdata->get_posts_all($userregistrationid);


//print_r($user_posts); exit;

            //$data['user_following']=$this->getdata->get_following_all($user_id);
            $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
            $me;
            foreach ($data as $value) {
                $me = $value;
            }
            //print_r($me);
            function sksort(&$array, $subkey = "id", $sort_ascending = false)
            {

                if (count($array))
                    $temp_array[key($array)] = array_shift($array);

                foreach ($array as $key => $val) {
                    $offset = 0;
                    $found = false;
                    foreach ($temp_array as $tmp_key => $tmp_val) {
                        if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                            $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                                array($key => $val),
                                array_slice($temp_array, $offset)
                            );
                            $found = true;
                        }
                        $offset++;
                    }

                    if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
                }

                if ($sort_ascending) $array = array_reverse($temp_array);

                else $array = $temp_array;
            }

            if (count($me) > 0) {
                sksort($me, "date_time");
                $data['user_val'] = $me;

                //print_r($data);
            }

            //timeline part ends here
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            //$this->load->view('pages/profile.php',$data);
            //if(count($me)>0&&count($myarr)>0){
            if (count($user_posts) > 0 && count($user_posts) > 0) {
                //echo "going here ";
                $this->load->view('pages/usertimeline.php', $data);
            } else {
                $this->load->view('pages/usertimeline.php', $data);
            }

        } else {

            echo "Login to visit your profile";
            //$this->load->view('pages/footer.php');
        }
    }

    public function get_post_id()
    {

        $data['post_id'] = $this->input->post('post_id');
        $this->load->model('getdata');
        //$data['postcount_crown']=$this->getdata->postcount_crown($post_id);
        // $data['postcomment_count']= $this->getdata->count_commentonpost($post_id);
        $this->load->view('pages/countcomment.php', $data);
        //$requestndinvite = $this->getdata->requestndinvite();
    }

    public function scroll_timeline()
    {

        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $user_comments = $this->getdata->get_comment_all($userregistrationid);
        //$user_posts=$this->getdata->get_posts_all('1');
        $user_crowns = $this->getdata->get_crowns_all($userregistrationid);
        $user_posts = $this->getdata->get_posts_all($userregistrationid);
        //$data['user_following']=$this->getdata->get_following_all($user_id);
        $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
        $me;
        foreach ($data as $value) {
            $me = $value;
        }
        //print_r($me);
        function sksort(&$array, $subkey = "id", $sort_ascending = false)
        {

            if (count($array))
                $temp_array[key($array)] = array_shift($array);

            foreach ($array as $key => $val) {
                $offset = 0;
                $found = false;
                foreach ($temp_array as $tmp_key => $tmp_val) {
                    if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                        $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                            array($key => $val),
                            array_slice($temp_array, $offset)
                        );
                        $found = true;
                    }
                    $offset++;
                }
                if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
            }

            if ($sort_ascending) $array = array_reverse($temp_array);

            else $array = $temp_array;
        }

        if (count($me) > 0) {
            sksort($me, "date_time");
            $user_val = $me; //his array is for the user timeline page

            //print_r($data);
        }

        //print_r($data);
        function paganation($display_array, $page)
        {
            $show_per_page = 3;

            $page = $page < 1 ? 1 : $page;

            // start position in the $display_array
            // +1 is to account for total values.
            $start = ($page - 1) * ($show_per_page + 1);
            //echo $start; exit;
            $offset = $show_per_page + 1;

            return $outArray = array_slice($display_array, $start, $offset);

            // print_r($outArray);
        }


        $data['user_detail'] = $this->getdata->user_details($userregistrationid);
        $data['user_val'] = paganation($user_val, $_POST['page']);
        $this->load->view('pages/partial_timeline.php', $data);

    }


    function collection()
    {
//$this->output->cache(5);
        $this->load->model('getdata');
        $data['finest'] = $this->getdata->finest();
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();
        $userregistrationid = $this->session->userdata('registrationid');
        if (!$userregistrationid) {
            redirect(base_url());
        }
        $data['gallery_img'] = $this->getdata->get_gallery_image($userregistrationid);
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['collection_productid'] = $this->getdata->get_collection($userregistrationid);
        if ($userregistrationid) {
            $data['seo'] = $this->getdata->seo('page', 'profile');
            $data['user_detail'] = $this->getdata->user_details($userregistrationid);
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head.php', $data);//passing category in the head section
            $this->load->view('pages/collection.php', $data);
        } else {

            echo "Login to visit your profile";
            //$this->load->view('pages/footer.php');
        }
    }


    public function scroll_friend_timeline($friend_id)
    {

        $userregistrationid = $friend_id;
        $userregistrationid_me = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['userdata_friend'] = $this->getdata->user_details($this->session->userdata('registrationid'));
        $user_comments = $this->getdata->get_comment_all($userregistrationid);
        //$user_posts=$this->getdata->get_posts_all('1');
        $user_crowns = $this->getdata->get_crowns_all($userregistrationid);
        $user_posts = $this->getdata->get_posts_all($userregistrationid);
        //$data['user_following']=$this->getdata->get_following_all($user_id);
        $data['user_val'] = array_merge($user_comments, $user_crowns, $user_posts);
        $me;
        foreach ($data as $value) {
            $me = $value;
        }
        //print_r($me);
        function sksort(&$array, $subkey = "id", $sort_ascending = false)
        {

            if (count($array))
                $temp_array[key($array)] = array_shift($array);

            foreach ($array as $key => $val) {
                $offset = 0;
                $found = false;
                foreach ($temp_array as $tmp_key => $tmp_val) {
                    if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                        $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                            array($key => $val),
                            array_slice($temp_array, $offset)
                        );
                        $found = true;
                    }
                    $offset++;
                }
                if (!$found) $temp_array = array_merge($temp_array, array($key => $val));
            }

            if ($sort_ascending) $array = array_reverse($temp_array);

            else $array = $temp_array;
        }

        if (count($me) > 0) {
            sksort($me, "date_time");
            $user_val = $me; //his array is for the user timeline page

            //print_r($data);
        }

        //print_r($data);
        function paganation($display_array, $page)
        {
            $show_per_page = 3;

            $page = $page < 1 ? 1 : $page;

            // start position in the $display_array
            // +1 is to account for total values.
            $start = ($page - 1) * ($show_per_page + 1);
            //echo $start; exit;
            $offset = $show_per_page + 1;

            return $outArray = array_slice($display_array, $start, $offset);

            // print_r($outArray);
        }

        $data['myfriend_partial_id'] = $friend_id;
        $data['user_detail'] = $this->getdata->user_details($userregistrationid);
        $data['user_val'] = paganation($user_val, $_POST['page']);
        $this->load->view('pages/partial_timeline_friend.php', $data);

    }


    public function feed(){
        $this->session->unset_userdata('inviteuserid');
        $userregistrationid = $this->session->userdata('registrationid');
        $this->load->model('getdata');
        $data['products'] = $this->getdata->getall();
        $data['avali_cat'] = $this->getdata->availcat();
        $data['all_products_count'] = $this->getdata->record_count();

        //print_r($data['avali_cat']); exit;
        $data['slider_image'] = $this->getdata->slider('page', 'main');
        $data['seo'] = $this->getdata->seo('page', 'home');
        $userregistrationid = $this->session->userdata('registrationid');
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //print_r($data);
        $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
        $data['heading'] = "See whats new here !";
        $this->load->view('pages/head.php', $data);//passing category in the head section
        $this->load->view('pages/stores.php', $data);
        $this->load->view('pages/footer.php');



    }


    public function myslider(){


        $this->load->view('pages/slider_content.php');
            }



    public function follow_notification()
    {

        $this->load->model('getdata');
        $userregistrationid = $this->session->userdata('registrationid');

         return  $user_pending_id = $this->getdata->get_pending_id($userregistrationid);


    }

}