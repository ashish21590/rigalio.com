<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Linkedin_signup extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->helper('url');
		$this->load->library('session');
    }

    function index() {
        echo '<form id="linkedin_connect_form" action="http://rigalio.com/x/no_access/linkedin_signup/initiate" method="post">';
        echo '<input type="submit" value="Login with LinkedIn" />';
        echo '</form>';
    }

    function initiate() {

        // setup before redirecting to Linkedin for authentication.
        $linkedin_config = array(
          'appKey' => '756jk5lm9azphk',
            'appSecret' => 'wPKyvtUn1kaOwK44',
            'callbackUrl' => 'http://rigalio.com/x/no_access/linkedin_signup/data/'
        );

        $this->load->library('linkedin', $linkedin_config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $token = $this->linkedin->retrieveTokenRequest();

        $this->session->set_flashdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
        $this->session->set_flashdata('oauth_request_token', $token['linkedin']['oauth_token']);

        $link = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
        redirect($link);
    }

    function cancel() {

        // See https://developer.linkedin.com/documents/authentication
        // You need to set the 'OAuth Cancel Redirect URL' parameter to <your URL>/linkedin_signup/cancel

        echo 'Linkedin user cancelled login';
    }

    function logout() {
		$base_url=$this->config->item('base_url');
		$this->session->sess_destroy();
		redirect($base_url);
        session_unset();
       // $_SESSION = array();
       // echo "Logout successful";
    }

    function data() {

        $linkedin_config = array(
            'appKey' => '756jk5lm9azphk',
            'appSecret' => 'wPKyvtUn1kaOwK44',
            'callbackUrl' => 'http://rigalio.com/x/no_access/linkedin_signup/data/'
        );

        $this->load->library('linkedin', $linkedin_config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $oauth_token = $this->session->flashdata('oauth_request_token');
        $oauth_token_secret = $this->session->flashdata('oauth_request_token_secret');
        $oauth_verifier = $this->input->get('oauth_verifier');
        $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
        // ok if we are good then proceed to retrieve the data from Linkedin
        if ($response['success'] === TRUE) {
            // From this part onward it is up to you on how you want to store/manipulate the data 
            $oauth_expires_in = $response['linkedin']['oauth_expires_in'];
            $oauth_authorization_expires_in = $response['linkedin']['oauth_authorization_expires_in'];
            $response = $this->linkedin->setTokenAccess($response['linkedin']);
            $profile = $this->linkedin->profile('~:(id,first-name,last-name,picture-url,email-address)');
            $profile_connections = $this->linkedin->profile(                            '~/connections:(id,email-address,first-name,last-name,picture-url,industry)');
            $user = json_decode($profile['linkedin']);
            //print_r($user); exit;
            $user_array = array('linkedin_id' => $user->id, 'second_name' => $user->lastName, 'profile_picture' => $user->pictureUrl, 'first_name' => $user->firstName,'email'=>$user->emailAddress);
            //$data['user']=$user_array;
			$this->load->model('getdata');
			$data=$this->getdata->checkuser($user_array['email']);
			//echo count($data); exit;
			 if(count($data)>0){
		  redirect('linkedin_signup/closewindow');
		  }
			else{
			 $inviteuserid = $this->session->userdata('inviteuserid');
				$myvalue=array(
				'userinviteid' => $inviteuserid,
				'firstname' => $user_array['first_name'],
				'lastname' => $user_array['second_name'],
				'email'=>$user_array['email'],
				'linkedinid'=>$user_array['linkedin_id'],
				'profile_picture'=>$user_array['profile_picture'],
				'auth_provider'=>'Linkedin'
				 );
				// print_r($myvalue); exit;
				 $data['user_id']=$this->getdata->insert_fbuser($myvalue);
				  redirect('linkedin_signup/closewindow');
               }
        } 
    }

  function closewindow()
	{
	$this->load->view('pages/fbloginonload.php'); 
	}
}